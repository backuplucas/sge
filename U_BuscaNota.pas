unit U_BuscaNota;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U_Padrao, StdCtrls, Buttons, ExtCtrls, ComCtrls, Grids, DBGrids,
  FMTBcd, DB, DBClient, Provider, SqlExpr, SLClientDataSet,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.AppEvnts;

type
  TF_BuscaNotaFiscal = class(TF_Padrao)
    Panel1: TPanel;
    pstb_padrao: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBGrid1: TDBGrid;
    sdsNfList: TFDQuery;
    dspNfList: TDataSetProvider;
    cdsNfList: TClientDataSet;
    dsNfEntrada: TDataSource;
    cdsNfListd_entrada: TSQLTimeStampField;
    cdsNfListi_nrnf: TIntegerField;
    cdsNfListi_cdnfentrada: TIntegerField;
    cdsNfListi_cdparceiro: TIntegerField;
    cdsSalvaNfBkp: TSLClientDataSet;
    dspSalvaNfBkp: TDataSetProvider;
    sdsSalvaNfBkp: TFDQuery;
    cdsVerificaNfBkp: TClientDataSet;
    dspVerificaNfBkp: TDataSetProvider;
    sdsVerificaNfBkp: TFDQuery;
    cdsSalvaItem: TSLClientDataSet;
    dspSalvaItem: TDataSetProvider;
    sdsSalvaItem: TFDQuery;
    cdsItemNF: TClientDataSet;
    dspItemNF: TDataSetProvider;
    sdsItemNF: TFDQuery;
    cdsVerificaItem: TClientDataSet;
    dspVerificaItem: TDataSetProvider;
    sdsVerificaItem: TFDQuery;
    cdsNfListc_serienf: TWideStringField;
    cdsNfListn_vlrnf: TBCDField;
    cdsNfListc_nome: TWideStringField;
    procedure BitBtn2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    Function VerificaNfBkp(vI_CdNf :integer): integer;
    Function VerificaItem(vI_CdProduto: integer): integer;
  public
    vI_CdNfBkp: Integer;
    procedure CarregaNota;
  end;

var
  F_BuscaNotaFiscal: TF_BuscaNotaFiscal;

implementation

{$R *.dfm}

uses dm, U_entrada;

procedure TF_BuscaNotaFiscal.BitBtn2Click(Sender: TObject);
begin
  inherited;
  CarregaNota;
end;

procedure TF_BuscaNotaFiscal.CarregaNota;
var
  vI_CdNfBkpVinc: Integer;
  vI_CdItemNfBkp: Integer;
  TD: TTransactionDesc;
begin
  try
    if not(DMSGE.SQLConnection.InTransaction) then begin
      TD.TransactionID := 1;
      TD.IsolationLevel := xilREADCOMMITTED;
      DMSGE.SQLConnection.StartTransaction();
      cdsSalvaNfbkp.RequestFindID(vI_CdNfBkp);
      cdsSalvaNfBkp.RequestFindID(vI_CdNfBkp);

      if cdsSalvaNfBkp.RecordCount = 0 then
        Abort;

      vI_CdNfBkpVinc := VerificaNfBkp(cdsNfListi_cdnfentrada.AsInteger);

      if vI_CdNfBkpVinc > 0 then begin
        if vI_CdNfBkpVinc = vI_CdNfBkp then
          MessageDlg('Complemento j� vinculado!', mtWarning, [mbOK], 0)
        else
          MessageDlg('Existe complemento vinculado a nota fiscal. Complemento ' + IntToStr(vI_CdNfBkpVinc) + '.', mtWarning, [mbOK], 0);
        Abort;
      end;

      cdsSalvaNfbkp.AddValue('i_cdparceiro', cdsNfListi_cdparceiro.AsInteger, ftInteger);
      cdsSalvaNfbkp.AddValue('n_vlrprodutos', cdsNfListn_vlrnf.AsFloat, ftFloat);
      cdsSalvaNfbkp.AddValue('i_cdsequencial', cdsNfListi_cdnfentrada.AsInteger, ftInteger);
      cdsSalvaNfbkp.RequestSave;

      cdsItemNF.Close;
      cdsItemNF.Params.ParamByName('I_CdNfEntrada').AsInteger := cdsNfListi_cdnfentrada.AsInteger;
      cdsItemNF.Open;

      if cdsItemNF.RecordCount > 0 then begin
        cdsItemNF.First;

        while not(cdsItemNF.Eof) do begin
          vI_CdItemNfBkp := VerificaItem(cdsItemNF.FieldByName('i_ctitem').AsInteger);

          if vI_CdItemNfBkp > 0 then
            cdsSalvaItem.RequestFindID(vI_CdItemNfBkp)
          else
            cdsSalvaItem.RequestClear;

          cdsSalvaItem.AddValue('i_cdnfbkp', vI_CdNfBkp, ftInteger);
          cdsSalvaItem.AddValue('i_ctitem', cdsItemNF.FieldByName('i_ctitem').AsInteger, ftInteger);
          cdsSalvaItem.AddValue('c_cdfornec', cdsItemNF.FieldByName('c_cdprodfornec').AsString, ftString);
          cdsSalvaItem.AddValue('i_cdproduto', cdsItemNF.FieldByName('i_cdproduto').AsInteger, ftInteger);
          cdsSalvaItem.AddValue('n_prcunitario', cdsItemNF.FieldByName('n_prcunitario').AsFloat, ftFloat);
          cdsSalvaItem.AddValue('n_quantidade', cdsItemNF.FieldByName('n_quantidade').AsFloat, ftFloat);
          cdsSalvaItem.AddValue('i_cdorigem', cdsItemNF.FieldByName('i_cditemnfentrada').AsInteger, ftInteger);
          cdsSalvaItem.RequestSave;
          cdsItemNF.Next;
        end;
      end;

      if not(F_Entrada.cdsNfBkp.State in [dsInsert, dsEdit]) then
        F_Entrada.cdsNfBkp.Edit;

      F_Entrada.eIDNota.Field.Value := cdsNfListi_cdnfentrada.AsInteger;
      DMSGE.SQLConnection.Commit();
    end;
  except
    on E: Exception do begin
      DMSGE.SQLConnection.Rollback();
      raise;
      Exit;
    end;
  end;
end;

procedure TF_BuscaNotaFiscal.DBGrid1DblClick(Sender: TObject);
begin
  inherited;
  CarregaNota;
  ModalResult := mrOK;
end;

function TF_BuscaNotaFiscal.VerificaItem(vI_CdProduto: Integer): Integer;
begin
  cdsVerificaItem.Close;
  cdsVerificaItem.CommandText :=
    ' select ' +
    '   i_cditemnfbkp ' +
    ' from ' +
    '   itemnfbkp ' +
    ' where ' +
    '   i_cdproduto = ' + IntToStr(vI_CdProduto) + ' and ' +
    '   i_cdnfbkp = ' + IntToStr(vI_CdNfBkp);
  cdsVerificaItem.Open;

  Result := cdsVerificaItem.FieldByName('i_cditemnfbkp').AsInteger;
end;

function TF_BuscaNotaFiscal.VerificaNfBkp(vI_CdNf: integer): integer;
begin
  cdsVerificaNfBkp.Close;
  cdsVerificaNfBkp.CommandText := ' select i_cdnfbkp from nfbkp join itemnfbkp using (i_cdnfbkp) where i_cdsequencial = ' + IntToStr(vI_CdNf);
  cdsVerificaNfBkp.Open;

  Result := cdsVerificaNfBkp.FieldByName('i_cdnfbkp').AsInteger;
end;

end.
