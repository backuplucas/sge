inherited F_Monitor: TF_Monitor
  Left = 388
  Top = 49
  Caption = 'Monitor'
  ClientWidth = 1057
  Font.Charset = ANSI_CHARSET
  FormStyle = fsMDIForm
  OldCreateOrder = True
  OnDestroy = FormDestroy
  ExplicitWidth = 1073
  PixelsPerInch = 96
  TextHeight = 13
  inherited stb_Padrao: TStatusBar
    Width = 1057
  end
  object PanelPrincipal: TPanel [1]
    Left = 0
    Top = 0
    Width = 1057
    Height = 41
    Align = alTop
    TabOrder = 1
    ExplicitWidth = 700
    object Label1: TLabel
      Left = 760
      Top = 12
      Width = 216
      Height = 19
      Caption = '* Tag = 1 n'#227'o abre o ClientDataSet'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      OnClick = FormActivate
    end
    object lSalvando: TLabel
      Left = 520
      Top = 12
      Width = 186
      Height = 19
      Caption = 'Salvando e formatando SQL...'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object BitBtn1: TBitBtn
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Limpar'
      Kind = bkAbort
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object Button1: TButton
      Left = 102
      Top = 10
      Width = 76
      Height = 23
      Caption = 'Gravar'
      TabOrder = 1
      OnClick = Button1Click
    end
    object eCaminho: TEdit
      Left = 272
      Top = 11
      Width = 222
      Height = 21
      TabOrder = 2
      Text = 'C:\Monitor.SQL'
    end
    object Button4: TButton
      Left = 184
      Top = 8
      Width = 81
      Height = 25
      Caption = 'Monitoramento'
      TabOrder = 3
      OnClick = Button4Click
    end
  end
  object PageControl1: TPageControl [2]
    Left = 0
    Top = 41
    Width = 1057
    Height = 540
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 2
    ExplicitWidth = 700
    object TabSheet1: TTabSheet
      Caption = 'S&QL'#39's'
      object StringGrid: TStringGrid
        Left = 0
        Top = 0
        Width = 1049
        Height = 512
        Align = alClient
        ColCount = 4
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing, goRowMoving, goColMoving, goEditing, goTabs, goAlwaysShowEditor]
        TabOrder = 0
        ColWidths = (
          59
          734
          87
          64)
        RowHeights = (
          24
          17)
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'T&abelas'
      ImageIndex = 1
      object sgtabelas: TStringGrid
        Left = 0
        Top = 0
        Width = 1049
        Height = 512
        Align = alClient
        ColCount = 6
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing, goRowMoving, goColMoving, goEditing, goTabs, goAlwaysShowEditor]
        TabOrder = 0
        OnDrawCell = sgtabelasDrawCell
        ColWidths = (
          135
          37
          42
          48
          61
          87)
        RowHeights = (
          24
          24)
      end
    end
    object tsGeraTeste: TTabSheet
      Caption = 'Gera &Teste'
      ImageIndex = 2
      object GroupBox3: TGroupBox
        Left = 8
        Top = 8
        Width = 625
        Height = 65
        Caption = ' Gera arquivos de Teste '
        TabOrder = 0
        object ePath: TEdit
          Left = 8
          Top = 24
          Width = 289
          Height = 21
          TabOrder = 0
          Text = 'D:\Projetos'
        end
        object eArq: TEdit
          Left = 304
          Top = 24
          Width = 217
          Height = 21
          TabOrder = 1
          Text = 'CadUsuario'
        end
        object Button2: TButton
          Left = 528
          Top = 22
          Width = 75
          Height = 25
          Caption = '&Gera'
          TabOrder = 2
          OnClick = Button2Click
        end
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 80
        Width = 625
        Height = 65
        Caption = ' Clona cadastro em Tela '
        TabOrder = 1
        object eCaminhoSQL: TEdit
          Left = 8
          Top = 20
          Width = 289
          Height = 21
          TabOrder = 0
          Text = 'D:\Projetos\Cadastros\'
        end
        object eArqSQL: TEdit
          Left = 304
          Top = 20
          Width = 217
          Height = 21
          TabOrder = 1
          Text = 'CadUsuario'
        end
        object Button3: TButton
          Left = 526
          Top = 16
          Width = 75
          Height = 25
          Caption = '&Clona'
          TabOrder = 2
          OnClick = Button3Click
        end
      end
    end
    object tsFormataSql: TTabSheet
      Caption = 'Formatar &SQL'
      ImageIndex = 3
      DesignSize = (
        1049
        512)
      object lFormatando: TLabel
        Left = 688
        Top = 143
        Width = 209
        Height = 20
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Formatando SQL ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        ExplicitWidth = 158
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 163
        Width = 1049
        Height = 349
        Align = alBottom
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = ' SQL formatado '
        TabOrder = 0
        object mSqlFormatado: TRichEdit
          Left = 2
          Top = 15
          Width = 1045
          Height = 332
          Align = alClient
          HideSelection = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 0
          WantTabs = True
          Zoom = 100
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1049
        Height = 139
        Align = alTop
        Caption = ' SQL '
        TabOrder = 1
        object mSql: TRichEdit
          Left = 2
          Top = 15
          Width = 1045
          Height = 122
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
          WantTabs = True
          Zoom = 100
        end
      end
      object btn_FormatarSql: TButton
        Left = 896
        Top = 143
        Width = 154
        Height = 24
        Anchors = [akLeft, akTop, akRight]
        Caption = '&Formatar'
        TabOrder = 2
        OnClick = btn_FormatarSqlClick
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Par'#226'metros de Sistema'
      ImageIndex = 4
      ExplicitWidth = 692
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 1049
        Height = 512
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 692
        object TabSheet5: TTabSheet
          Caption = 'Configura'#231#245'es de Conex'#227'o'
          ImageIndex = 1
          DesignSize = (
            1041
            484)
          object mConexao: TMemo
            Left = 0
            Top = 0
            Width = 1041
            Height = 453
            Align = alTop
            Anchors = [akLeft, akTop, akRight, akBottom]
            Color = clMoneyGreen
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Times New Roman'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object btnSalvar: TBitBtn
            Left = 0
            Top = 457
            Width = 75
            Height = 25
            Anchors = [akLeft, akBottom]
            Caption = 'Salvar'
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333333333333333330000333333333333333333333333F33333333333
              00003333344333333333333333388F3333333333000033334224333333333333
              338338F3333333330000333422224333333333333833338F3333333300003342
              222224333333333383333338F3333333000034222A22224333333338F338F333
              8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
              33333338F83338F338F33333000033A33333A222433333338333338F338F3333
              0000333333333A222433333333333338F338F33300003333333333A222433333
              333333338F338F33000033333333333A222433333333333338F338F300003333
              33333333A222433333333333338F338F00003333333333333A22433333333333
              3338F38F000033333333333333A223333333333333338F830000333333333333
              333A333333333333333338330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
            TabOrder = 1
            OnClick = btnSalvarClick
          end
          object btnAtualizar: TBitBtn
            Left = 88
            Top = 457
            Width = 75
            Height = 25
            Anchors = [akLeft, akBottom]
            Caption = 'Atualizar'
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
              33333333333F8888883F33330000324334222222443333388F3833333388F333
              000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
              F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
              223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
              3338888300003AAAAAAA33333333333888888833333333330000333333333333
              333333333333333333FFFFFF000033333333333344444433FFFF333333888888
              00003A444333333A22222438888F333338F3333800003A2243333333A2222438
              F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
              22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
              33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
              3333333333338888883333330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
            TabOrder = 2
            OnClick = btnAtualizarClick
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Informa'#231#245'es e Valores'
          ExplicitWidth = 684
          object Panel1: TPanel
            Left = 0
            Top = 179
            Width = 1041
            Height = 305
            Align = alClient
            Alignment = taLeftJustify
            TabOrder = 0
            ExplicitWidth = 684
            object Splitter1: TSplitter
              Left = 488
              Top = 1
              Height = 303
              ExplicitHeight = 223
            end
            object GroupBox7: TGroupBox
              Left = 1
              Top = 1
              Width = 487
              Height = 303
              Align = alLeft
              Caption = 'Par'#226'metros que existem apenas localmente:'
              TabOrder = 0
              DesignSize = (
                487
                303)
              object DBGrid3: TDBGrid
                Left = 2
                Top = 29
                Width = 483
                Height = 272
                Align = alBottom
                Anchors = [akLeft, akTop, akBottom]
                DataSource = dsLocal
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDrawColumnCell = DBGrid3DrawColumnCell
                OnDblClick = DBGrid3DblClick
                OnTitleClick = DBGrid3TitleClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'marcado'
                    Title.Caption = 'Sel.'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'i_cdparametro'
                    Title.Caption = 'C'#243'd.Param.'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'c_nome'
                    Title.Caption = 'Nome'
                    Width = 111
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'c_descricao'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 164
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'c_valor'
                    Title.Caption = 'Valor'
                    Width = 74
                    Visible = True
                  end>
              end
              object btnInsereLocal: TBitBtn
                Left = 359
                Top = 10
                Width = 122
                Height = 25
                Hint = 'O Par'#226'metro escolhido ser'#225' inserido no Banco de Dados Remoto.'
                Anchors = [akTop, akRight]
                Caption = 'Inserir Par'#226'metro'
                Glyph.Data = {
                  DE010000424DDE01000000000000760000002800000024000000120000000100
                  0400000000006801000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                  3333333333333333333333330000333333333333333333333333F33333333333
                  00003333344333333333333333388F3333333333000033334224333333333333
                  338338F3333333330000333422224333333333333833338F3333333300003342
                  222224333333333383333338F3333333000034222A22224333333338F338F333
                  8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
                  33333338F83338F338F33333000033A33333A222433333338333338F338F3333
                  0000333333333A222433333333333338F338F33300003333333333A222433333
                  333333338F338F33000033333333333A222433333333333338F338F300003333
                  33333333A222433333333333338F338F00003333333333333A22433333333333
                  3338F38F000033333333333333A223333333333333338F830000333333333333
                  333A333333333333333338330000333333333333333333333333333333333333
                  0000}
                NumGlyphs = 2
                TabOrder = 1
                OnClick = btnInsereLocalClick
              end
            end
            object Panel2: TPanel
              Left = 491
              Top = 1
              Width = 549
              Height = 303
              Align = alClient
              TabOrder = 1
              ExplicitWidth = 192
              object GroupBox6: TGroupBox
                Left = 1
                Top = 1
                Width = 547
                Height = 301
                Align = alClient
                Caption = 'Par'#226'metros que existem apenas no banco conectado:'
                TabOrder = 0
                ExplicitWidth = 190
                DesignSize = (
                  547
                  301)
                object DBGrid2: TDBGrid
                  Left = 2
                  Top = 27
                  Width = 543
                  Height = 272
                  Align = alBottom
                  Anchors = [akTop, akRight, akBottom]
                  DataSource = dsRemoteGridConectado
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 0
                  TitleFont.Charset = ANSI_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDrawColumnCell = DBGrid2DrawColumnCell
                  OnDblClick = DBGrid2DblClick
                  OnTitleClick = DBGrid2TitleClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'marcado'
                      Title.Caption = 'Sel.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'i_cdparametro'
                      Title.Caption = 'C'#243'd.Param.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'c_nome'
                      Title.Caption = 'Nome'
                      Width = 115
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'c_descricao'
                      Title.Caption = 'Descri'#231#227'o'
                      Width = 156
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'c_valor'
                      Title.Caption = 'Valor'
                      Width = 79
                      Visible = True
                    end>
                end
                object btnInsereRemote: TBitBtn
                  Left = 427
                  Top = 8
                  Width = 122
                  Height = 25
                  Hint = 'O Par'#226'metro escolhido ser'#225' inserido no Banco de Dados Local.'
                  Anchors = [akTop, akRight]
                  Caption = 'Inserir Par'#226'metro'
                  Glyph.Data = {
                    DE010000424DDE01000000000000760000002800000024000000120000000100
                    0400000000006801000000000000000000001000000000000000000000000000
                    80000080000000808000800000008000800080800000C0C0C000808080000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                    3333333333333333333333330000333333333333333333333333F33333333333
                    00003333344333333333333333388F3333333333000033334224333333333333
                    338338F3333333330000333422224333333333333833338F3333333300003342
                    222224333333333383333338F3333333000034222A22224333333338F338F333
                    8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
                    33333338F83338F338F33333000033A33333A222433333338333338F338F3333
                    0000333333333A222433333333333338F338F33300003333333333A222433333
                    333333338F338F33000033333333333A222433333333333338F338F300003333
                    33333333A222433333333333338F338F00003333333333333A22433333333333
                    3338F38F000033333333333333A223333333333333338F830000333333333333
                    333A333333333333333338330000333333333333333333333333333333333333
                    0000}
                  NumGlyphs = 2
                  TabOrder = 1
                  OnClick = btnInsereRemoteClick
                  ExplicitLeft = 70
                end
              end
            end
          end
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 1041
            Height = 179
            Align = alTop
            TabOrder = 1
            ExplicitWidth = 684
            object Splitter2: TSplitter
              Left = 1
              Top = 175
              Width = 1039
              Height = 3
              Cursor = crVSplit
              Align = alBottom
              ExplicitWidth = 988
            end
            object GroupBox5: TGroupBox
              Left = 1
              Top = 1
              Width = 1039
              Height = 174
              Align = alClient
              Caption = 'Par'#226'metros que existem nos dois bancos com valores diferentes:'
              TabOrder = 0
              ExplicitWidth = 682
              DesignSize = (
                1039
                174)
              object DBGrid1: TDBGrid
                Left = 2
                Top = 36
                Width = 1035
                Height = 136
                Align = alBottom
                DataSource = dsGridValores
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnCellClick = DBGrid1CellClick
                OnDrawColumnCell = DBGrid1DrawColumnCell
                OnDblClick = DBGrid1DblClick
                OnMouseUp = DBGrid1MouseUp
                OnTitleClick = DBGrid1TitleClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'i_cdparametro_local'
                    Title.Caption = 'C'#243'd.Param.'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'c_nome_local'
                    Title.Caption = 'Nome'
                    Width = 204
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'c_descricao_local'
                    Title.Caption = 'Descri'#231#227'o'
                    Width = 249
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'marcadolocal'
                    Title.Caption = 'Sel.'
                    Width = 39
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'c_valor_local'
                    Title.Caption = 'Valor Local'
                    Width = 141
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'marcadoremote'
                    Title.Caption = 'Sel.'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'c_valor_remote'
                    Title.Caption = 'Valor Remoto'
                    Width = 198
                    Visible = True
                  end>
              end
              object btnAlteraValor: TBitBtn
                Left = 795
                Top = 8
                Width = 158
                Height = 26
                Hint = 'O Par'#226'metro ser'#225' alterado no Banco de Dados selecionado.'
                Anchors = [akTop, akRight]
                Caption = 'Alterar Valor Par'#226'metro'
                Glyph.Data = {
                  F2010000424DF201000000000000760000002800000024000000130000000100
                  0400000000007C01000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333334433333
                  3333333333388F3333333333000033334224333333333333338338F333333333
                  0000333422224333333333333833338F33333333000033422222243333333333
                  83333338F3333333000034222A22224333333338F33F33338F33333300003222
                  A2A2224333333338F383F3338F33333300003A2A222A222433333338F8333F33
                  38F33333000034A22222A22243333338833333F3338F333300004222A2222A22
                  2433338F338F333F3338F3330000222A3A2224A22243338F3838F338F3338F33
                  0000A2A333A2224A2224338F83338F338F3338F300003A33333A2224A2224338
                  333338F338F3338F000033333333A2224A2243333333338F338F338F00003333
                  33333A2224A2233333333338F338F83300003333333333A2224A333333333333
                  8F338F33000033333333333A222433333333333338F338F30000333333333333
                  A224333333333333338F38F300003333333333333A223333333333333338F8F3
                  000033333333333333A3333333333333333383330000}
                NumGlyphs = 2
                TabOrder = 1
                OnClick = btnAlteraValorClick
                ExplicitLeft = 438
              end
              object BitBtn2: TBitBtn
                Left = 965
                Top = 8
                Width = 75
                Height = 26
                Anchors = [akTop, akRight]
                Caption = 'Buscar'
                Glyph.Data = {
                  DE010000424DDE01000000000000760000002800000024000000120000000100
                  0400000000006801000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
                  33333333333F8888883F33330000324334222222443333388F3833333388F333
                  000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
                  F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
                  223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
                  3338888300003AAAAAAA33333333333888888833333333330000333333333333
                  333333333333333333FFFFFF000033333333333344444433FFFF333333888888
                  00003A444333333A22222438888F333338F3333800003A2243333333A2222438
                  F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
                  22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
                  33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
                  3333333333338888883333330000333333333333333333333333333333333333
                  0000}
                NumGlyphs = 2
                TabOrder = 2
                OnClick = BitBtn2Click
                ExplicitLeft = 608
              end
            end
          end
        end
      end
    end
  end
  object sdsLocal: TFDQuery
    MasterSource = dsLocal
    SQL.Strings = (
      
        'select cast('#39#39' as varchar(1)) as marcado,c_nome,i_cdparametro,ca' +
        'st(c_descricao as varchar(200)) as c_descricao,c_valor,f_tipo fr' +
        'om parametro')
    Left = 469
    Top = 405
  end
  object dspLocal: TDataSetProvider
    DataSet = sdsLocal
    Options = [poAllowCommandText]
    Left = 437
    Top = 405
  end
  object cdsLocal: TClientDataSet
    Tag = 1
    Aggregates = <>
    CommandText = 
      'select cast('#39#39' as varchar(1)) as marcado,c_nome,i_cdparametro,ca' +
      'st(c_descricao as varchar(200)) as c_descricao,c_valor,f_tipo fr' +
      'om parametro'
    Params = <>
    ProviderName = 'dspLocal'
    Left = 405
    Top = 405
    object cdsLocalc_nome: TStringField
      FieldName = 'c_nome'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 25
    end
    object cdsLocali_cdparametro: TIntegerField
      FieldName = 'i_cdparametro'
    end
    object cdsLocalc_valor: TStringField
      FieldName = 'c_valor'
      Size = 80
    end
    object cdsLocalc_descricao: TStringField
      FieldName = 'c_descricao'
      Size = 200
    end
    object cdsLocalmarcado: TStringField
      FieldName = 'marcado'
      Size = 1
    end
    object cdsLocalf_tipo: TStringField
      FieldName = 'f_tipo'
      FixedChar = True
      Size = 1
    end
  end
  object dsLocal: TDataSource
    DataSet = cdsLocal
    Left = 373
    Top = 405
  end
  object dsRemote: TDataSource
    DataSet = cdsRemote
    Left = 373
    Top = 438
  end
  object cdsRemote: TClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRemote'
    Left = 405
    Top = 438
  end
  object dspRemote: TDataSetProvider
    DataSet = sdsRemote
    Options = [poAllowCommandText]
    Left = 437
    Top = 438
  end
  object sdsRemote: TFDQuery
    MasterSource = dsRemote
    Left = 469
    Top = 438
  end
  object dsLocalGridConectado: TDataSource
    DataSet = cdsLocalGridConectado
    Left = 872
    Top = 402
  end
  object cdsLocalGridConectado: TClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    ProviderName = 'dspLocalGridConectado'
    Left = 904
    Top = 402
  end
  object dspLocalGridConectado: TDataSetProvider
    DataSet = sdsLocalGridConectado
    Options = [poAllowCommandText]
    Left = 936
    Top = 402
  end
  object sdsLocalGridConectado: TFDQuery
    MasterSource = dsLocalGridConectado
    SQL.Strings = (
      
        'select c_nome,i_cdparametro,cast(c_descricao as varchar(200)) as' +
        ' c_descricao,c_valor from parametro')
    Left = 968
    Top = 402
  end
  object sdsRemoteGridConectado: TFDQuery
    MasterSource = dsRemoteGridConectado
    SQL.Strings = (
      
        'select cast('#39#39' as varchar(1)) as marcado,c_nome,i_cdparametro,ca' +
        'st(c_descricao as varchar(200)) as c_descricao,c_valor,f_tipo fr' +
        'om parametro')
    Left = 968
    Top = 435
  end
  object dspRemoteGridConectado: TDataSetProvider
    DataSet = sdsRemoteGridConectado
    Options = [poAllowCommandText]
    Left = 936
    Top = 435
  end
  object cdsRemoteGridConectado: TClientDataSet
    Tag = 1
    Aggregates = <>
    CommandText = 
      'select cast('#39#39' as varchar(1)) as marcado,c_nome,i_cdparametro,ca' +
      'st(c_descricao as varchar(200)) as c_descricao,c_valor,f_tipo fr' +
      'om parametro'
    Params = <>
    ProviderName = 'dspRemoteGridConectado'
    Left = 904
    Top = 435
    object cdsRemoteGridConectadoc_nome: TStringField
      FieldName = 'c_nome'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 25
    end
    object cdsRemoteGridConectadoi_cdparametro: TIntegerField
      FieldName = 'i_cdparametro'
    end
    object cdsRemoteGridConectadoc_descricao: TStringField
      FieldName = 'c_descricao'
      Size = 200
    end
    object cdsRemoteGridConectadoc_valor: TStringField
      FieldName = 'c_valor'
      Size = 80
    end
    object cdsRemoteGridConectadomarcado: TStringField
      FieldName = 'marcado'
      Size = 1
    end
    object cdsRemoteGridConectadof_tipo: TStringField
      FieldName = 'f_tipo'
      FixedChar = True
      Size = 1
    end
  end
  object dsRemoteGridConectado: TDataSource
    DataSet = cdsRemoteGridConectado
    Left = 872
    Top = 435
  end
  object cdsGridValores: TClientDataSet
    Tag = 1
    Aggregates = <>
    CommandText = 
      'select 0 as i_cdparametro_local,cast('#39#39' as varchar(25)) as c_nom' +
      'e_local, cast('#39#39' as varchar(80)) as c_valor_local,'#13#10'cast('#39#39' as v' +
      'archar(80)) as c_valor_remote,cast('#39#39' as varchar(200)) as c_desc' +
      'ricao_local,cast('#39#39' as varchar(1)) as marcadolocal,cast('#39#39' as va' +
      'rchar(1)) as f_tipo,'#13#10'cast('#39#39' as varchar(1)) as marcadoremote'#13#10'f' +
      'rom pedidovenda limit 1'
    Params = <>
    ProviderName = 'dspGridValores'
    Left = 904
    Top = 158
    object cdsGridValoresi_cdparametro_local: TIntegerField
      FieldName = 'i_cdparametro_local'
    end
    object cdsGridValoresc_nome_local: TStringField
      FieldName = 'c_nome_local'
      Size = 25
    end
    object cdsGridValoresc_valor_local: TStringField
      FieldName = 'c_valor_local'
      Size = 80
    end
    object cdsGridValoresc_valor_remote: TStringField
      FieldName = 'c_valor_remote'
      Size = 80
    end
    object cdsGridValoresc_descricao_local: TStringField
      FieldName = 'c_descricao_local'
      Size = 200
    end
    object cdsGridValoresmarcadolocal: TStringField
      FieldName = 'marcadolocal'
      Size = 1
    end
    object cdsGridValoresmarcadoremote: TStringField
      FieldName = 'marcadoremote'
      Size = 1
    end
    object cdsGridValoresf_tipo: TStringField
      FieldName = 'f_tipo'
      Size = 1
    end
  end
  object sdsGridValores: TFDQuery
    MasterSource = dsGridValores
    SQL.Strings = (
      
        'select 0 as i_cdparametro_local,cast('#39#39' as varchar(25)) as c_nom' +
        'e_local, cast('#39#39' as varchar(80)) as c_valor_local,'
      
        'cast('#39#39' as varchar(80)) as c_valor_remote,cast('#39#39' as varchar(200' +
        ')) as c_descricao_local,cast('#39#39' as varchar(1)) as marcadolocal,c' +
        'ast('#39#39' as varchar(1)) as f_tipo,'
      'cast('#39#39' as varchar(1)) as marcadoremote'
      'from pedidovenda limit 1')
    Left = 968
    Top = 158
  end
  object dspGridValores: TDataSetProvider
    DataSet = sdsGridValores
    Options = [poAllowCommandText]
    Left = 936
    Top = 158
  end
  object dsGridValores: TDataSource
    DataSet = cdsGridValores
    Left = 872
    Top = 158
  end
  object cdsParamRemote: TClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    ProviderName = 'dspParamRemote'
    Left = 904
    Top = 222
  end
  object cdsParamLocal: TClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    ProviderName = 'dspParamLocal'
    Left = 904
    Top = 190
  end
  object dspParamRemote: TDataSetProvider
    DataSet = sdsParamRemote
    Options = [poAllowCommandText]
    Left = 936
    Top = 222
  end
  object dspParamLocal: TDataSetProvider
    DataSet = sdsParamLocal
    Options = [poAllowCommandText]
    Left = 936
    Top = 190
  end
  object sdsParamRemote: TFDQuery
    Left = 968
    Top = 222
  end
  object sdsParamLocal: TFDQuery
    Left = 968
    Top = 190
  end
  object cdsInsereRemote: TClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    ProviderName = 'dspInsereRemote'
    Left = 384
    Top = 355
  end
  object dspInsereRemote: TDataSetProvider
    DataSet = sdsInsereRemote
    Options = [poAllowCommandText]
    Left = 416
    Top = 355
  end
  object sdsInsereRemote: TFDQuery
    Left = 448
    Top = 355
  end
  object SQLConnectionRemote: TFDConnection
    Params.Strings = (
      'DriverID=PG')
    Left = 928
    Top = 56
  end
end
