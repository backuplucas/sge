unit U_BuscaPedCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U_Padrao, StdCtrls, Buttons, ExtCtrls, ComCtrls, Grids, DBGrids,
  FMTBcd, DB, DBClient, Provider, SqlExpr, SLClientDataSet,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.AppEvnts;

type
  TF_BuscaPedCompra = class(TF_Padrao)
    Panel1: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBGrid1: TDBGrid;
    sdsPedList: TFDQuery;
    dspPedList: TDataSetProvider;
    cdsPedList: TClientDataSet;
    dsPedList: TDataSource;
    cdsPedListi_cdpedidocompra: TIntegerField;
    cdsPedListi_cdparceiro: TIntegerField;
    cdsItemPedido: TClientDataSet;
    dspItemPedido: TDataSetProvider;
    sdsItemPedido: TFDQuery;
    cdsSalvaItem: TSLClientDataSet;
    dspSalvaItem: TDataSetProvider;
    sdsSalvaItem: TFDQuery;
    cdsSalvaNfbkp: TSLClientDataSet;
    dspSalvaNfbkp: TDataSetProvider;
    sdsSalvaNfbkp: TFDQuery;
    cdsVerificaNfBkp: TClientDataSet;
    dspVerificaNfBkp: TDataSetProvider;
    sdsVerificaNfBkp: TFDQuery;
    sdsVerificaItem: TFDQuery;
    dspVerificaItem: TDataSetProvider;
    cdsVerificaItem: TClientDataSet;
    cdsPedListn_totalpedido: TBCDField;
    cdsPedListc_nome: TWideStringField;
    procedure BitBtn2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    Procedure CarregaNota;
    Function VerificaNfBkp(vI_CdPedidoCompra: Integer): Integer;
    Function VerificaItem(vI_CdProduto: Integer): Integer;
  public
    vI_CdNfBkp: Integer;
  end;

var
  F_BuscaPedCompra: TF_BuscaPedCompra;

implementation

{$R *.dfm}

uses dm, U_entrada;

procedure TF_BuscaPedCompra.BitBtn2Click(Sender: TObject);
begin
  inherited;
  CarregaNota;
end;

procedure TF_BuscaPedCompra.CarregaNota;
var
  vI_CdNfBkpVinc: Integer;
  vI_CdItemNfBkp: Integer;

  TD: TTransactionDesc;
begin
  try
    if not(DMSGE.SQLConnection.InTransaction) then begin
      TD.TransactionID := 1;
      TD.IsolationLevel := xilREADCOMMITTED;
      DMSGE.SQLConnection.StartTransaction();
      cdsSalvaNfbkp.RequestFindID(vI_CdNfBkp);

      if cdsSalvaNfbkp.RecordCount = 0 then
        Abort;

      vI_CdNfBkpVinc := VerificaNfBkp(cdsPedListi_cdpedidocompra.AsInteger);

      if vI_CdNfBkpVinc > 0 then begin
        if vI_CdNfBkpVinc = vI_CdNfBkp then
          MessageDlg('Complemento j� vinculado!', mtWarning, [mbOK], 0)
        else
          MessageDlg('Existe complemento vinculado a pedido compra. Complemento ' + IntToStr(vI_CdNfBkpVinc) + '.', mtWarning, [mbOK], 0);

        Abort;
      end;

      cdsSalvaNfbkp.AddValue('i_cdparceiro', cdsPedListi_cdparceiro.AsInteger, ftInteger);
      cdsSalvaNfbkp.AddValue('n_vlrprodutos', cdsPedListn_totalpedido.AsFloat, ftFloat);
      cdsSalvaNfbkp.AddValue('i_cdsequencial_pc', cdsPedListi_cdpedidocompra.AsInteger, ftInteger);
      cdsSalvaNfbkp.RequestSave;

      cdsItemPedido.Close;
      cdsItemPedido.Params.ParamByName('I_CdPedidoCompra').AsInteger := cdsPedListi_cdpedidocompra.AsInteger;
      cdsItemPedido.Open;

      if cdsItemPedido.RecordCount > 0 then begin
        cdsItemPedido.First;

        while not(cdsItemPedido.Eof) do begin
          vI_CdItemNfBkp := VerificaItem(cdsItemPedido.FieldByName('i_cdproduto').AsInteger);

          if vI_CdItemNfBkp > 0 then
            cdsSalvaItem.RequestFindID(vI_CdItemNfBkp)
          else
            cdsSalvaItem.RequestClear;

          cdsSalvaItem.AddValue('i_cdnfbkp', vI_CdNfBkp, ftInteger);
          cdsSalvaItem.AddValue('c_cdfornec', cdsItemPedido.FieldByName('c_cdprodfornec').AsString, ftString);
          cdsSalvaItem.AddValue('i_cdproduto', cdsItemPedido.FieldByName('i_cdproduto').AsInteger, ftInteger);
          cdsSalvaItem.AddValue('n_prcunitario', cdsItemPedido.FieldByName('n_prcunitario').AsFloat, ftFloat);
          cdsSalvaItem.AddValue('n_quantidade', cdsItemPedido.FieldByName('n_quantidade').AsFloat, ftFloat);
          cdsSalvaItem.AddValue('i_cdorigem', cdsItemPedido.FieldByName('i_cditempedidocompra').AsInteger, ftInteger);
          cdsSalvaItem.RequestSave;

          cdsItemPedido.Next;
        end;
      end;

      if not(F_Entrada.cdsNfBkp.State in [dsinsert, dsedit]) then
        F_Entrada.cdsNfBkp.Edit;

      F_Entrada.ePedCompra.Field.Value := cdsPedListi_cdpedidocompra.AsInteger;
      DMSGE.SQLConnection.Commit();
    end;
  except
    on E: Exception do begin
      DMSGE.SQLConnection.Rollback();
      raise;
      Exit;
    end;
  end;
end;

procedure TF_BuscaPedCompra.DBGrid1DblClick(Sender: TObject);
begin
  inherited;
  CarregaNota;
  ModalResult := mrOK;
end;

function TF_BuscaPedCompra.VerificaItem(vI_CdProduto: integer): integer;
begin
  cdsVerificaItem.Close;
  cdsVerificaItem.CommandText :=
    ' select ' +
    '   i_cditemnfbkp ' +
    ' from ' +
    '   itemnfbkp ' +
    ' where ' +
    '   i_cdproduto = ' + IntToStr(vI_CdProduto) + ' and ' +
    '   i_cdnfbkp = ' + IntToStr(vI_CdNfBkp);
  cdsVerificaItem.Open;

  Result := cdsVerificaItem.FieldByName('i_cditemnfbkp').AsInteger;
end;

function TF_BuscaPedCompra.VerificaNfBkp(
  vI_CdPedidoCompra: integer): integer;
begin
  cdsVerificaNfBkp.Close;
  cdsVerificaNfBkp.CommandText := ' select i_cdnfbkp from nfbkp where i_cdsequencial_pc = ' + IntToStr(vI_CdPedidoCompra);
  cdsVerificaNfBkp.Open;

  Result := cdsVerificaNfBkp.FieldByName('i_cdnfbkp').AsInteger;
end;

end.
