program SGE;



uses
  Forms,
  Menu in 'Menu.pas' {F_Menu},
  U_entrada in 'U_entrada.pas' {F_Entrada},
  dm in 'dm.pas' {dmSGE: TDataModule},
  U_BuscaNota in 'U_BuscaNota.pas' {F_BuscaNotaFiscal},
  U_ConsulProdEntrada in 'U_ConsulProdEntrada.pas' {F_ConsulProdEntrada},
  U_Padrao in 'U_Padrao.pas' {F_Padrao},
  U_PadraoCadastro in 'U_PadraoCadastro.pas' {F_PadraoCadastro},
  UDBFindForm in 'UDBFindForm.pas' {SlDBLookupForm},
  SlDBFind in 'SlDBFind.pas',
  pgeLicense in 'PgExpress\pgeLicense.pas',
  U_BuscaPedCompra in 'U_BuscaPedCompra.pas' {F_BuscaPedCompra},
  U_RelPadrao in 'U_RelPadrao.pas' {F_RelPadrao},
  U_RelEntrada in 'U_RelEntrada.pas' {F_RelEntrada};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmSGE, dmSGE);
  Application.CreateForm(TF_Menu, F_Menu);
  Application.Run;
end.
