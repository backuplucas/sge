unit U_ConsulProdEntrada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U_PadraoCadastro, FMTBcd, SlDBFind, DB, SqlExpr, Menus, ImgList,
  ComCtrls, ToolWin, ExtCtrls, Provider, DBClient, SLClientDataSet, Grids,
  DBGrids, StdCtrls, Mask, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.ImageList, Vcl.AppEvnts;

type
  TF_ConsulProdEntrada = class(TF_PadraoCadastro)
    Panel1: TPanel;
    cdsProduto: TSLClientDataSet;
    dspProduto: TDataSetProvider;
    sdsProduto: TFDQuery;
    eCdProd: TSlDBEdit;
    SlDBEdit2: TSlDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    cbFat: TCheckBox;
    eDataInicial: TSLDate;
    eDataFinal: TSLDate;
    Label3: TLabel;
    Label4: TLabel;
    lblReg: TLabel;
    eCdEmp: TSlDBEdit;
    eEmp: TSLEdit;
    dsEmpresa: TDataSource;
    cdsEmpresa: TSLClientDataSet;
    dspEmpresa: TDataSetProvider;
    sdsEmpresa: TFDQuery;
    Label5: TLabel;
    Label6: TLabel;
    sdsItemNfBkp: TFDQuery;
    dspItemNfBkp: TDataSetProvider;
    cdsItemNfBkp: TSLClientDataSet;
    dsItemNfBkp: TDataSource;
    cdsItemNfBkpi_cdnfbkp: TIntegerField;
    cdsItemNfBkpd_cadastro: TSQLTimeStampField;
    cdsItemNfBkpi_cditemnfbkp: TIntegerField;
    cdsItemNfBkpn_quantidade: TBCDField;
    cdsItemNfBkpn_prcunitario: TBCDField;
    cdsItemNfBkpf_fechado: TWideStringField;
    cdsItemNfBkpi_cdproduto: TIntegerField;
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure Btn_IgualClick(Sender: TObject);
    procedure cdsProdutoAfterSql(Sender: TObject);
    procedure eCdProdExit(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure Btn_LimparClick(Sender: TObject);
    procedure eCdEmpExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cdsEmpresaAfterSql(Sender: TObject);
    procedure Btn_ProcuraClick(Sender: TObject);
    procedure eCdEmpEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure AtualizaGrid;
    Procedure BuscaEmpresa;
  end;

var
  F_ConsulProdEntrada: TF_ConsulProdEntrada;

implementation

Uses Dm, U_Funcoes, U_Entrada;

{$R *.dfm}

procedure TF_ConsulProdEntrada.DBGrid1TitleClick(Column: TColumn);
begin
  inherited;
  cdsItemNfBkp.IndexFieldNames := Column.FieldName;
end;

procedure TF_ConsulProdEntrada.Btn_IgualClick(Sender: TObject);
begin
  if not(eCdProd.Focused) then begin
    eCdProd.FindNow;
    cdsProduto.RequestFind;
  end;

  inherited;
end;

procedure TF_ConsulProdEntrada.AtualizaGrid;
var
  Sql: string;
begin
  if (eDataInicial.Text <> '') and (eDataFinal.Text <> '') then begin
    Sql :=
      ' select ' +
      '   ItemNfBkp.i_cdnfbkp, ' +
      '   ItemNfBkp.n_quantidade, ' +
      '   ItemNfBkp.n_prcunitario, ' +
      '   nfbkp.D_cadastro, ' +
      '   nfbkp.f_fechado, ' +
      '   ItemNfBkp.I_CdProduto, ' +
      '   ItemNfBkp.I_CdItemNfBkp ' +
      ' from ' +
      '   ItemNfBkp ' +
      ' join nfbkp on nfbkp.i_cdnfbkp = ItemNfBkp.i_cdnfbkp ' +
      ' where ' +
      '   date(nfbkp.d_cadastro) <= :D_Final and ' +
      '   date(nfbkp.d_cadastro) >= :D_Inicial and ' +
      '   ItemNfBkp.i_cdProduto = :i_cdProduto ';

    if cbFat.Checked then
      Sql := Sql + ' and NfBkp.F_Fechado = ''S'' ';

    if eCdEmp.Text <> '' Then
      Sql := Sql + ' and NfBkp.I_CdEmpresa = :I_CdEmpresa ';

    Sql := Sql + ' order by ItemNfBkp.I_CdItemNfBkp ';

    cdsItemNfBkp.Close;
    cdsItemNfBkp.CommandText := Sql;
    cdsItemNfBkp.Params.ParamByName('D_Inicial').AsDate := StrToDate(eDataInicial.Text);
    cdsItemNfBkp.Params.ParamByName('D_Final').AsDate := StrToDate(eDataFinal.Text);
    cdsItemNfBkp.Params.ParamByName('I_CdProduto').AsInteger := cdsProduto.Field_id;

    if eCdEmp.Text <> '' then
      cdsItemNfBkp.Params.ParamByName('I_CdEmpresa').AsInteger := StrToInt(eCdEmp.Text);
    cdsItemNfBkp.Open;
  end;

  lblReg.Caption := 'Registro: ' + IntToStr(cdsItemNfBkp.RecordCount);
end;

procedure TF_ConsulProdEntrada.cdsProdutoAfterSql(Sender: TObject);
begin
  inherited;
  AtualizaGrid;
end;

procedure TF_ConsulProdEntrada.eCdProdExit(Sender: TObject);
begin
  inherited;
  AtualizaGrid;
end;

procedure TF_ConsulProdEntrada.DBGrid1DblClick(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TF_Entrada, F_Entrada);
  F_Entrada.cdsNfBkp.RequestFindID(cdsItemNfBkp.FieldByName('I_CdNfBkp').AsInteger);
  F_Entrada.ShowModal;
end;

procedure TF_ConsulProdEntrada.Btn_LimparClick(Sender: TObject);
begin
  inherited;
  cdsEmpresa.RequestClear;
  eDataInicial.Text := '';
  eDataFinal.Text := '';
  cbFat.Checked := False;
end;

procedure TF_ConsulProdEntrada.eCdEmpExit(Sender: TObject);
begin
  inherited;
  if eCdEmp.Text = '' then
    eEmp.Text := ''
  else
    BuscaEmpresa;
end;

procedure TF_ConsulProdEntrada.BuscaEmpresa;
begin
  if eCdEmp.FieldsValues.Count > 0 then
    eEmp.Text := eCdEmp.FieldsValues[2]
  else
    eEmp.Text := '';
end;

procedure TF_ConsulProdEntrada.FormCreate(Sender: TObject);
begin
  inherited;
  cdsEmpresa.RequestClear;
end;

procedure TF_ConsulProdEntrada.cdsEmpresaAfterSql(Sender: TObject);
begin
  inherited;
  eCdEmp.FindNow;
  eCdEmpExit(Sender);
end;

procedure TF_ConsulProdEntrada.Btn_ProcuraClick(Sender: TObject);
begin
  SlDSPesquisa.Connection := sdsProduto.Connection;
  inherited;
end;

procedure TF_ConsulProdEntrada.eCdEmpEnter(Sender: TObject);
begin
  inherited;
  SlDSPesquisa.Connection := sdsProduto.Connection;
end;

end.
