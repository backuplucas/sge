unit dm;

interface

uses
  SysUtils, Classes, DB, SqlExpr, dialogs, U_Funcoes, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Phys.PGDef, FireDAC.Phys.PG, FireDAC.Comp.Client;

type
  TdmSGE = class(TDataModule)
    SQLConnectionBkp: TFDConnection;
    SQLConnection: TFDConnection;
    FDPhysPgDriverLink1: TFDPhysPgDriverLink;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ConexaoDB;
  end;

var
  dmSGE: TdmSGE;

implementation

{$R *.dfm}

procedure TdmSGE.ConexaoDB;
var
  vPath: string;
  Configuracao: TStrings;
begin
  vPath := 'sge.ini';
  Configuracao := TStringList.Create;

  try
    Configuracao.LoadFromFile(vPath);

    dmSGE.SQLConnection.Connected := False;
    dmSGE.SQLConnectionBkp.Connected := False;

    if Configuracao.Count = 6 then begin
      dmSGE.SQLConnection.Params.Values['Database'] := Configuracao[0];
      dmSGE.SQLConnection.Params.Values['Server'] := Configuracao[1];
      dmSGE.SQLConnection.Params.Values['Port'] := Configuracao[2];
      dmSGE.SQLConnection.Params.Values['DriverID'] := 'PG';
      dmSGE.SQLConnectionBkp.Params.Values['Database'] := Configuracao[3];
      dmSGE.SQLConnectionBkp.Params.Values['Server'] := Configuracao[4];
      dmSGE.SQLConnectionBkp.Params.Values['Port'] := Configuracao[5];
      dmSGE.SQLConnectionBkp.Params.Values['DriverID'] := 'PG';
    end
    else begin
      dmSGE.SQLConnection.Params.Values['Database'] := Configuracao[0];
      dmSGE.SQLConnection.Params.Values['Server'] := Configuracao[1];
      dmSGE.SQLConnection.Params.Values['Port'] := Configuracao[2];
      dmSGE.SQLConnection.Params.Values['User_Name'] := Configuracao[3];
      dmSGE.SQLConnection.Params.Values['Password'] := Configuracao[4];
      dmSGE.SQLConnection.Params.Values['DriverID'] := 'PG';
      dmSGE.SQLConnectionBkp.Params.Values['Database'] := Configuracao[5];
      dmSGE.SQLConnectionBkp.Params.Values['Server'] := Configuracao[6];
      dmSGE.SQLConnectionBkp.Params.Values['Port'] := Configuracao[7];
      dmSGE.SQLConnectionBkp.Params.Values['User_Name'] := Configuracao[8];
      dmSGE.SQLConnectionBkp.Params.Values['Password'] := Configuracao[9];
      dmSGE.SQLConnectionBkp.Params.Values['DriverID'] := 'PG';
    end;
  finally
    Configuracao.Free;
  end;
end;

procedure TdmSGE.DataModuleCreate(Sender: TObject);
begin
  ConexaoDB;
  try
    SQLConnection.Connected := True;
  except
    on e: Exception do
      ShowMessage(e.Message);
  end;

  try
    SQLConnectionBkp.Connected := True;
  except
    on e: Exception do
      ShowMessage(e.Message);
  end;
end;

end.

// ipes da esto 192.168.0.228 - SQLConnectionBkp
// 192.168.0.202 - SQLConnection

// ipes da fortaleza 192.168.0.1 - SQLConnectionBkp
// 200.230.52.9  - SQLConnection

// ipes da supr 192.168.1.2 - SQLConnectionBkp
//IP SUPREMA 192.168.1.22  - SQLConnection

// ipes da sempre  10.1.1.24 - SQLConnectionBkp
//IP sempre - 10.1.1.200 SQLConnection

// IPs da confour   10.1.1.37 - SQLConnectionBkp
// IP confour       10.1.1.100 - SQLConnection
