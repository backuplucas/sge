unit U_Funcoes;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, FMTBcd, ComCtrls,
  SLClientDataSet, DBClient, Provider, DBCtrls, Mask, ExtCtrls, DB, SqlExpr, SlDBFind, Menus, ImgList, ToolWin,
  Buttons, Grids, DBGrids, ADODB, ActiveX, Registry, smtpsend, ssl_openssl, mimemess, mimepart, StdCtrls,
  Printers, IdIcmpClient, StrUtils,IdBaseComponent, IdComponent, IdIPWatch,
  FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.ImageList ;
  

type
  TConsisteInscricaoEstadual = function(const Insc, UF: string): Integer; stdcall;

type Tparametro = class(TComponent)
  public
    Fparams: TStrings;
    constructor Create;
    destructor Destroy;
  end;


  TSendMailThread = class(TThread)
  private
    FException: Exception;
    FOwner: TForm;

  public
    OcorreramErros: Boolean;
    Terminado: Boolean;
    smtp: TSMTPSend;
    sFrom: string;
    sTo: string;
    sCC: TStrings;
    slmsg_Lines: TStrings;

    constructor Create(AOwner: TForm);
    destructor Destroy; override;
  protected
    procedure Execute; override;
  end;

  procedure BloquearAcaoUsuario(Bloquea: Boolean);
  function getParametro(Appl: TApplication; Chave: string; Menssagem: Boolean = True): string;
  procedure setParametro(Appl: TApplication; Chave: string; Valor: string);
  function ParametroIndexOfName(Appl: TApplication; valor: string): integer;
  function StrMaskToFloat(vlr: string): Extended;
  function FloatToStrZero(f: Real; Tam, Dec: Integer): string;
  function TrocaStr(SubStrOld, SubStrNew, Str: string): string;
  procedure Posiciona(var Linha: string; Inicio: Integer; Fim: Integer; valor: string; numerico: boolean);
  function BlockInput(fBlockInput: Boolean): DWORD; stdcall; external 'user32.DLL';
  procedure EnableCTRLALTDEL(YesNo: boolean);
  function FunctionDetect(LibName, FuncName: string; var LibPointer: Pointer): Boolean;
  procedure AtualizarArquivo(DirServer: string; Arquivo: string);
  procedure deletarArquivos(Diretorio: string; Arquivo: string; AntesDe: TDateTime);
  procedure CopiarArquivos(DiretorioOrig, DiretorioDest: string; Arquivo: string; AntesDe: TDateTime);
  function IntToStrZero(Inteiro: Integer; Tamanho: Integer): string;
  function validaIE(IE, UF: string; ApenasDigitos: Boolean = True): boolean;
  function AjustString(s: string; Tamanho: Integer; Carac_subs: string; Alig_Right: Boolean = True): string;
  function RetirarCaractere(s: string; Caractere: Char): string;
  function GeraFindTransportadora(x: TSLDBEdit; y: TCustomEdit; Z: TCustomEdit; mensagem: boolean = true): boolean;
  function TiraAcento(sin: string; RetiraCaracNaoEncontrado: Boolean = False): string;
  function CallPkgFromFind(edit: TSlDBEdit): Boolean;
  function AlimentaDescricao(sldbcampo: TSlDBEdit; slcampo: TCustomEdit; fieldvalue: integer = 2): Boolean;
  function Arredondar(Valor: Double; Dec: Integer): Double;
  function Coalesce(vValue,vIfNull: Variant): Variant;
  function diferente(vValue1, vValue2: Variant): String;
  function igual(vValue1, vValue2: Variant): String;
  function maior(vValue1, vValue2: Variant): String;
  function menor(vValue1, vValue2: Variant): String;
  function maior_igual(vValue1, vValue2: Variant): String;
  function menor_igual(vValue1, vValue2: Variant): String;
  function formatarSql(sSql : String) : String;
  function formatarSqlSimples(sSql : String)  : String;
  function CountChar(Key : char; sTexto : String) : integer;
  function initCap( sTexto : String): string;
  function AdicionaZerosAEsquerda(vValor: String; vTamanho: Integer): String;
  function valorPorExtenso(rValor : Real) : String;
  function ping(host: String): boolean;
  procedure EnviarEmail(const sPrograma,
                                  sSmtpHost,
                                  sSmtpPort,
                                  sSmtpUser,
                                  sSmtpPasswd,
                                  sFrom,
                                  sTo,
                                  sAssunto: String;
                                  sMensagem : TStrings;
                                  SSL : Boolean;
                                  sCC: TStrings = nil;
                                  Anexos:TStrings=nil;
                                  PedeConfirma: Boolean = False;
                                  AguardarEnvio: Boolean = False;
                                  NomeRemetente: String = '';
                                  TLS : Boolean = True);

  function GetDefaultPrinterName : String;
  function ValidaEmail(const Email : String): Boolean;
  procedure SalvaSQLFormatado(SQL: string; CaminhoArquivo: string = ''; NomeArquivo : String = 'SQLFormatado.sql');
  function ApenasNumerosNaString (s :string) :string;
  function ApenasNumeros (s :string) :string;
  function FormataTelefone( Telefone :string) :string;
  function BuscaIP(): string;

implementation

uses Math, D_Bds, U_PadraoCadastro;

var vparametro: Tparametro;

function BuscaIP(): string;
var
  r : TIdIPWatch;
begin
  r := TIdIPWatch.Create(nil);
  Result := r.LocalIP;
  r.free;
end;

function ApenasNumerosNaString (s :string) :string;
//Retira qualquer caractere que n�o seja numero
//e elimina zeros a esquerda
//Tulio Vitor - 04/2015
var 
  I: integer;
  Formated : string;
begin
  Formated := '';
  for I := 1 To Length(s) Do
  begin
    if (s[I] in ['0'..'9']) then
    begin
      Formated := Formated + Copy(s, I, 1);
    end;
  end;

  if Pos('0',Formated) = 1 then
    result := Copy(Formated,2,Length(Formated))
  else
    Result := Formated;

end;

function ApenasNumeros (s :string) :string;
//Retira qualquer caractere que n�o seja numero

//Tulio Vitor - 04/2015
var 
  I: integer;
  Formated : string;
begin
  Formated := '';
  for I := 1 To Length(s) Do
  begin
    if (s[I] in ['0'..'9']) then
    begin
      Formated := Formated + Copy(s, I, 1);
    end;
  end;
  Result := Formated;

end;

function FormataTelefone( Telefone :string) :string;
//Formata o numero do telefone passado como parametro de acordo 
//com o tamanho da string
// Tulio Vitor - 04/2015
var
  s,teste: string;
  tam: Integer;
begin
  s  := Trim(ApenasNumerosNaString(Telefone));
  tam:= Length(s);

  case tam of
    8: //Ex: 37212121 = 3721-2121
      begin
        Result:= Copy(s,1,4)+'-'+Copy(s,5,4);
      end;

    9: //Ex: 372121211 = 37212-1211
      begin
        Result:= Copy(s,1,5)+'-'+Copy(s,6,4);
      end;

    10: //Ex: 3237212121 = (32)3721-2121
      begin
        Result:= '('+Copy(s,1,2)+')'+Copy(s,3,4)+'-'+Copy(s,7,4);
      end;

    11: //Ex: 32372121211 = (32)93721-2121
      begin
        Result:= '('+Copy(s,1,2)+')'+Copy(s,3,5)+'-'+Copy(s,8,4);
        teste := Result;
      end;
  else
      Result:= s;
  end;
end;

constructor TSendMailThread.Create(AOwner: TForm);
begin
  FOwner := AOwner;
  smtp := TSMTPSend.Create;
  slmsg_Lines := TStringList.Create;
  sCC := TStringList.Create;
  sFrom := '';
  sTo := '';
  FreeOnTerminate := True;
  inherited Create(True);
end;

destructor TSendMailThread.Destroy;
begin
  slmsg_Lines.Free;
  sCC.Free;
  smtp.Free;
  inherited;
end;

procedure TSendMailThread.Execute;
var
  I: integer;
begin
  inherited;
  try
    Terminado := False;
    try
      if not smtp.Login() then
        raise Exception.Create('SMTP ERROR: Login:' + smtp.EnhCodeString + sLineBreak + smtp.FullResult.Text);
      if not smtp.MailFrom(sFrom, Length(sFrom)) then
        raise Exception.Create('SMTP ERROR: MailFrom:' + smtp.EnhCodeString + sLineBreak + smtp.FullResult.Text);
      if not smtp.MailTo(sTo) then
        raise Exception.Create('SMTP ERROR: MailTo:' + smtp.EnhCodeString + sLineBreak + smtp.FullResult.Text);
      if (sCC <> nil) then
      begin
        for I := 0 to sCC.Count - 1 do
        begin
          if not smtp.MailTo(sCC.Strings[i]) then
            raise Exception.Create('SMTP ERROR: MailTo:' + smtp.EnhCodeString + sLineBreak + smtp.FullResult.Text);
        end;
      end;
      if not smtp.MailData(slmsg_Lines) then
        raise Exception.Create('SMTP ERROR: MailData:' + smtp.EnhCodeString + sLineBreak + smtp.FullResult.Text);
      if not smtp.Logout() then
        raise Exception.Create('SMTP ERROR: Logout:' + smtp.EnhCodeString + sLineBreak + smtp.FullResult.Text);
    finally
      try
        smtp.Sock.CloseSocket;
      except
      end;
      Terminado := True;
    end;
  except
    Terminado := True;
  end;
end;


function AlimentaDescricao(sldbcampo: TSlDBEdit; slcampo: TCustomEdit; fieldvalue: integer = 2): Boolean;
begin
  if (sldbcampo.Text = '') or (sldbcampo.FieldsValues.Count = 0) then
    slcampo.Text := ''
  else
    slcampo.Text := sldbcampo.FieldsValues[fieldvalue];
end;

function CallPkgFromFind(edit: TSlDBEdit): Boolean;
var id: integer;
begin
  try
    if edit.FindInformation.ExecuteBpl then
    begin
      edit.FindInformation.ExecuteBpl := False;
      id := F_PadraoCadastro.ExecutaBpl(edit.FindInformation.Bpl);
      if id > 0 then
      begin
        edit.Field.value := id;
        edit.findNow;
        result := true;
      end
      else
        result := false;
    end;
  except
    result := false;
  end;
end;

procedure setParametro(Appl: TApplication; Chave: string; Valor: string);
var posicao: Integer;
begin
  vparametro := Tparametro(Appl.FindComponent('vParametro'));
  Posicao := vparametro.Fparams.IndexOfName(chave);
  if posicao > -1 then
    vparametro.Fparams.Delete(posicao);

  vparametro.Fparams.Add(chave + '=' + valor);
end;

function GeraFindTransportadora(x: TSLDBEdit; y: TCustomEdit; z: TCustomEdit; mensagem: boolean = true): boolean;
begin
  if y.Text <> '' then
  begin
    x.FindInformation.TableName := '(select t.i_cdtransp,t.c_nome'
      + ' from transp_cidade tc'
      + ' join cidade c on c.i_cdcidade = tc.i_cdcidade'
      + ' join transportadora t on t.i_cdtransp = tc.i_cdtransp'
      + ' where tc.i_cdcidade =' + inttostr(strtointdef(z.Text, 0))
      + ' union '
      + ' select t.i_cdtransp ,t.c_nome '
      + ' from cidade c join transportadora t using(i_Cdcidade) '
      + ' where t.i_cdcidade =' + inttostr(strtointdef(z.Text, 0))
      + ' ) as tabela ';
    result := true;
  end
  else
  begin
    x.FindInformation.TableName := '(select t.i_cdtransp,t.c_nome'
      + ' from transp_cidade tc'
      + ' join cidade c on c.i_cdcidade = tc.i_cdcidade'
      + ' join transportadora t on t.i_cdtransp = tc.i_cdtransp'
      + ' where tc.i_cdcidade = -100'
      + ' union '
      + ' select t.i_cdtransp ,t.c_nome '
      + ' from cidade c join transportadora t using(i_Cdcidade) '
      + ' where t.i_cdcidade = -100'
      + ' ) as tabela ';
    if mensagem then
      MessageDlg('Se o campo cidade estiver vazio, voc� n�o poder� cadastrar uma transportadora!', mtWarning, [mbok], 0);
    x.DataSource.DataSet.Edit;
    x.FieldsValues.Clear;
    x.Field.Value := null;
    x.text := '';
    result := false;
  end;

end;

function getParametro(Appl: TApplication; Chave: string; Menssagem: Boolean = True): string;
begin
  vparametro := Tparametro(Appl.FindComponent('vParametro'));
  if vparametro <> nil then
  begin
    if (vparametro.Fparams.Values[Chave] = '') and Menssagem then
      ShowMessage('P�rametro ' + Chave + ' N�o Encontrado!');

    result := vparametro.Fparams.Values[Chave];
  end
  else
    result := '';
end;

function ParametroIndexOfName(Appl: TApplication; valor: string): integer;
begin
  vparametro := Tparametro(Appl.FindComponent('vParametro'));
  result := vparametro.Fparams.IndexOfName(valor);
end;

function StrMaskToFloat(vlr: string): Extended;
var posi: integer;
  Resultado: string;
begin
  resultado := '';
  if Trim(vlr) = '' then
    vlr := '0';
  while Pos('.', vlr) > 0 do
  begin
    posi := Pos('.', vlr);
    resultado := resultado + Copy(vlr, 1, posi - 1);
    vlr := copy(vlr, posi + 1, length(vlr));
  end;
  resultado := resultado + vlr;
  Result := StrToFloat(resultado);
end;

{ Tparametro }

constructor Tparametro.Create;
begin
  Fparams := TStringList.Create;
  Name := 'vParametro';
end;

destructor Tparametro.Destroy;
begin
  Fparams.Free;
end;

function AjustString(s: string; Tamanho: Integer; Carac_subs: string; Alig_Right: Boolean = True): string;
begin
  copy(s, 1, Tamanho);
  if Length(s) < Tamanho then
    while Length(s) < Tamanho do
      if Alig_Right then
        s := Carac_Subs + s
      else
        s := s + Carac_Subs;
  Result := s;
end;

function FloatToStrZero(f: Real; Tam, Dec: Integer): string;
var zeros, zerosdec: string;
  i: Integer;
begin
  for i := 0 to Tam - 1 do
    zeros := zeros + '0';
  for i := 0 to Dec - 1 do
    zerosdec := zerosdec + '0';
  Result := FormatFloat(zeros + '.' + zerosdec, f);
  Result := TrocaStr(',', '', Result);
end;

function TrocaStr(SubStrOld, SubStrNew, Str: string): string;
begin
  while Pos(SubstrOld, Str) > 0 do
  begin
    Str := Copy(Str, 1, Pos(SubStrOld, Str) - 1) + SubStrNew +
      Copy(Str, Pos(SubStrOld, Str) + Length(SubStrOld), Length(Str));
  end;
  Result := Str;
end;

procedure Posiciona(var Linha: string; Inicio, Fim: Integer;
  valor: string; numerico: boolean);
var tam: Integer;
  InicioLinha, FimLinha: string;
begin
  tam := Length(Linha);
  InicioLinha := copy(Linha, 1, Inicio - 1);
  FimLinha := copy(Linha, Fim + 1, tam);
  while Inicio > Length(InicioLinha) + 1 do
    InicioLinha := InicioLinha + ' ';
  //Busca  tamanho do campo
  Tam := Fim - Inicio + 1;
  Valor := copy(Valor, 1, Tam);
  if numerico then
  begin //mumerico
    while Tam > Length(valor) do
      Valor := '0' + Valor;
  end
  else //alfa-numerico
  begin
    while Tam > Length(valor) do
      Valor := Valor + ' ';
  end;
  Linha := InicioLinha + valor + FimLinha;
end;

procedure BloquearAcaoUsuario(Bloquea: Boolean);
var
  xBlockInput: function(Block: BOOL): BOOL; stdcall;
begin
  //Vini desabilitar o bloqueo de controles temporariamente...
  EnableCTRLALTDEL(not (Bloquea));
  if FunctionDetect('USER32.DLL', 'BlockInput', @xBlockInput) then
  begin
    xBlockInput(Bloquea);
  end;
end;

procedure EnableCTRLALTDEL(YesNo: boolean);
const
  sRegPolicies = '\Software\Microsoft\Windows\CurrentVersion\Policies';
begin
  with TRegistry.Create do
  try
    RootKey := HKEY_CURRENT_USER;
    if OpenKey(sRegPolicies + '\System\', True) then
    begin
      case YesNo of
        False:
          begin
            WriteInteger('DisableTaskMgr', 1);
          end;
        True:
          begin
            WriteInteger('DisableTaskMgr', 0);
          end;
      end;
    end;
    CloseKey;
    if OpenKey(sRegPolicies + '\Explorer\', True) then
    begin
      case YesNo of
        False:
          begin
            WriteInteger('NoChangeStartMenu', 1);
            WriteInteger('NoClose', 1);
            WriteInteger('NoLogOff', 1);
          end;
        True:
          begin
            WriteInteger('NoChangeStartMenu', 0);
            //            &nb sp;
            WriteInteger('NoClose', 0);
            WriteInteger('NoLogOff', 0);
          end;
      end;
    end;
    CloseKey;
  finally
    Free;
  end;
end;

procedure DisableTaskMgr(bTF: Boolean);
var
  reg: TRegistry;
begin
  reg := TRegistry.Create;
  reg.RootKey := HKEY_CURRENT_USER;
  reg.OpenKey('Software', True);
  reg.OpenKey('Microsoft', True);
  reg.OpenKey('Windows', True);
  reg.OpenKey('CurrentVersion', True);
  reg.OpenKey('Policies', True);
  reg.OpenKey('System', True);
  if bTF = True then
  begin
    reg.WriteString('DisableTaskMgr', '1');
  end
  else
    if bTF = False then
    begin
      reg.DeleteValue('DisableTaskMgr');
    end;
  reg.CloseKey;
end;

function FunctionDetect(LibName, FuncName: string; var LibPointer: Pointer): Boolean;
var
  LibHandle: THandle;
begin
  Result := False;
  LibPointer := nil;
  if LoadLibrary(PChar(LibName)) = 0 then
    Exit;
  LibHandle := GetModuleHandle(PChar(LibName));
  if LibHandle <> 0 then
  begin
    LibPointer := GetProcAddress(LibHandle, PChar(FuncName));
    if LibPointer <> nil then
      Result := True;
  end;
end;

procedure AtualizarArquivo(DirServer: string; Arquivo: string);
var vPath: string;
  vIntDataServer, vIntDataLocal: Integer;
begin
  if Copy(DirServer, Length(DirServer), 1) <> '\' then
    DirServer := DirServer + '\';
  vPath := ExtractFilePath(Application.ExeName);
  vIntDataServer := FileAge(DirServer + Arquivo);
  vIntDataLocal := FileAge(vPath + Arquivo);
  //    Testando um Intervalo maior que 1 hora (2200)
  if abs(vIntDataLocal - vIntDataServer) > 2200 then
    if not CopyFile(PChar(DirServer + Arquivo), PChar(vPath + Arquivo), False) then
      Application.MessageBox(PChar('� necess�rio atualizar o arquivo ' + Arquivo + ' e eu n�o consegui atualizar automaticamente do servidor: ' + DirServer + '!'), 'Erro');
end;

procedure deletarArquivos(Diretorio: string; Arquivo: string; AntesDe: TDateTime);
var Arqs: TSearchRec;
begin
  if Copy(Diretorio, 1, length(Diretorio)) <> '\' then
    Diretorio := Diretorio + '\';

  if FindFirst(Diretorio + Arquivo, 0, Arqs) = 0 then
  begin
    repeat
      try
        if FileAge(Diretorio + Arqs.Name) < DateTimeToFileDate(AntesDe) then
          DeleteFile(Diretorio + arqs.Name);
      except
      end;
    until FindNext(Arqs) <> 0;
  end;
  FindClose(Arqs);
end;

procedure CopiarArquivos(DiretorioOrig, DiretorioDest: string; Arquivo: string; AntesDe: TDateTime);
var Arqs: TSearchRec;
begin
  if Copy(DiretorioOrig, 1, length(DiretorioOrig)) <> '\' then
    DiretorioOrig := DiretorioOrig + '\';

  if Copy(DiretorioDest, 1, length(DiretorioDest)) <> '\' then
    DiretorioDest := DiretorioDest + '\';

  if FindFirst(DiretorioOrig + Arquivo, 0, Arqs) = 0 then
  begin
    repeat
      try
        if FileAge(DiretorioOrig + Arqs.Name) < DateTimeToFileDate(AntesDe) then
          CopyFile(PChar(DiretorioOrig + arqs.Name), PChar(DiretorioDest + arqs.Name), False);
      except
      end;
    until FindNext(Arqs) <> 0;
  end;
  FindClose(Arqs);
end;

function IntToStrZero(Inteiro: Integer; Tamanho: Integer): string;
begin
  Result := IntToStr(Inteiro);
  while Length(Result) < Tamanho do
    Result := '0' + Result;
end;

{function validaIE(IE, UF: string; ApenasDigitos: Boolean = True): boolean;

//c�digo pego do site  http://cincobytes.net/FUNCAO-PARA-VALIDAR-INSCRICAO-ESTADUAL-IE-DE-TODOS-OS-ESTADOS/

 const
   NUMERO: smallint = 37;
   MASCARAS_: string = '     NNNNNNNNX- NNNNNNNNNNNXY-   NNNNNNNNNNX-NNNNNNNNNNNNNX-      NNNNNNYX-    NNNNNNNNXY-' +
   '    NNNNNNNNNX-  NNNNNNNNXNNY-  NNNNNNNNXNNN-     NNNNNNNXY';
   PESOS_: string = 'GFEDCJIHGFEDCA-FEDCJIHGFEDCAA-GFEDCJIHGFEDAC-AAAAAAAAGFEDCA-AAAAABCDEFGHIA-AAAJIAAHGFEDCA-' +
   'FEDCBJIHGFEDCA-IHGFEDCHGFEDCA-HGFEDCHGFEDCAA-ABCBBCBCBCBCAA-ADCLKJIHGFEDCA-AABDEFGHIKAAAA-AADCKJIHGFEDCA-' +
     'AAAAAJIHGFEDCA-AAAAAIHGFEDCAA-AAAAAJIHGFEDCA-AAAAKJIHGFEDCA-';
   PESO_: string = 'ABAAAAABBABAAAAAAJAAIGAHAADAEALLAFNOQ!A!!!!!CC!A!!!!!!K!!H!!!!!!!!!M!!!!P!';
   ALFA_: string = 'ABCDEFGHIJKLMNOPQRS';
   ROTINAS_: string = 'EE011EEEEEEEEEEEE2EEEEEE0EEEDEDDEEEE0!E!!!!!EE!E!!!!!!E!!E!!!!!!!!!D!!!!E!';
   MODULOS_: string = '99999998999999999899999999997999999990900000890900000090090000000009000090';
   INICIO_ : string = '0020000AB000111X2X11X11X2XXX2XXXX2XX2114333XXXX7XCC2X8X56X89X0XXX4XXXX9XX0';
   MASCARA_: string = 'ABAAAAAEEABAAAACABAAFDAEAGADAAHIACAJG';
   FATORES_: string = '0000100000001000000001000011000000000';
   ESTADOS_: string = 'ACACALA1APA2AMBABACEDFESGOGOMAMTMSMGPAPBPRPEPIRJRNRSRORORRSCSPSPSET0TOPERN';
 var
   c1, c2, alternativa, inicio, posicao, erros, fator, modulo, soma1, soma2, valor, digito: smallint;
   mascara, inscricao, a1, a2, peso, rotina: string;
 begin
   if UF = '' then MessageDlg('Informe o UF!',mtError,[mbOK],0);
   // Copyright: www.cincobytes.net / suporte@cincobytes.net //
   IE := trim(uppercase(IE));
   result := ((IE = 'ISENTO') or (IE = 'EM ANDAMENTO') or ((UF = 'EX') and ((IE = '') or (IE = '00000000000000'))));
   posicao := 0;
   while ((not result) and (posicao < NUMERO) and (IE <> '')) do
   begin
     inc(posicao);
     if (UF = 'AP') and (StrToFloat(IE) <= 30170009) then
       UF := 'A1';
     if (UF = 'AP') and (StrToFloat(IE) >= 30190230) then
       UF := 'A2';
     if (copy(ESTADOS_, posicao * 2 - 1, 2)) <> UF then
       continue;
     inscricao := '';
     for C1 := 1 to 30 do
       if pos(copy(IE, C1, 1), '0123456789') <> 0 then
         inscricao := inscricao + copy(IE, C1, 1);
     mascara := copy(MASCARAS_, pos(copy(MASCARA_, posicao, 1), ALFA_) * 15 - 14, 14);
     if length(inscricao) <> length(trim(mascara)) then
       continue;
     inscricao := copy('00000000000000' + inscricao, length(inscricao) + 1, 14);
     erros := 0;
     alternativa := 0;
     while (alternativa < 2) do
     begin
       inc(alternativa);
       inicio := posicao + (alternativa * NUMERO) - NUMERO;
       peso := copy(PESO_, inicio, 1);
       if peso = '!' then
         continue;
       a1 := copy(INICIO_, inicio, 1);
       a2 := copy(copy(inscricao, 15 - length(trim(mascara)), length(trim(mascara))), alternativa, 1);
       if (not ApenasDigitos) and (((pos(a1, 'ABCX') = 0) and (a1 <> a2)) or
         ((pos(a1, 'ABCX') <> 0) and (pos(a2, copy('0123458888-6799999999-0155555555-0123456789',
         (pos(a1, 'ABCX') * 11 - 10), 10)) = 0))) then
         erros := 1;
       soma1 := 0;
       soma2 := 0;
       for C2 := 1 to 14 do
       begin
         valor := StrToInt(copy(inscricao, C2, 1)) *
           (pos(copy(copy(PESOS_, (pos(peso, ALFA_) * 15 - 14), 14), C2, 1), ALFA_) - 1);
         soma1 := soma1 + valor;
         if valor > 9 then
           valor := valor - 9;
         soma2 := soma2 + valor;
       end;
       rotina := copy(ROTINAS_, inicio, 1);
       modulo := StrToInt(copy(MODULOS_, inicio, 1)) + 2;
       fator := StrToInt(copy(FATORES_, posicao, 1));
       if pos(rotina, 'A22') <> 0 then
         soma1 := soma2;
       if pos(rotina, 'B00') <> 0 then
         soma1 := soma1 * 10;
       if pos(rotina, 'C11') <> 0 then
         soma1 := soma1 + (5 + 4 * fator);
       if pos(rotina, 'D00') <> 0 then
         digito := soma1 mod modulo;
       if pos(rotina, 'E12') <> 0 then
         digito := modulo - (soma1 mod modulo);
       if digito = 10 then
         digito := 0;
       if digito = 11 then
         digito := fator;
       if (copy(inscricao, pos(copy('XY', alternativa, 1), mascara), 1) <> IntToStr(digito)) then
         erros := 1;
     end;
     result := (erros = 0);
  end;
end;     }

function RetirarCaractere(s: string; Caractere: Char): string;
begin
  Result := S;
  while Pos(Caractere, Result) > 0 do
    Result := Copy(Result, 1, Pos(Caractere, Result) - 1) + Copy(Result, Pos(Caractere, Result) + 1, Length(Result));
end;

function TiraAcento(sin: string; RetiraCaracNaoEncontrado: Boolean = False): string;
var sOut, sChar: string;
  i, ii: Integer;
  //Base, new: string;
  //    CaracIn : array of Char;
  //    CaracOut : array of Char;
  CaracIn, CaracOut: string;
begin
  SetLength(CaracIn, 256);
  SetLength(CaracOut, 256);

  CaracIn  := ' ������AAA����EEEEE����IIII������OOO����UUUUUU������aaa����eeeee����iiii������ooo����uuuuu�������CcCcCcCcDd�dGgGgGgGgHhHhABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890abcdefghijklmnopqrstuvwxyz!@$%�&*()-_=+[]�~/;:.,<>\|{}"���� ' + #9 + #13;
  CaracOut := ' AAAAAAAAAEEEEEEEEEIIIIIIIIOOOOOOOOOUUUUUUUUUUaaaaaaaaaeeeeeeeeeiiiiiiiiooooooooouuuuuuuuuCcNnYyyCcCcCcCcDdDdGgGgGgGgHhHhABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890abcdefghijklmnopqrstuvwxyz.@    *()-_=+    /;:.,      "       ';

  sOut := '';
  for i := 1 to Length(sin) do
  begin
    sChar := '';
    for ii := 0 to length(CaracIn) - 1 do
    begin
      if sin[i] = CaracIn[ii] then
      begin
        sChar := CaracOut[ii];
      end;
    end;
    if sChar = '' then
    begin
      if RetiraCaracNaoEncontrado then
        sChar := ''
      else
        sChar := ' ';
    end;
    sOut := sOut + sChar;
  end;
  Result := sOut;
end;

function validaIE(IE, UF: string; ApenasDigitos: Boolean = True): boolean;
var
//  IRet, IOk, IErro: Integer;
  vinscricao: string;
  LibHandle: THandle;
  ConsisteInscricaoEstadual: TConsisteInscricaoEstadual;
begin
  vinscricao := RetirarCaractere(IE, '.');
  vinscricao := RetirarCaractere(vinscricao, '-');
  vinscricao := RetirarCaractere(vinscricao, ',');
  try
      LibHandle := LoadLibrary(PChar(Trim('DllInscE32.Dll')));
    if LibHandle <= HINSTANCE_ERROR then
      raise Exception.Create('Dll n�o carregada');

    @ConsisteInscricaoEstadual := GetProcAddress(LibHandle,
      'ConsisteInscricaoEstadual');
    if @ConsisteInscricaoEstadual = nil then
      raise Exception.Create('Entrypoint Download n�o encontrado na Dll');

    Result := ConsisteInscricaoEstadual(vinscricao, UF) = 0;
  finally
    FreeLibrary(LibHandle);
  end;

end;

function Arredondar(Valor: Double; Dec: Integer): Double;
var
  Valor1,
    Numero1,
    Numero2,
    Numero3: Double;
begin
  Valor1 := Exp(Ln(10) * (Dec + 1));
  Numero1 := Int(Valor * Valor1);
  Numero2 := (Numero1 / 10);
  Numero3 := Round(Numero2);
  Result := (Numero3 / (Exp(Ln(10) * Dec)));
end;

function Coalesce(vValue,vIfNull: Variant): Variant;
begin
  if vValue = Null then
    Result := vIfNull
  else
    Result := vValue;
end;

procedure EnviarEmail(const sPrograma,
  sSmtpHost,
  sSmtpPort,
  sSmtpUser,
  sSmtpPasswd,
  sFrom,
  sTo,
  sAssunto: string;
  sMensagem: TStrings;
  SSL: Boolean;
  sCC: TStrings = nil;
  Anexos: TStrings = nil;
  PedeConfirma: Boolean = False;
  AguardarEnvio: Boolean = False;
  NomeRemetente: string = '';
  TLS: Boolean = True);
var
  ThreadSMTP: TSendMailThread;
  m: TMimemess;
  p: TMimepart;
  StreamNFe: TStringStream;
//  NomeArq: string;
  i: Integer;
  Self : TForm;
  sdsGravaLog : TFDQuery;
begin
  m := TMimemess.create;
  ThreadSMTP := TSendMailThread.Create(Self); // N�o Libera, pois usa FreeOnTerminate := True ;
  StreamNFe := TStringStream.Create('');
  try
    p := m.AddPartMultipart('mixed', nil);
    if sMensagem <> nil then
      m.AddPartText(sMensagem, p);
    if assigned(Anexos) then
      for i := 0 to Anexos.Count - 1 do
      begin
        m.AddPartBinaryFromFile(Anexos[i], p);
      end;
    m.header.tolist.add(sTo);
    if Trim(NomeRemetente) <> '' then
      m.header.From := Format('%s<%s>', [NomeRemetente, sFrom])
    else
      m.header.From := sFrom;
    m.header.subject := sAssunto;
    m.Header.ReplyTo := sFrom;
    if PedeConfirma then
      m.Header.CustomHeaders.Add('Disposition-Notification-To: ' + sFrom);
    m.EncodeMessage;

    ThreadSMTP.sFrom := sFrom;
    ThreadSMTP.sTo := sTo;
    if sCC <> nil then
      ThreadSMTP.sCC.AddStrings(sCC);
    ThreadSMTP.slmsg_Lines.AddStrings(m.Lines);

    ThreadSMTP.smtp.UserName := sSmtpUser;
    ThreadSMTP.smtp.Password := sSmtpPasswd;
    ThreadSMTP.smtp.TargetHost := sSmtpHost;
    ThreadSMTP.smtp.TargetPort := sSmtpPort; // Usa default

    ThreadSMTP.smtp.FullSSL := SSL;
    ThreadSMTP.smtp.AutoTLS := TLS;
    ThreadSMTP.smtp.StartTLS; // Linha adicionada para Hotmail

    ThreadSMTP.Resume; // inicia a thread
    if AguardarEnvio then
    begin
      repeat
        Sleep(1000);
        Application.ProcessMessages;
      until ThreadSMTP.Terminado;
    end;
  finally
    m.free;
    StreamNFe.Free;

    if Application.FindComponent('sdsGravaLog') = nil then
        sdsGravaLog := TFDQuery.Create(Application);
    sdsGravaLog.Connection := Bds.SQLConnection;
    sdsGravaLog.Name := 'sdsGravaLog';

    sdsGravaLog.Close;
    sdsGravaLog.SQL.CommaText := 'insert into email_log (i_cdusuario, c_programa, d_envio, c_destinatario, c_textoemail, c_comcopia) values ('
                            + ':i_cdusuario, :c_programa, now(), :destinatario, :textoemail, :comcopia)';
    sdsGravaLog.Params.ParamByName('i_cdusuario').AsInteger := StrToInt(getParametro(Application, 'I_CDUSUARIO', false));
    sdsGravaLog.Params.ParamByName('c_programa').AsString := sPrograma;
    sdsGravaLog.Params.ParamByName('destinatario').AsString := sTo;
    sdsGravaLog.Params.ParamByName('textoemail').AsString := sMensagem.Text;
    sdsGravaLog.Params.ParamByName('comcopia').AsString := sCC.Text;
    sdsGravaLog.ExecSQL();

    sdsGravaLog.Free;
  end;
end;

function GetDefaultPrinterName : String;
begin
  if (Printer.PrinterIndex > 0) then
    Result := Printer.Printers[Printer.PrinterIndex]
  else
    Result := 'NOPRINTER';    
end;

function diferente(vValue1, vValue2: Variant): String;
begin
  try
    if vValue1 <> vValue2 then
      Result := '[VALIDO]'
    else
      Result := '[INVALIDO] Motivo: ' + VarToStr(vValue1) + ' = ' + VarToStr(vValue2);
  except
    on E: Exception do
    begin
      Result := '[INVALIDO] Motivo: ' + e.Message;
    end;
  end;
end;

function igual(vValue1, vValue2: Variant): String;
begin
  try
    if vValue1 = vValue2 then
      Result := '[VALIDO]'
    else
      Result := '[INVALIDO] Motivo: ' + VarToStr(vValue1) + ' <> ' + VarToStr(vValue2);
  except
    on E: Exception do
    begin
      Result := '[INVALIDO] Motivo: ' + e.Message;
    end;
  end;      
end;

function maior(vValue1, vValue2: Variant): String;
begin
  try
    if vValue1 > vValue2 then
      Result := '[VALIDO]'
    else
      Result := '[INVALIDO] Motivo: ' + VarToStr(vValue1) + ' <= ' + VarToStr(vValue2);
  except
    on E: Exception do
    begin
      Result := '[INVALIDO] Motivo: ' + e.Message;
    end;
  end;
end;

function menor(vValue1, vValue2: Variant): String;
begin
  try
    if vValue1 < vValue2 then
      Result := '[VALIDO]'
    else
      Result := '[INVALIDO] Motivo: ' + VarToStr(vValue1) + ' >= ' + VarToStr(vValue2);
  except
    on E: Exception do
    begin
      Result := '[INVALIDO] Motivo: ' + e.Message;
    end;
  end;
end;

function maior_igual(vValue1, vValue2: Variant): String;
begin
  try
     If vValue1 >= vValue2 then
       Result := '[VALIDO]'
     else
       Result := '[INVALIDO] Motivo: ' + VarToStr(vValue1) + ' < ' + VarToStr(vValue2);
  except
    on E: Exception do
    begin
      Result := '[INVALIDO] Motivo: ' + e.Message;
    end;
  end;
end;

function menor_igual(vValue1, vValue2: Variant): String;
begin
  try
    If vValue1 <= vValue2 then
      Result := '[VALIDO]'
    else
      Result := '[INVALIDO] Motivo: ' + VarToStr(vValue1) + ' > ' + VarToStr(vValue2);
  except
    on E: Exception do
    begin
      Result := '[INVALIDO] Motivo: ' + e.Message;
    end;
  end;
end;

function formatarSql(sSql : String) : String;
var
  vCaminhoArquivo, vCaminhoExecutavel, vComandoFormatter, vCaminhoFormatter, vArgumentosFormatter: String;
  tSQL: TStrings;
begin
  // procedure para formatar o sql recebido, salvando em um determinado arquivo temporario
  // e efetuando a formatacao. apos isso, retorna a string e atribui ao result
  {*** INICIALIZACAO DE VARIAVEIS ***}

  tSQL := TStringList.Create;
  try
    vCaminhoExecutavel   := ExtractFilePath(Application.ExeName);
    vCaminhoArquivo      := vCaminhoExecutavel + 'SQLTemporario.sql';
    vCaminhoFormatter    := vCaminhoExecutavel + 'SQLFormatter.exe';
    vArgumentosFormatter := ' /ae:true /b:false /tc:true /sk:true /uk:false /ecs:false ';

    {*** EXECUCAO DE PROCEDIMENTOS PARA SALVAR, FORMATAR LER E RETORNAR SQL ***}
    tSQL.Add(sSQL);
    tSQL.SaveToFile(vCaminhoArquivo);

    sleep(2000);
    WinExec(PAnsiChar(vCaminhoFormatter + vArgumentosFormatter + vCaminhoArquivo) ,0);
    sleep(1500);
    tSQL.LoadFromFile(vCaminhoArquivo);

    If tSQL.ValueFromIndex[0] = '-WARNING! ERRORS ENCOUNTERED DURING SQL PARSING!' then tSQL.Delete(0);
    DeleteFile(vCaminhoArquivo);
    DeleteFile(vCaminhoArquivo + '.bak');

      result := tSQL.Text;
    finally
      tSQL.Free;
    end;
end;


function formatarSqlSimples(sSql : String) : String;
Const
  ASqls : array[1..16] of string=('SELECT ',' AND ',' OR ',',',' FROM ',' WHERE ',' GROUP BY ',' UNION ',' ORDER BY ',' LIMIT ',' HAVING ',' OFFSET ',' INNER ',' LEFT ',' RIGHT ',' JOIN ');

var i:integer;
   ret : Tstrings;

begin
  for i := 1 to 16 do
  begin
    while  pos(ASqls[i],UpperCase(sSql)) > 0 do
    begin
       //preciso retirar  a string da array ASqls do sSql para nao entrar em loop
       // substituo o valor pelo "i"
       // exemplo se o sql = "Select i_cdteste, c_nome"
       // vai ficar   sql =  "Select i_cdteste#10#13#1 c_nome"

       if i < 10 then
         sSql := copy(sSql,0,pos(ASqls[i],UpperCase(sSql))-1) + '#10#13#0'+ IntToStr(i)+ copy(sSql,pos(ASqls[i],UpperCase(sSql))+length(ASqls[i]),length(sSql))
       else
         sSql := copy(sSql,0,pos(ASqls[i],UpperCase(sSql))-1) + '#10#13#'+ IntToStr(i)+ copy(sSql,pos(ASqls[i],UpperCase(sSql))+length(ASqls[i]),length(sSql))
    end;
  end;
  sSql := sSql + '#10#13#';

  ret := Tstringlist.Create;
  try
     while Pos('#10#13#',sSql)+6 < length(sSql)  do
     begin

       ret.Add(copy(sSql,0,pos('#10#13#',sSql)-1)+ASqls[StrToInt(copy(sSql,pos('#10#13#',sSql)+7,2))] + copy(sSql,Pos('#10#13#',sSql)+9, PosEx('#10#13#',sSql,2)-10)) ;
       sSql := copy(sSql,posEx('#10#13#',sSql,2),length(sSql));
     end;

     if length(sSql) > 0 then
       ret.Add(copy(sSql,0,pos('#10#13#',sSql)-1));
     result := ret.text;
  finally
    ret.free;
  end;

end;


procedure SalvaSQLFormatado(SQL: String; CaminhoArquivo: String = ''; NomeArquivo: String = 'SQLFormatado.sql');
var stringArquivo: TStrings;
    tSQL: string;
begin
  { Salva um determinado sql no disco e formata atraves da funcao de formatacao         }
  { Ainda, busca no caminho do arquivo o nome e extensao .sql, se nao existir, adiciona }
  { automaticamente ao final do arquivo o nome SQLFormatado caso n�o informe nenhum nome}
  {. Adicionalmente, se nao for                                                         }
  { informado em hipotese alguma o caminho do arquivo, o sistema ir� salvar na pasta de }
  { execucao do sistema aquele determinado sql a ser formatado                          }

  if CaminhoArquivo = '' then
    CaminhoArquivo := (ExtractFilePath(Application.ExeName) + NomeArquivo)
  else
    if pos('.sql', CaminhoArquivo) > 0 then
      CaminhoArquivo := CaminhoArquivo + NomeArquivo
    else
      CaminhoArquivo := CaminhoArquivo + NomeArquivo + '.sql';

  try
    stringArquivo := TStringList.Create();
    tSQL := formatarSql(SQL);
    stringArquivo.Add(tSQL);

    stringArquivo.SaveToFile(CaminhoArquivo);
  finally
    stringArquivo.Free;
  end;
end;

function CountChar(Key : char; sTexto : String) : integer;
var
  i, iTotal : integer;
begin
  iTotal := 0;
  
  if Length(sTexto) > 0 then
  begin
    for i := 0 to Length(sTexto) do
    begin
      if Key = sTexto[i] then
        inc(iTotal);
    end;
  end;

  Result := iTotal;
end;

function AdicionaZerosAEsquerda(vValor: String; vTamanho: Integer): String;
begin
  Result := StringOfChar('0',vTamanho-Length(vValor))+vValor;
end;

function valorPorExtenso(rValor : Real) : String;
const
  asUnidade: array[1..19] of String = ('um', 'dois', 'tr�s', 'quatro', 'cinco', 'seis', 'sete', 'oito', 'nove',
    'dez', 'onze', 'doze', 'treze', 'quatorze', 'quinze', 'dezesseis', 'dezessete', 'dezoito', 'dezenove');
  asDezena: array[2..9] of String = ('vinte', 'trinta', 'quarenta', 'cinquenta', 'sessenta', 'setenta', 'oitenta', 'noventa');
  asCentena: array[1..9] of String = ('cento', 'duzentos', 'trezentos', 'quatrocentos', 'quinhetos', 'seiscentos', 'setecentos', 'oitocentos', 'novecentos');
  asMr: array[0..4] of String = ('', 'mil', 'milh�o', 'bilh�o', 'trilh�o');
  asMrs: array[0..4] of String = ('', 'mil', 'milh�es', 'bilh�es', 'trilh�es');
var
  iInteiro: int64;
  sVlrInteiro, sValorExtenso, sCentavos, sVlrParticionado, sAux : String;
  rResto: Real;
  bUmReal, bTem: boolean;
  iTamanho, iCentena, iDezena, iCount, iUnidade, iAux: Integer;
begin
  iInteiro    := trunc(rValor);
  rResto      := rValor - iInteiro;
  sCentavos   := IntToStr(round(rResto * 100));
  sVlrInteiro := IntToStr(iInteiro);

  if rValor = 0 then
  begin
    Result := 'Zero.';
    Exit;
  end;

  if Length(sVlrInteiro) > 15 then
  begin
    Result := 'Valor superior a 999 trilh�es.';
    Exit;
  end;

  //000.000.000.000,00
  sValorExtenso := '';
  iCount        := 0;
  bUmReal       := False;
  bTem          := False;
  while (sVlrInteiro <> '') do
  begin
    iTamanho := Length(sVlrInteiro);

    if iTamanho > 3 then
    begin
      sVlrParticionado := copy(sVlrInteiro, iTamanho - 2, iTamanho);
      sVlrInteiro      := copy(sVlrInteiro, 0, iTamanho - 3);
    end
    else
    begin
      sVlrParticionado := sVlrInteiro;
      sVlrInteiro      := '';
    end;

    if sVlrParticionado <> '000' then
    begin

      if sVlrParticionado = '100' then
        sAux := 'cem'
      else
      begin
        iAux     := StrToInt(sVlrParticionado);
        iCentena := iAux div 100;
        iDezena  := (iAux mod 100) div 10;
        iUnidade := (iAux mod 100) mod 10;
        sAux     := '';
        if iCentena > 0 then
          sAux := asCentena[iCentena];


        if (iDezena > 0) or (iUnidade > 0) then
        begin

          if (iAux mod 100) <= 19 then //obtem somente as dezenas: de um a dezenove
          begin
            if sAux <> '' then
              sAux := sAux + ' e ' + asUnidade[iAux mod 100]
            else
              sAux := asUnidade[iAux mod 100];
          end
          else
          begin     //Obtem a dezena e a unidade separadas
            if iDezena > 0 then
              if sAux <> '' then
                sAux := sAux + ' e ' + asDezena[iDezena]
              else
                sAux := asDezena[iDezena];

            if iUnidade > 0 then
              if sAux <> '' then
                sAux := sAux + ' e ' + asUnidade[iUnidade]
              else
                sAux := asUnidade[iUnidade];
          end;
        end;
      end;

      if (sVlrParticionado = '1') or (sVlrParticionado = '001') then
      begin
        if iCount = 0 then
          bUmReal := True
        else
          sAux := sAux + ' ' + asMr[iCount];
      end
      else
        if iCount > 0 then
          sAux := sAux + ' ' + asMrs[iCount];

      if sValorExtenso <> '' then
        sValorExtenso := sAux + ', ' + sValorExtenso
      else
        sValorExtenso := sAux;
    end;
    if (((iCount = 0) or (iCount = 1)) and (length(sValorExtenso) <> 0)) then
      bTem := True;

    inc(iCount);
  end;

  if sValorExtenso <> '' then
  begin
    if bUmReal then
      sValorExtenso := sValorExtenso + ' real'
    else
      if bTem then
        sValorExtenso := sValorExtenso + ' reais'
      else
        sValorExtenso := sValorExtenso + ' de reais';

  end;

  if sCentavos <> '0' then
  begin
    if sValorExtenso <> '' then
      sValorExtenso := sValorExtenso + ' e ';

    if sCentavos = '1' then
      sValorExtenso := sValorExtenso + ' um centavo'
    else
    begin
      iInteiro := StrToInt(sCentavos);

      if iInteiro <= 19 then
        sValorExtenso := sValorExtenso + asUnidade[iInteiro]
      else
      begin
        iDezena  := iInteiro div 10;
        iUnidade := iInteiro mod 10;

        sValorExtenso := sValorExtenso + asDezena[iDezena];

        if iUnidade > 0 then
        begin
          sValorExtenso := sValorExtenso + ' e ' + asUnidade[iUnidade]; 
        end;
      end;
      sValorExtenso := sValorExtenso + ' centavos';
    end;
  end;

  sValorExtenso := initCap(sValorExtenso) + '.'; 

  Result := sValorExtenso;
end;

function initCap( sTexto : String): string;
var
  sPCaractere, sRestante : String;
begin
  sPCaractere := '';
  sRestante   := '';
  if(sTexto <> '') then
  begin
    sPCaractere := copy(sTexto, 0, 1);
    sRestante   := copy(sTexto, 2, Length(sTexto));

    sPCaractere := UpperCase(sPCaractere);
  end;

  Result := sPCaractere + sRestante;

end;

function ping(host: String): boolean;
var
  IdICMPClient: TIdICMPClient;
begin

  try
    try
      IdICMPClient := TIdICMPClient.Create( nil );
      IdICMPClient.Host := host;
      IdICMPClient.ReceiveTimeout := 100;
      IdICMPClient.Ping;
      result := ( IdICMPClient.ReplyStatus.BytesReceived > 0 );
    except
      result := False;
    end;
  finally
    IdICMPClient.Free;
  end;
end;

function ValidaEmail(const Email : String): Boolean;
const
  CaracterEspecial: array[1..40] of string[1] =
  ( '!', '#', '$', '%', '�', '&', '*',
    '(', ')', '+', '=', '�', '�', '�', '�', '�',
    '�', '�', '�', '`', '�', '�', ',', ';', ':',
    '<', '>', '~', '^', '?', '/', '' , '|', '[',
    ']', '{', '}', '�', '�', '�'
  );

var i, cont,PosArroba,PosUnder : Integer;

begin
   Result := True;
   cont   := 0;

   if Email <> '' then
      if (Pos('@', Email) <> 0) and (Pos('.', Email) <> 0) then //Existe @ .
         begin
            if (Pos('@', Email) = 1) or (Pos('@', Email) = Length(Email)) or
               (Pos('.', Email) = 1) or (Pos('.', Email) = Length(Email)) or
               (Pos(' ', Email) <> 0) then
                  Result := False
            else              // @ seguido de . e vice versa
               if (Abs(Pos('@', Email) - Pos('.', Email)) = 1) then
                  Result := False
               else
                  begin
                     for i := 1 to 40 do     // se existe caracter especial
                        if Pos(CaracterEspecial[i], Email) <> 0 then
                           Result := False;

                     for i := 1 to Length(Email) do
                        begin             // Se existe apaneas 1 @
                           if Email[i] = '@' then
                              cont := cont + 1;             // . seguidos de .
                           if (Email[i] = '.') and (Email[i + 1] = '.') then
                              Result := False;
                        end;

                     // . no f, 2 ou + @, . no i, - no i, _no i
                     if (cont >= 2) or (Email[Length(Email)] = '.') or
                        (Email[1] = '.') or
                        (Email[1] = '-') then
                           Result := False;

                     //@ seguido de COM e vice versa
                     if (Abs(Pos('@', Email) - Pos('com', Email)) = 1) then
                        Result := False;

                     //@ seguido de - e vice versa
                     if (Abs(Pos('@', Email) - Pos('-', Email)) = 1) then
                        Result := False;

                     //@ seguido de _ e vice versa
                     PosArroba := 0;
                     PosUnder := 0;
                     if (Pos('@', Email) > 0) and (Pos('_', Email) > 0) then
                     begin
                       PosArroba := Pos('@', Email);
                       PosUnder := Pos('_', Email);
                       if (PosArroba - PosUnder) = -1 then
                         Result := False;
                     end;
                  end;
         end
      else
         Result := False;
end;



end.

