unit U_RelPadrao;

interface

uses
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Windows, U_PadraoCadastro, FMTBcd, Provider, DBClient, SLClientDataSet,
  Controls, ExtCtrls, SlDBFind, RpRender, RpRenderCanvas, RpRenderPrinter,
  RpBase, RpSystem, RpDefine, RpRave, SqlExpr, Menus, ImgList, ComCtrls,
  ToolWin, Classes,Dialogs, Sysutils, Forms, RpRenderHTML, RpRenderPDF,
  RpRenderText, RpRenderRTF, AppEvnts, System.ImageList, Data.DB,Messages,
  frxClass, frxExportPDF, frxExportCSV, frxExportText, frxExportImage,
  frxExportRTF, frxExportBIFF, frxExportXML, frxExportBaseDialog, frxExportXLS,
  frxDesgn, dm;

type
  TF_RelPadrao = class(TF_PadraoCadastro)
    Btn_Visualizar: TToolButton;
    Btn_Imprimir: TToolButton;
    Panel1: TPanel;
    ImprimirPrinter1: TMenuItem;
    Visualizar2: TMenuItem;
    cdsEmpresa: TSLClientDataSet;
    dspEmpresa: TDataSetProvider;
    sdsEmpresa: TFDQuery;
    frxReport: TfrxReport;
    frxXMLExport1: TfrxXMLExport;
    frxBIFFExport1: TfrxBIFFExport;
    frxRTFExport1: TfrxRTFExport;
    frxJPEGExport1: TfrxJPEGExport;
    frxGIFExport1: TfrxGIFExport;
    frxSimpleTextExport1: TfrxSimpleTextExport;
    frxCSVExport1: TfrxCSVExport;
    frxPDFExport1: TfrxPDFExport;
    frxDesigner: TfrxDesigner;
    pnImprimindo: TPanel;
    procedure Imprimir(Sender: TObject);
    procedure ImprimirFast(Sender: TObject);
    procedure PrintFast();
    procedure ImprimirDireto(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure SetParam(NomeParam, ValorParam: string);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Btn_VisualizarMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Btn_ImprimirMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    vReportName: string;
    vFileReport: string;

    function tbKeyIsDown(const Key: Integer): Boolean;
    function BotaoEsquerdoPressionado(const Key: Integer): Boolean;
    { Public declarations }
  end;

var
  F_RelPadrao: TF_RelPadrao;
  vClickMouse: Boolean = False;
  vClick: Boolean = True;

implementation

uses
  U_Funcoes, U_SelImpressora, U_entrada;

{$R *.dfm}

procedure TF_RelPadrao.ImprimirFast(Sender: TObject);
  procedure AtualizarArquivoServer(DirServer: string; Arquivo: string);
  var
    vPath: string;
    vIntDataLocal: Integer;
    vIntDataServer: Integer;
  begin
    if Copy(DirServer, Length(DirServer), 1) <> '\' then
      DirServer := DirServer + '\';

    vPath := ExtractFilePath(Application.ExeName);

    if not CopyFile(PChar(vPath + Arquivo), PChar(DirServer + Arquivo), False) then
      Application.MessageBox(PChar('� necess�rio atualizar o arquivo ' + Arquivo + ' e eu n�o consegui atualizar automaticamente no servidor: ' + DirServer + '!'), 'Erro');
  end;

begin
  inherited;
  vClickMouse := False;

  if Pos('.fr3', vFileReport) > 0 then
    AtualizarArquivo(ExtractFilePath(Application.ExeName), 'own_' + vFileReport)
  else
    AtualizarArquivo(ExtractFilePath(Application.ExeName), 'own_' + vFileReport + '.fr3');

  if FileExists(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport) then begin
    frxReport.Clear;

    if frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport) then begin
      frxReport.Variables['Empresa'] := '''' + cdsEmpresa.FieldByName('C_NomeFantasia').AsString + '''';
      frxReport.Variables['Usuario'] := '''' + F_Entrada.BuscaParam('UsuarioLogin') + '''';
    end;
  end
  else begin
    if Pos('.fr3', vFileReport) > 0 then
      AtualizarArquivo(ExtractFilePath(Application.ExeName), vFileReport)
    else
      AtualizarArquivo(ExtractFilePath(Application.ExeName), vFileReport + '.fr3');

    frxReport.Clear;
    if frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) + vFileReport) then begin
      frxReport.Variables['Empresa'] := '''' + cdsEmpresa.FieldByName('C_NomeFantasia').AsString + '''';
      frxReport.Variables['Usuario'] := '''' + F_Entrada.BuscaParam('UsuarioLogin') + '''';
    end
    else begin
      ShowMessage('Arquivo ' + vFileReport + ' n�o foi encontrado na pasta ' + ExtractFilePath(Application.ExeName));
      Exit;
    end;
  end;

  if not(vClick) then begin
    if F_Entrada.BuscaParam('UsuarioLogin') = F_Entrada.BuscaParam('SUPERUSUARIO') then begin
      if not(FileExists(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport)) then begin
        frxReport.Clear;

        CopyFile(
          PChar(ExtractFilePath(Application.ExeName) + vFileReport),
          PChar(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport),
          False
        );

        if frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport) then begin
          frxReport.Variables['Empresa'] := '''' + cdsEmpresa.FieldByName('C_NomeFantasia').AsString + '''';
          frxReport.Variables['Usuario'] := '''' + F_Entrada.BuscaParam('UsuarioLogin') + '''';
        end;
      end;

      frxReport.DesignReport(True, False);
      AtualizarArquivoServer(ExtractFilePath(Application.ExeName), 'own_' + vFileReport);
    end
    else
      ShowMessage('Relat�rio utiliza no FastRepot - ' + vFileReport);

    vClick := True;
  end;
end;

procedure TF_RelPadrao.PrintFast;
begin
  Self.Enabled := False;

  pnImprimindo.Visible := True;
  pnImprimindo.BringToFront;

  pnImprimindo.Top := (Panel1.Height div 2) - (pnImprimindo.Height div 2);
  pnImprimindo.Left := (Panel1.Width div 2) - (pnImprimindo.Width div 2);
  pnImprimindo.Width := (Self.Width div 2);

  pnImprimindo.Caption := 'Imprimindo...';

  if frxReport.PrepareReport then
    frxReport.ShowPreparedReport
  else
    ShowMessage('Arquivo n�o pode ser impresso');

  pnImprimindo.Left := 978;
  pnImprimindo.Top := 8;
  pnImprimindo.Width := 185;
  pnImprimindo.Caption := '';
  pnImprimindo.Visible := False;

  Self.Enabled := True;
end;

procedure TF_RelPadrao.FormDeactivate(Sender: TObject);
begin
  inherited;
  if F_Entrada.BuscaParam('Rel_Internet') = 'True' then
    Close;
end;

procedure TF_RelPadrao.SetParam(NomeParam, ValorParam: string);
begin
  F_SelImpressora.RvProject1.SetParam(NomeParam, ValorParam);
end;

function TF_RelPadrao.BotaoEsquerdoPressionado(const Key: Integer): Boolean;
begin
  Result := (GetAsyncKeyState(Key) and KF_UP) = 0;
end;

procedure TF_RelPadrao.Btn_ImprimirMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if (ssLeft in Shift) and (ssCtrl in Shift) then
    vClick := False;
end;

procedure TF_RelPadrao.Btn_VisualizarMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if (ssLeft in Shift) and (ssCtrl in Shift) then
    vClick := False;
end;

procedure TF_RelPadrao.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TF_SelImpressora, F_SelImpressora);
end;

procedure TF_RelPadrao.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_F1 then begin
    setParametro(Application, 'avisoprograma', Name);

    if name = 'F_avisoprograma' then
      Exit
    else
      CriaPkg('avisoprograma', True);
  end;

  if (Shift = [ssCtrl]) and (Key = VK_LBUTTON) then
    ShowMessage('Relatorio utiliza o rav - ' + vFileReport);
end;

function TF_RelPadrao.tbKeyIsDown(const Key: integer): boolean;
begin
  Result := GetKeyState(Key) and 128 > 0;
end;

procedure TF_RelPadrao.ImprimirDireto(Sender: TObject);
begin
  Imprimir(Sender);
end;

procedure TF_RelPadrao.Imprimir(Sender: TObject);
begin
  if not(vClick) then begin
    ShowMessage('Relat�rio utiliza o rav - ' + vFileReport);
    vClick := true;
    Exit;
  end;

  if Pos('.rav', vFileReport) > 0 then
    AtualizarArquivo(F_Entrada.BuscaParam('DirServer'), vFileReport)
  else
    AtualizarArquivo(F_Entrada.BuscaParam('DirServer'), vFileReport + '.rav');

  F_SelImpressora.RvProject1.Close;
  F_SelImpressora.RvProject1.ProjectFile := ExtractFilePath(Application.ExeName) + vFileReport;
  F_SelImpressora.RvProject1.SelectReport(vReportName, True);

  cdsEmpresa.RequestFindID(StrToInt(F_Entrada.BuscaParam('EMPRESADEFAULT')));

  F_SelImpressora.RvProject1.SetParam('pEmpresa', cdsEmpresa.FieldByName('C_NomeFantasia').AsString);
  F_SelImpressora.RvProject1.SetParam('pUsuario', F_Entrada.BuscaParam('UsuarioLogin'));

  if F_Entrada.BuscaParam('Rel_Internet') = 'True' then begin
    F_SelImpressora.RvSystem1.DefaultDest := rdFile;
    F_SelImpressora.RvSystem1.DoNativeOutput := False;
    F_SelImpressora.RvSystem1.RenderObject := F_SelImpressora.RvRenderPDF1;
    F_SelImpressora.RvRenderPDF1.Active := True;
    F_SelImpressora.RvSystem1.SystemSetups := F_SelImpressora.RvSystem1.SystemSetups - [ssAllowSetup];
    F_SelImpressora.RvSystem1.OutputFileName := 'd:\Tomcat\webapps\estorilweb\web\RelEstoril\' + F_Entrada.BuscaParam('Arq_RelatorioWeb') + '.pdf';

    F_SelImpressora.RvProject1.ExecuteReport(vReportName);

    F_SelImpressora.RvSystem1.RenderObject := F_SelImpressora.RvRenderHTML1;
    F_SelImpressora.RvRenderHTML1.Active := True;
    F_SelImpressora.RvSystem1.OutputFileName := 'd:\Tomcat\webapps\estorilweb\web\RelEstoril\' + F_Entrada.BuscaParam('Arq_RelatorioWeb') + '.html';
    F_SelImpressora.RvProject1.ExecuteReport(vReportName);
  end
  else begin
    F_SelImpressora.RvProject1.SetParam('pEmpresa', cdsEmpresa.FieldByName('C_NomeFantasia').AsString);
    F_SelImpressora.RvProject1.SetParam('pUsuario', F_Entrada.BuscaParam('UsuarioLogin'));
    F_SelImpressora.RvProject1.ProjectFile := ExtractFilePath(Application.ExeName) + vFileReport;
    F_SelImpressora.RvProject1.SelectReport(vReportName, True);

    if (Sender = Btn_Visualizar) or (Sender = Visualizar2) then begin
      F_SelImpressora.btnPreviewClick(Sender);
      F_SelImpressora.Close;
    end
    else
      F_SelImpressora.ShowModal;
  end;

  vClickMouse := False;
end;

end.

