unit Menu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, U_Funcoes, FMTBcd, DB, SqlExpr;

type
  TF_Menu = class(TForm)
    MainMenu1: TMainMenu;
    Sistema1: TMenuItem;
    Entrada1: TMenuItem;
    ConsultaEntradadeProduto1: TMenuItem;
    procedure Entrada1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ConsultaEntradadeProduto1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  F_Menu: TF_Menu;
  vParametro: TParametro;

implementation

{$R *.dfm}

uses U_entrada, U_ConsulProdEntrada;

procedure TF_Menu.Entrada1Click(Sender: TObject);
begin
  Application.CreateForm(TF_Entrada, F_Entrada);
  F_Entrada.ShowModal;
end;

procedure TF_Menu.FormCreate(Sender: TObject);
begin
  vParametro := TParametro.Create;
  Application.InsertComponent(vParametro);
  vparametro.Fparams.add('PERMISSAO=|CONSULTA|INCLUS�O|ALTERA��O|EXCLUSAO');
  vparametro.Fparams.add('I_CDFUNCIONARIO=SGE');
  vparametro.Fparams.add('I_CdParceiroLogado=SGE');
  vparametro.Fparams.add('USUARIOLOGIN=SGE');
  vparametro.Fparams.add('SUPERUSUARIO=SGE');
  vparametro.Fparams.add('ICONESISTEMA=SGE');
end;

procedure TF_Menu.FormDestroy(Sender: TObject);
begin
//  vParametro.Free;
end;

procedure TF_Menu.FormActivate(Sender: TObject);
begin
  Application.CreateForm(TF_Entrada, F_Entrada);
  F_Entrada.ShowModal;
end;

procedure TF_Menu.ConsultaEntradadeProduto1Click(Sender: TObject);
begin
  Application.CreateForm(TF_ConsulProdEntrada, F_ConsulProdEntrada);
  F_ConsulProdEntrada.ShowModal;
end;

end.
