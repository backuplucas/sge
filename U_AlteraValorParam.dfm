inherited F_AlteraValorParam: TF_AlteraValorParam
  Left = 549
  Top = 280
  Caption = 'Valor do Par'#226'metro'
  ClientHeight = 289
  ClientWidth = 488
  OldCreateOrder = True
  ExplicitTop = -4
  ExplicitWidth = 504
  ExplicitHeight = 328
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 8
    Top = 14
    Width = 132
    Height = 13
    Caption = 'Digite o Valor do Par'#226'metro:'
  end
  object Label2: TLabel [1]
    Left = 375
    Top = 14
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo do Par'#226'metro:'
  end
  inherited pstb_padrao: TPanel
    Top = 267
    Width = 488
    inherited stb_Padrao: TStatusBar
      Width = 456
      Panels = <
        item
          Width = 150
        end
        item
          Alignment = taCenter
          Bevel = pbRaised
          Text = 'Usuario'
          Width = 300
        end
        item
          Width = 250
        end
        item
          Width = 100
        end>
    end
  end
  object Edit1: TEdit [3]
    Left = 8
    Top = 32
    Width = 457
    Height = 21
    TabOrder = 1
    OnKeyPress = Edit1KeyPress
  end
  object Button1: TButton [4]
    Left = 389
    Top = 56
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 2
    OnClick = Button1Click
  end
  object DateTimePicker1: TDateTimePicker [5]
    Left = 8
    Top = 32
    Width = 457
    Height = 21
    Date = 42153.471311550930000000
    Time = 42153.471311550930000000
    TabOrder = 3
    Visible = False
  end
  inherited ApplicationEvents2: TApplicationEvents
    Left = 48
    Top = 56
  end
end
