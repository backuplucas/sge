unit U_RelEntrada;

interface

uses
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U_RelPadrao, FMTBcd, SlDBFind, RpRender, RpRenderCanvas,
  RpRenderPrinter, RpBase, RpSystem, RpDefine, RpRave, DB, SqlExpr, Menus,
  ImgList, ExtCtrls, ComCtrls, ToolWin, Provider, DBClient, SLClientDataSet,
  StdCtrls, RpCon, RpConDS, RpRenderHTML, RpRenderPDF, Mask, RpRenderText,
  RpRenderRTF, System.ImageList, Vcl.AppEvnts, frxExportPDF, frxExportCSV,
  frxExportText, frxExportImage, frxExportRTF, frxExportBIFF, frxClass,
  frxExportBaseDialog, frxExportXML, frxDBSet, frxDock, frxDesgn, dm;

type
  TF_RelEntrada = class(TF_RelPadrao)
    cdsPedidoCompra: TSLClientDataSet;
    dspPedidoCompra: TDataSetProvider;
    sdsPedidoCompra: TFDQuery;
    Label1: TLabel;
    dsNotaEntrada: TDataSource;
    dsTitulo: TDataSource;
    cdsNotaEntrada: TSLClientDataSet;
    cdsTitulo: TSLClientDataSet;
    dspNotaEntrada: TDataSetProvider;
    dspTitulo: TDataSetProvider;
    sdsNotaEntrada: TFDQuery;
    sdsTitulo: TFDQuery;
    RvTitulo: TRvDataSetConnection;
    RvNotaEntrada: TRvDataSetConnection;
    RvPedidoCompra: TRvDataSetConnection;
    eDataInicial: TSLDate;
    eDataFinal: TSLDate;
    Label7: TLabel;
    eCdEmp: TSlDBEdit;
    eEmp: TSLEdit;
    Label8: TLabel;
    Label2: TLabel;
    dsempreza: TDataSource;
    cdsempreza: TSLClientDataSet;
    dspempreza: TDataSetProvider;
    sdsempreza: TFDQuery;
    cbxSituacao: TComboBox;
    Label3: TLabel;
    rgFiltro: TRadioGroup;
    eCdFornec: TSlDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    eFornec: TSLEdit;
    dsFornecedor: TDataSource;
    cdsFornecedor: TSLClientDataSet;
    dspFornecedor: TDataSetProvider;
    sdsFornecedor: TFDQuery;
    eCdComprador: TSlDBEdit;
    eComprador: TSLEdit;
    dsComprador: TDataSource;
    cdsComprador: TSLClientDataSet;
    dspComprador: TDataSetProvider;
    sdsComprador: TFDQuery;
    Label6: TLabel;
    Label9: TLabel;
    frDBNotaEntrada: TfrxDBDataset;
    frDBTitulo: TfrxDBDataset;
    frDBPedidoCompra: TfrxDBDataset;
    frxDockSite1: TfrxDockSite;
    qSQL: TFDQuery;
    cdsSQL: TClientDataSet;
    dspSQL: TDataSetProvider;
    procedure Imprimir(Sender: TObject);
    procedure Btn_LimparClick(Sender: TObject);
    procedure eCdEmpExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cdsemprezaAfterSql(Sender: TObject);
    procedure eCdFornecExit(Sender: TObject);
    procedure cdsFornecedorAfterSql(Sender: TObject);
    procedure eCdCompradorClickFind(Sender: TObject);
    procedure cdsCompradorAfterSql(Sender: TObject);
    procedure ImprimirFast(Sender: TObject);
    procedure Btn_VisualizarMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    procedure GerarRelatorio;
    procedure BuscaEmpresa;
  public
    { Public declarations }
    v_MultiEstoque: Boolean;
    v_EmpresaDefault: Integer;
    v_MultiEmpresa: Boolean;
  end;

var
  F_RelEntrada: TF_RelEntrada;

implementation

uses U_Funcoes, U_entrada;

{$R *.dfm}

procedure TF_RelEntrada.GerarRelatorio;
var
  sql: string;
  Situacao: string;
  i_cdnfSGE: TStringList;
begin
  cdsSQL.Close;
  cdsSQL.CommandText :=
    ' select ' +
    '   i_cdsequencial ' +
    ' from ' +
    '   nfbkp ' +
    ' where ' +
    '   date(d_cadastro) >= date(:d_ini) and ' +
    '   date(d_cadastro) <= date(:d_fim) and ' +
    '   i_cdsequencial is not null and ' +
    '   i_cdsequencial > 0 ';
  cdsSQL.ParamByName('d_ini').AsDate := eDataInicial.Date;
  cdsSQL.ParamByName('d_fim').AsDate := eDataFinal.Date;
  cdsSQL.Open;

  if not(cdsSQL.IsEmpty) then begin
    i_cdnfSGE := TStringList.Create;

    while not(cdsSQL.Eof) do begin
      if Trim(cdsSQL.FieldByName('i_cdsequencial').AsString) = '' then begin
        cdsSQL.Next;
        Continue;
      end;

      i_cdnfSGE.Add(cdsSQL.FieldByName('i_cdsequencial').AsString + ',');
      cdsSQL.Next;
    end;
  end;

  case cbxSituacao.ItemIndex of
    1: Situacao := ' and pedidocompra.f_situacao = ''P'' ';
    2: Situacao := ' and pedidocompra.f_situacao = ''E'' ';
    3: Situacao := ' and pedidocompra.f_situacao = ''C'' ';
    4: Situacao := ' and pedidocompra.f_situacao = ''N'' ';
    5: Situacao := ' and pedidocompra.f_situacao = ''R'' ';
    6: Situacao := ' and pedidocompra.f_situacao = ''A'' ';
    7: Situacao := ' and pedidocompra.f_situacao = ''I'' ';
  else
    Situacao := '';
  end;

  sql :=
    ' select ' +
    '   fornecedor.c_nome, ' +
    '   fornecedor.i_cdparceiro, ' +
    '   pedidocompra.i_cdpedidocompra, ' +
    '   pedidocompra.c_condicaopgto, ' +
    '   cast(pedidocompra.n_totalpedido as numeric(12,4)) as n_totalpedido, ' +
    '   cast(pedidocompra.n_vlrentrada as numeric(12,4)) as n_vlrentrada, ' +
    '   date(pedidocompra.d_pedido), ' +
    '   date(nfentrada.d_entrada), ' +
    '   date(pedidocompra.d_efetuacao) as d_efetuacao, ' +
    '   date(pedidocompra.d_entrega) as d_entrega, ' +
    '   cast( ' +
    '     case pedidocompra.f_situacao ' +
    '       when ''P'' then ''Constru��o'' ' +
    '       when ''E'' then ''Entregue'' '+
    '       when ''C'' then ''Cancelado'' '+
    '       when ''N'' then ''Efetuado'' '+
    '       when ''R'' then ''Entregue Parc.'' ' +
    '       when ''A'' then ''Pedido Entrada'' ' +
    '       when ''I'' then ''Indefirido'' ' +
    '     end as varchar(15) ' +
    '   ) as c_situacao, ' +
    '   funcionario.c_nome as comprador ' +
    ' from ' +
    '   pedidocompra ' +
    ' join fornecedor on pedidocompra.i_cdparc_fornec = fornecedor.i_cdparceiro ' +
    ' left join nfentrada on nfentrada.i_cdpedidocompra = pedidocompra.i_cdpedidocompra and ' +
    ' nfentrada.i_cdparceiro = fornecedor.i_cdparceiro ' +
    ' join funcionario using(i_cdfuncionario) ' +
    ' where ' +
    '   pedidocompra.i_cdempresa = :I_CdEmpresa ';

  case rgFiltro.ItemIndex of
    0: sql := sql + ' and date(pedidocompra.d_pedido) >= date(:D_Inicial) ' +
                    ' and date(pedidocompra.d_pedido) <= date(:D_Final) ';

    1: sql := sql + ' and date(pedidocompra.d_efetuacao) >= date(:D_Inicial) ' +
                    ' and date(pedidocompra.d_efetuacao) <= date(:D_Final) ';

    2: sql := sql + ' and date(nfentrada.d_entrada) >= date(:D_Inicial) ' +
                    ' and date(nfentrada.d_entrada) <= date(:D_Final) ';

    3: sql := sql + ' and date(pedidocompra.d_entrega) >= date(:D_Inicial) ' +
                    ' and date(pedidocompra.d_entrega) <= date(:D_Final) ';
  end;

  if eCdFornec.Text <> '' then
    sql := sql + ' and pedidocompra.i_cdparc_fornec = :I_CdFornec';

  if eCdComprador.Text <> '' then
    sql := sql + ' and funcionario.i_cdfuncionario = :I_CdFuncionario ';

  if Assigned(i_cdnfSGE) then
    sql := sql + ' and nfentrada.i_cdnfentrada in ( ' + Copy(Trim(i_cdnfSGE.Text), 1, Length(Trim(i_cdnfSGE.Text)) -1) + ' ) ';

  sql := sql + Situacao +
    ' group by ' +
    '   fornecedor.c_nome, ' +
    '   fornecedor.i_cdparceiro, ' +
    '   pedidocompra.i_cdpedidocompra, ' +
    '   pedidocompra.c_condicaopgto, ' +
    '   pedidocompra.n_totalpedido, ' +
    '   pedidocompra.n_vlrentrada, ' +
    '   date(nfentrada.d_entrada), ' +
    '   date(pedidocompra.d_pedido), ' +
    '   pedidocompra.f_situacao, ' +
    '   funcionario.c_nome, ' +
    '   pedidocompra.d_efetuacao, ' +
    '   pedidocompra.d_entrega ' +
    ' order by ' +
    '   pedidocompra.i_cdpedidocompra ';

  cdsPedidoCompra.Close;
  cdsPedidoCompra.CommandText := sql;
  cdsPedidoCompra.Params.ParamByName('I_CdEmpresa').AsInteger := StrToInt(ecdEmp.Text);

  if eCdFornec.Text <> '' then
    cdsPedidoCompra.Params.ParamByName('I_CdFornec').AsInteger := StrToInt(eCdFornec.Text);

  if eCdComprador.Text <> '' then
    cdsPedidoCompra.Params.ParamByName('I_CdFuncionario').AsInteger := StrToInt(eCdComprador.Text);

  cdsPedidoCompra.Params.ParamByName('D_Inicial').AsDate := StrToDate(eDataInicial.Text);
  cdsPedidoCompra.Params.ParamByName('D_Final').AsDate := StrToDate(eDataFinal.Text);
  cdsPedidoCompra.Open;

  cdsNotaEntrada.Close;
  cdsNotaEntrada.CommandText :=
    ' select ' +
    '   nfentrada.i_nrnf, ' +
    '   nfentrada.c_serienf, ' +
    '   date(nfentrada.d_entrada) as d_entrada, ' +
    '   cast(nfentrada.n_vlrnf as numeric(12,4)) as n_vlrnf, ' +
    '   pedidocompra.i_cdpedidocompra, ' +
    '   date(nfentrada.d_emissao) as d_emissao ' +
    ' from ' +
    '   nfentrada, ' +
    '   pedidocompra ' +
    ' where ' +
    '   pedidocompra.i_cdpedidocompra = nfentrada.i_cdpedidocompra ' ;
  cdsNotaEntrada.Open;

  cdsTitulo.Close;
  cdsTitulo.CommandText :=
    ' select ' +
    '   tipotitulo.c_sigla, ' +
    '   titulo.c_documento, ' +
    '   titulo.i_cdtitulo, ' +
    '   date(titulo.d_vencimento) as d_vencimento, ' +
    '   cast(titulo.n_vrtitulo as numeric(12,4)) as n_vrtitulo, ' +
    '   pedidocompra.i_cdpedidocompra ' +
    ' from ' +
    '   titulo, ' +
    '   pedidocompra, ' +
    '   tipotitulo ' +
    ' where ' +
    '   pedidocompra.i_cdpedidocompra = titulo.i_cdpedidocompra ' +
        Situacao +
    '   and tipotitulo.i_cdtipotitulo = titulo.i_cdtipotitulo ' +
    ' order by titulo.i_cdtitulo ' ;
  cdsTitulo.Open;
end;

procedure TF_RelEntrada.Imprimir(Sender: TObject);
begin
  if (eDataInicial.Text = '') or (eDataFinal.Text = '') then begin
    ShowMessage('Informe as datas do campo "Per�odo Entre".');
    eDataInicial.SetFocus;
    Exit;
  end;

  if eCdEmp.Text = '' then begin
    MessageDlg('Informe a Empresa!', mtError, [mbOK], 0);
    Exit;
  end;

  if Trunc(eDataFinal.Date) < Trunc(eDataInicial.Date) then begin
    MessageDlg('Data de Final deve ser maior ou igual a Data Inicial!', mtError, [mbOK], 0);
    eDataInicial.Date := Now();
    eDataFinal.Date := Now();
    Exit;
  end;

  ImprimirFast(Self);
end;

procedure TF_RelEntrada.ImprimirFast(Sender: TObject);
var
  vFiltros: TStringList;
begin
  vFileReport := 'fr_relentrada.fr3';
  inherited;
  GerarRelatorio;

  if cdsPedidoCompra.RecordCount = 0 then begin
    MessageDlg('N�o Cont�m Pedido nesse per�odo!', mtError, [mbOK], 0);
    Exit;
  end;

  vFiltros := TStringList.Create;
  vFiltros.Add('Filtro por data de: ' + rgFiltro.Items[rgFiltro.ItemIndex]);

  if cbxSituacao.Text = EmptyStr then
    vFiltros.Add('Todas Situa��es')
  else
    vFiltros.Add('Situa��o: ' + cbxSituacao.Text);

  if eCdFornec.Text <> EmptyStr then
    vFiltros.Add('Fornecedor: ' + eCdFornec.Text + ' - ' + eFornec.Text);

  if eCdComprador.Text <> EmptyStr then
    vFiltros.Add('Comprador: ' + eCdComprador.Text + ' - ' + eComprador.Text);

  if eCdEmp.Text <> EmptyStr then
    vFiltros.Add('Empresa: ' + eCdEmp.Text + ' - ' + eEmp.Text);

  frxReport.Variables['Nome'] := '''' + 'Relat�rio de Entrada' + '''';
  frxReport.Variables['DataInicial'] := '''' + FormatDateTime('dd/mm/yyyy', eDataInicial.Date) + '''' ;
  frxReport.Variables['DataFinal'] := '''' + FormatDateTime('dd/mm/yyyy', eDataFinal.Date) + '''';

  frxReport.Variables['Filtros'] := '' + vFiltros.Text + '';
  PrintFast;
end;

procedure TF_RelEntrada.Btn_LimparClick(Sender: TObject);
begin
  inherited;
  eDataInicial.Date := Now();
  eDataFinal.Date := Now();
  cdsFornecedor.RequestClear;
  cdsComprador.RequestClear;
  cdsempreza.RequestClear;
end;

procedure TF_RelEntrada.eCdEmpExit(Sender: TObject);
begin
  inherited;
  eCdEmp.FindNow;
  if eCdEmp.Text = '' Then
    eEmp.Text := ''
  else
    BuscaEmpresa;
end;

procedure TF_RelEntrada.Btn_VisualizarMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if (ssLeft in Shift) and (ssCtrl in Shift) then
    vClick := False;
end;

procedure TF_RelEntrada.BuscaEmpresa;
begin
  eCdEmp.FindNow;
  if eCdEmp.FieldsValues.Count > 0 Then
    eEmp.Text := eCdEmp.FieldsValues[2]
  else
    eEmp.Text := '';
end;

procedure TF_RelEntrada.FormCreate(Sender: TObject);
begin
  inherited;
  cdsempreza.RequestClear;
  cdsFornecedor.RequestClear;
  cdsComprador.RequestClear;
end;

procedure TF_RelEntrada.FormActivate(Sender: TObject);
begin
  inherited;
  cdsempreza.RequestFindID(v_EmpresaDefault);

  if not(MultiEmpresa) then begin
    eCdEmp.FindEdit := False;
    eCdEmp.Enabled := False;
    eCdEmp.Visible := False;
    eEmp.Visible := False;
    Label7.Visible := False;
    Label8.Visible := False;

    Self.Height := 315;
  end
  else
    Self.Height := 365;
end;

procedure TF_RelEntrada.cdsemprezaAfterSql(Sender: TObject);
begin
  inherited;
  eCdEmp.FindNow;
  eCdEmpExit(Sender);
end;

procedure TF_RelEntrada.eCdFornecExit(Sender: TObject);
begin
  inherited;
  if eCdFornec.Text = '' then
    eFornec.Text := ''
  else if eCdFornec.FieldsValues.Count > 0 then
    eFornec.Text := eCdFornec.FieldsValues[2]
  else
    eFornec.Text := '';
end;

procedure TF_RelEntrada.cdsFornecedorAfterSql(Sender: TObject);
begin
  inherited;
  eCdFornec.FindNow;
  eCdFornecExit(Sender);
end;

procedure TF_RelEntrada.eCdCompradorClickFind(Sender: TObject);
begin
  inherited;
  eCdComprador.FindNow;
  if Trim(eCdComprador.Text) = '' then
    eComprador.Text := ''
  else
  begin
    if eCdComprador.FieldsValues.Count > 0 then
      eComprador.Text := eCdComprador.FieldsValues[2]
    else
      eComprador.Text := '';
  end;
end;

procedure TF_RelEntrada.cdsCompradorAfterSql(Sender: TObject);
begin
  inherited;
  eCdComprador.FindNow;
  eCdCompradorClickFind(Sender);
end;

end.

