// Desenvolvido por : Vinicius Garcia Oliveira em 26/05/2004  */
// Alterado por.....: Vin�cius Garcia Oliveira em 20/09/2004  */
// Alterado por.....: Wendel Alves Machado     em 28/01/2005  */

unit U_PadraoCadastro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, U_Padrao, ComCtrls, ExtCtrls, DBCtrls, Grids, DBGrids, DB,  ADODB,
  StdCtrls, ImgList, ToolWin, Buttons, Menus,  DBClient, SLClientDataSet,
  SlDBFind, FMTBcd, SqlExpr, Provider, AppEvnts, System.ImageList,FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;



type
  TPadraoClass = class of TF_Padrao;


  TF_PadraoCadastro = class(TF_Padrao)
    ds_Cadastro: TDataSource;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;                                              
    Btn_Primeiro: TToolButton;
    Btn_Anterior: TToolButton;
    Btn_Proximo: TToolButton;
    Btn_Ultimo: TToolButton;
    Btn_Gravar: TToolButton;
    Btn_Apagar: TToolButton;
    ImageList: TImageList;
    Btn_Limpar: TToolButton;
    PopupMenu1: TPopupMenu;
    Limpar: TMenuItem;
    Primeiro: TMenuItem;
    Anterior: TMenuItem;
    Proximo: TMenuItem;
    Ultimo: TMenuItem;
    Salvar: TMenuItem;
    Excluir1: TMenuItem;
    Btn_Procura: TToolButton;
    Btn_Igual: TToolButton;
    Igual1: TMenuItem;
    Procura1: TMenuItem;
    SlDBFPesquisa: TSlDBFind;
    ImageListage2: TImageList;
    SlDSPesquisa: TFDQuery;
    procedure dbn_CadastroClick(Sender: TObject; Button: TNavigateBtn);
    procedure Btn_PrimeiroClick(Sender: TObject);
    procedure Btn_ProximoClick(Sender: TObject);
    procedure Btn_UltimoClick(Sender: TObject);
    procedure Btn_AnteriorClick(Sender: TObject);
    procedure Btn_LimparClick(Sender: TObject);
    procedure Btn_GravarClick(Sender: TObject);
    procedure Btn_ApagarClick(Sender: TObject);
    procedure Btn_ProcuraClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Btn_IgualClick(Sender: TObject);
    procedure SetSlClientDataSetDefault(const Value : TSlClientDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FSlClientDataSetDefault : TSlClientDataSet;

  public
    vI_IdTabela : Integer;
    constructor Create(AOwner: TComponent; SQLConnection: TSQLConnection); overload;
    constructor Create(AOwner: TComponent; SQLConnection: TSQLConnection; NomeForm:String); overload;
    procedure PosicionaId;
    Function ExecutaBpl(bpl:String): Integer;
    Function CriaPkg(NomePkg : String; ShowModal: Boolean = False):Integer;
    Function BuscaStatus: String;
  Published
    Property SlClientDataSetDefault : TSlClientDataSet Read FSlClientDataSetDefault  write SetSlClientDataSetDefault;
  end;

var
  F_PadraoCadastro: TF_PadraoCadastro;
{v_inicia  V_inicia : Boolean;}

implementation

uses dm, StrUtils, U_Funcoes;

{$R *.dfm}

procedure Register;
begin
  RegisterComponents('AcessExpress', [TF_PadraoCadastro]);
end;


procedure TF_PadraoCadastro.dbn_CadastroClick(Sender: TObject;
  Button: TNavigateBtn);
begin
   inherited;
{vini   If Button = nbDelete Then
      If Application.MessageBox('Confirma a exclus�o do registro?','Exclus�o de Registro',4) <> 6 Then
         If dbn_Cadastro.DataSource.DataSet.State = dsinsert Then
            Abort; }
end;

procedure TF_PadraoCadastro.Btn_PrimeiroClick(Sender: TObject);
begin
   inherited;
   // 02
   SlClientDataSetDefault.RequestFirst;
   BuscaStatus;
end;

procedure TF_PadraoCadastro.Btn_ProximoClick(Sender: TObject);
begin
   inherited;
   // 05
   SlClientDataSetDefault.RequestNext;
   BuscaStatus;

end;

procedure TF_PadraoCadastro.Btn_UltimoClick(Sender: TObject);
begin
   inherited;
   // 06
   SlClientDataSetDefault.RequestLast;
   BuscaStatus;
end;

procedure TF_PadraoCadastro.Btn_AnteriorClick(Sender: TObject);
begin
   inherited;
   // 03
  SlClientDataSetDefault.RequestPrior;
  BuscaStatus;
end;

procedure TF_PadraoCadastro.Btn_LimparClick(Sender: TObject);
begin
  inherited;
  // 01
  SlClientDataSetDefault.RequestClear;
  BuscaStatus;
end;

procedure TF_PadraoCadastro.Btn_GravarClick(Sender: TObject);
begin
  inherited;
  // 08
  SlClientDataSetDefault.RequestSave;
  BuscaStatus;
end;

procedure TF_PadraoCadastro.Btn_ApagarClick(Sender: TObject);
begin
  inherited;
  // 09
  SlClientDataSetDefault.RequestDelete;
  BuscaStatus;
end;

procedure TF_PadraoCadastro.Btn_ProcuraClick(Sender: TObject);
var FindPesquisa: TSlDBFind;
   i : Integer;
   PesquisaPrincipal : Boolean;
begin
  inherited;
  // 07
  PesquisaPrincipal := True;
  FindPesquisa := SlDBFPesquisa;
  For i := 0 to ComponentCount - 1 do
    If (Components[i] is TSlDBEdit) and
       (TSlDBEdit(Components[i]).Focused) Then
    begin
      if TSlDBEdit(Components[i]).FindEdit then
      begin
        TSlDBEdit(Components[i]).ExecuteFind;
        PesquisaPrincipal := False;
      end
      else
        FindPesquisa := TSlDBEdit(Components[i]).SlDBFind;
    end;
   if PesquisaPrincipal then
      if FindPesquisa.Execute = mrOk then
       If FindPesquisa.SearchFieldValue <> Unassigned Then
           SlClientDataSetDefault.RequestFindID(FindPesquisa.SearchFieldValue);
end;

procedure TF_PadraoCadastro.FormShow(Sender: TObject);
var i:integer;
begin
  inherited;
  For i := 0 to ComponentCount - 1 do
      If Components[i] is TSlDBEdit Then
         TSlDBEdit(Components[i]).SlDBFind := SlDBFPesquisa;
end;

procedure TF_PadraoCadastro.Btn_IgualClick(Sender: TObject);
begin
   inherited;
   // 04
   SlClientDataSetDefault.RequestFind;
   stb_Padrao.Panels[0].Text := BuscaStatus;
end;

procedure TF_PadraoCadastro.SetSlClientDataSetDefault(const
  value: TSlClientDataSet);
begin
  If Assigned(value) Then
    FSlClientDataSetDefault := value;
end;

procedure TF_PadraoCadastro.PosicionaId;
Begin
  if vI_IdTabela > 0 then
    SlClientDataSetDefault.RequestFindID(vI_IdTabela);
End;

procedure TF_PadraoCadastro.FormActivate(Sender: TObject);
begin
  Inherited;
  If Assigned(ds_Cadastro.DataSet) then
  begin
    If Not Assigned(SlClientDataSetDefault) Then
    Begin
      vI_IdTabela := StrToIntDef(getParametro(Application,'vI_IdTabela',False),0);
      SlClientDataSetDefault := TSlClientDataSet(ds_Cadastro.DataSet);
      if vI_IdTabela = 0 Then
        SlClientDataSetDefault.RequestClear
      Else
        PosicionaId;
    End;
  end;

  {v_inicia  if V_inicia then
    begin
      if Assigned(SlClientDataSetDefault) then
         SlClientDataSetDefault.RequestClear;
   end;
   v_inicia := False;}
end;

procedure TF_PadraoCadastro.FormCreate(Sender: TObject);
begin
  inherited;

{v_inicia  V_inicia := True;}
  SlDSPesquisa.Connection :=   TFDConnection(vSqlConnection);
  stb_Padrao.Panels[1].Text := getParametro(Application,'I_CdParceiroLogado') + '-' + getParametro(Application,'UsuarioLogin') ;
  stb_Padrao.Panels[2].Text := getParametro(Application,'NOMEEMPRESAPADRAO',False) ;
  //vParametro.Values['I_CdFuncionario'] + '-' + vParametro.Values['UsuarioLogin'] ;
  if FileExists(getParametro(Application,'DirServer',False)+'\vaa.dll') then
  begin
    ToolBar1.Images := ImageListage2;
    Color:=  cl3DLight;
    PopupMenu1.Images := ImageListage2;
  end;

end;

constructor TF_PadraoCadastro.Create(AOwner: TComponent;
  SQLConnection: TSQLConnection);
begin
  TF_Padrao(Self).Create(AOwner, SQLConnection)
end;

Function TF_PadraoCadastro.ExecutaBpl(bpl:String): Integer;
begin
  Result := CriaPkg(bpl);
end;

Function TF_PadraoCadastro.CriaPkg(NomePkg : String; ShowModal: Boolean = False): Integer;
Var vPath, NomeClass, NomeForm : String;
    Hm :HMODULE;
    vPersistentClass : TPersistentClass;
    VForm : TF_Padrao;
    vNome : String;
    vpermissao : String;
Begin

  vNome := NomePkg;
  vPath     := ExtractFilePath(Application.ExeName);
  NomeClass := 'TF_'  + NomePkg;
  NomeForm  := 'F_'   + NomePkg;
  NomePkg   := 'Pkg_' + NomePkg + '.bpl';

  AtualizarArquivo(getParametro(Application,'DirServer',False),NomePkg);

  Result := 0;

  If FileExists(vPath + NomePkg) Then
  Begin
     Hm := LoadPackage(vPath + NomePkg);
     vPersistentClass := GetClass(NomeClass);
     If vPersistentClass = Nil Then
        raise Exception.Create('Classe n�o localizada: ' + NomeClass);

     //se o usuario logado for super usuario nao precisa de permissoes
     if   getParametro(Application,'UsuarioLogin') <> getParametro(Application,'SUPERUSUARIO') then
     begin
       vpermissao :=  getParametro(Application,'Permissao'+vNome,False);
       if vpermissao = '' then vpermissao := '||';
     end
     else
       vpermissao := '';

      setParametro(Application,'Permissao',vpermissao);

     if  getParametro(Application,'UsuarioLogin') <> getParametro(Application,'SUPERUSUARIO') then
       if ((vpermissao = '') or (vpermissao = '||'))
       and ( Assigned(SlClientDataSetDefault)) then
       begin
         setParametro(Application,'Permissao' + vnome, SlClientDataSetDefault.Permissao);
         vpermissao := SlClientDataSetDefault.Permissao;
       end;


     if ( (vpermissao = '') or (Pos('|CONSULTA|',vpermissao) > 0) ) then
     begin
       If Application.FindComponent(NomeForm) = nil Then
         VForm :=  TPadraoClass(vPersistentClass).Create(Application, DMSGE.SQLConnection) as TF_PadraoCadastro
       Else
         VForm := Application.FindComponent(NomeForm) as TF_PadraoCadastro;


       setParametro(Application,'FieldID','0');



       if ShowModal then
         VForm.ShowModal
       else
        VForm.Show;

       Result  := StrToInt(getParametro(Application,'FieldID'));
       vForm.Tag := HM;
     end
     else
        MessageDlg('Usuario n�o possui permiss�o ' + #13 + 'para visualizar este programa', mtInformation,
      [mbOk], 0);

  End
  Else
  Begin
     ShowMessage('Pacote ' + NomePkg + ' n�o encontrado!');
  End;
End;


procedure TF_PadraoCadastro.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
//   IF  Assigned (SlClientDataSetDefault) and (SlClientDataSetDefault.Field_id > 0) then
//     setParametro(Application,'FieldID',IntToStr(SlClientDataSetDefault.Field_id));
end;

constructor TF_PadraoCadastro.Create(AOwner: TComponent;
  SQLConnection: TSQLConnection; NomeForm: String);
begin

   IF Application.FindComponent(NomeForm) = nil then
   Begin
     TF_Padrao(Self).Create(AOwner, SQLConnection);
     //setParametro(Application,'Permissao'+Copy(NomeForm,3,Length(NomeForm)),getParametro(Application,'Permissao'+Copy(Name,3,length(Name))));
   End
  else
    TF_Padrao(Self) := Application.FindComponent(NomeForm) as TF_Padrao;
end;

function TF_PadraoCadastro.BuscaStatus: String;
begin
  Result := '';
  if  Assigned(SlClientDataSetDefault) then
  begin
     with  SlClientDataSetDefault do
     begin
       if StateAction = sdClear then
       begin
          stb_Padrao.Panels[0].Text := 'Limpa';
          //lstauspadrao.Font.Color := clGray;
          //tbstatuspadrao.ImageIndex := 1;
          Result := 'Limpa';
       end
       else if StateAction = sdFind then
       begin
          stb_Padrao.Panels[0].Text := 'Pesquisa';
          //tbstatuspadrao.ImageIndex := 2;
          //lstauspadrao.Font.Color := clGreen;
          Result := 'Pesquisa';
       end
       else if StateAction = sdInsert then
       begin
          stb_Padrao.Panels[0].Text := 'Inserido';
          //tbstatuspadrao.ImageIndex := 3;
          //lstauspadrao.Font.Color := clBlue;
          Result := 'Inserido';
       end
       else if StateAction = sdUpdate then
       begin
          stb_Padrao.Panels[0].Text := 'Salvo';
          //tbstatuspadrao.ImageIndex := 4;
          //lstauspadrao.Font.Color := clBlue;
          Result := 'Salvo';
       end
       else if StateAction = sdDelete then
       begin
          stb_Padrao.Panels[0].Text := 'Excluido';
          //tbstatuspadrao.ImageIndex := 5;
          //lstauspadrao.Font.Color := clRed;
          Result := 'Excluido';
       end
       else
       begin
          stb_Padrao.Panels[0].Text := '';
         //  tbstatuspadrao.ImageIndex := 0;
         //  lstauspadrao.Font.Color := clGray;
          Result := '';
       end;
     end;
  end;
end;


end.
