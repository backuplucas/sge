object Bds: TBds
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 323
  Width = 547
  object SQLConnectionWV: TFDConnection
    Params.Strings = (
      'Database=siga'
      'User_Name=postgres'
      'Password=postgres'
      'Server=192.168.0.253'
      'MonitorBy=Remote'
      'DriverID=PG')
    FetchOptions.AssignedValues = [evItems, evCache, evAutoClose, evUnidirectional, evCursorKind]
    FetchOptions.CursorKind = ckDefault
    FetchOptions.Unidirectional = True
    FetchOptions.Items = [fiBlobs, fiDetails]
    FetchOptions.Cache = [fiBlobs, fiDetails]
    UpdateOptions.AssignedValues = [uvCheckReadOnly, uvAutoCommitUpdates]
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 184
    Top = 24
  end
  object SQLConnection: TFDConnection
    Params.Strings = (
      'Database=asa'
      'User_Name=postgres'
      'Password=postgres'
      'Port=5433'
      'Server=192.168.1.120'
      'DriverID=PG')
    FetchOptions.AssignedValues = [evMode, evItems, evCache, evAutoClose, evUnidirectional, evCursorKind, evAutoFetchAll, evLiveWindowParanoic, evLiveWindowFastFirst]
    FetchOptions.CursorKind = ckDefault
    FetchOptions.Unidirectional = True
    FetchOptions.Items = [fiBlobs, fiDetails]
    FetchOptions.Cache = [fiBlobs, fiDetails]
    UpdateOptions.AssignedValues = [uvCheckReadOnly, uvAutoCommitUpdates]
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 144
    Top = 120
  end
  object SQLConnectionError: TFDConnection
    Params.Strings = (
      'Database=siga'
      'User_Name=postgres'
      'Password=postgres'
      'Server=192.168.0.253'
      'DriverID=PG')
    FetchOptions.AssignedValues = [evItems, evCache, evAutoClose, evUnidirectional, evCursorKind]
    FetchOptions.CursorKind = ckDefault
    FetchOptions.Unidirectional = True
    FetchOptions.Items = [fiBlobs, fiDetails]
    FetchOptions.Cache = [fiBlobs, fiDetails]
    UpdateOptions.AssignedValues = [uvCheckReadOnly, uvAutoCommitUpdates]
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 200
    Top = 224
  end
  object FDPhysPgDriverLink1: TFDPhysPgDriverLink
    DriverID = 'PG'
    VendorLib = 'libpq.dll'
    Left = 288
    Top = 24
  end
  object RMonitor: TFDMoniRemoteClientLink
    Left = 272
    Top = 104
  end
end
