{=========================================================================
  SlFind Unit

  Componente de busca automatica
  Developing Wendel Alves Machado
  Copyright � 2005
=========================================================================}

unit SlDBFind;

interface

uses
  Classes, Graphics, Forms, DB, Dialogs, DBClient, Provider, Controls, StrUtils;

type
  TFindLocate = (flBegin,flMidle,flEnd,flNone);
  TSlDBFind = class( TComponent )
  private
    FBorderStyle : TFormBorderStyle;
    FCaption : string;
    FHeight : Integer;
    FFont : TFont;
    FSearchField : string;
    FWidth : Integer;
    FTableName: string;
    FFieldNames: TStrings;
    FFieldType: TStrings;
    FFindAutomatic: Boolean;
    FFindLocate: TFindLocate;
    FFieldDefault: string;
    FColumnTitle: TStrings;
    FDataSet: TDataSet;
    FSearchFieldValue: Variant;
    FFieldsValues: TStrings;
    FDataSetProvider : TDataSetProvider;
    FClientDataSet : TClientDataSet;
    FValue: Variant;
    FDataTypeValue: TFieldType;
    FWhere: String;
    FForeingKeyRelation: TStrings;
    FBpl: String;
    procedure SetFont( Value : TFont );
    procedure SetFieldNames(const Value: TStrings);
    procedure SetFieldType(const Value: TStrings);
    procedure SetFindAutomatic(const Value: Boolean);
    procedure SetFindLocate(const Value: TFindLocate);
    procedure SetFieldDefault(const Value: string);
    procedure SetColumnTitle(const Value: TStrings);
    procedure SetDataSet(const Value: TDataSet);
    procedure SetFieldsValues(const Value: TStrings);
    procedure SetValue(const Value: Variant);
    procedure SetDataTypeValue(const Value: TFieldType);
    procedure SetWhere(const Value: String);
    procedure SetForeingKeyRelation(const Value: TStrings);
    procedure SetBpl(const Value: String);
  protected
    procedure Notification( AComponent : TComponent;
                            Operation : TOperation ); override;

  public
    constructor Create( AOwner : TComponent ); override;
    destructor Destroy; override;
    function Execute : TModalResult;
    function FindNow : Boolean;
  published
    property BorderStyle : TFormBorderStyle      read FBorderStyle      write FBorderStyle      default bsSizeable;
    property Caption : string                    read FCaption          write FCaption;
    property TableName : string                  read FTableName        write FTableName;
    property Font : TFont                        read FFont             write SetFont;
    property Height : Integer                    read FHeight           write FHeight           default 300;
    property SearchField : string                read FSearchField      write FSearchField;
    property SearchFieldValue : Variant          read FSearchFieldValue;
    property Width : Integer                     read FWidth            write FWidth            default 631;
    property FieldNames : TStrings               read FFieldNames       write SetFieldNames;
    property FieldType  : TStrings               read FFieldType        write SetFieldType;
    property FindAutomatic : Boolean             read FFindAutomatic    write SetFindAutomatic  default True;
    property FindLocate: TFindLocate             read FFindLocate       write SetFindLocate;
    property FieldDefault: string                read FFieldDefault     write SetFieldDefault;
    property ColumnTitle: TStrings               read FColumnTitle      write SetColumnTitle;
    property DataSet: TDataSet                   read FDataSet          write SetDataSet;
    property FieldsValues: TStrings              read FFieldsValues     write SetFieldsValues;
    property Value : Variant                     read FValue            write SetValue;
    property DataTypeValue :TFieldType read FDataTypeValue write SetDataTypeValue;
    property Where : String read FWhere write SetWhere;
    property ForeingKeyRelation: TStrings read FForeingKeyRelation write SetForeingKeyRelation;
    property Bpl: String read FBpl write SetBpl;
 end;

 procedure Register;


implementation
uses
  Windows, UDBFindForm, SysUtils, Variants;


procedure Register;
begin
  RegisterComponents('AcessExpress', [TSlDBFind]);
end;



constructor TSlDBFind.Create( AOwner : TComponent );
begin
  inherited Create( AOwner );

  FDataSetProvider := TDataSetProvider.Create(self);
  FDataSetProvider.Options := [poAllowCommandText];
  FDataSetProvider.Name := 'dsp'+Name;
  FClientDataSet := TClientDataSet.Create(self);
  FClientDataSet.ProviderName :=  FDataSetProvider.Name;
 
  FHeight := 350;
  FWidth := 680;
  FBorderStyle := bsSizeable;
  FFont := TFont.Create;
  FFieldNames  := TStringList.Create;
  FFieldType   := TStringList.Create;
  FColumnTitle := TStringList.Create;
  FFieldsValues := TStringList.Create;
  FForeingKeyRelation := TStringList.Create;
  if Owner is TForm then                          // If Owner is a form...
    FFont.Assign( TForm( Owner ).Font ); // Use the form's font by default
end;


destructor TSlDBFind.Destroy;
begin
  FFont.Free;
  FFieldNames.Free;
  FColumnTitle.Free;
  FFieldType.Free;
  FFieldsValues.Free;
  FClientDataSet.Free;
  FDataSetProvider.Free;
  FForeingKeyRelation.Free;

  inherited Destroy;
end;


procedure TSlDBFind.SetFont( Value : TFont );
begin
  FFont.Assign( Value );
end;


function TSlDBFind.Execute : TModalResult;
var i: integer;
begin
 SlDBLookupForm := TSlDBLookupForm.Create( Application );
  try
    if FDataSet.Active then FDataSet.Close;
    // inicializa propriedades
    SlDBLookupForm.BorderStyle := FBorderStyle;
    SlDBLookupForm.Width := FWidth;
    SlDBLookupForm.Height := FHeight;
    SlDBLookupForm.Font := FFont;
    SlDBLookupForm.cbCampos.Items := FColumnTitle;
//    SlDBLookupForm.cbBuscaAutmatica.Checked := FFindAutomatic;
    SlDBLookupForm.gbBusca.Visible := True;
    SlDBLookupForm.vTableName := FTableName;
    SlDBLookupForm.DataSetProvider.DataSet := FDataSet;
    SlDBLookupForm.vColumns:= FFieldNames;
    SlDBLookupForm.vSearchField := FSearchField;
    SlDBLookupForm.vWhere := FWhere;
    SlDBLookupForm.vFieldType := FFieldType;
    SlDBLookupForm.ForeingKeyRelation  := FForeingKeyRelation;
    SlDBLookupForm.vActiveButton := FBpl <> '';

     SlDBLookupForm.cbCampos.ItemIndex := FFieldNames.IndexOf(FFieldDefault);
     if (SlDBLookupForm.cbCampos.ItemIndex  = -1) or
        (SlDBLookupForm.cbCampos.ItemIndex  >= SlDBLookupForm.cbCampos.Items.Count) then
       SlDBLookupForm.cbCampos.ItemIndex := 0;

    {
     Case FFindLocate of
     flBegin : SlDBLookupForm.rgBusca.ItemIndex  := 0;
     flMidle : SlDBLookupForm.rgBusca.ItemIndex  := 1;
     flEnd   : SlDBLookupForm.rgBusca.ItemIndex  := 2;
     flNone  : SlDBLookupForm.rgBusca.Visible := False;
    end;
    }

    SlDBLookupForm.Caption := 'Busca ' + FCaption;
    SlDBLookupForm.vCaption := FCaption;

    Result := SlDBLookupForm.ShowModal;
    if Result = idOK then     // Display the dialog box
    begin
      FFieldsValues.Clear;
      If SlDBLookupForm.ClientDataSet.Active And
         (SlDBLookupForm.ClientDataSet.RecordCount > 0) Then
         begin
           with  SlDBLookupForm do
           begin
             FSearchFieldValue := ClientDataSet.FieldValues['key'];

             //Carrega Resultados no  fieldsvalues
             FFieldsValues.Add(ClientDataSet.FieldByName('key').AsString);
             For i := 0 to FFieldNames.Count -1 do
             begin

              // if UpperCase(FFieldNames[i]) <> UpperCase(FSearchField) then
                 //FFieldsValues.Add(ClientDataSet.FieldByName(FFieldNames[i]).AsString);
                 FFieldsValues.Add(ClientDataSet.FieldByName('Campo' +InttoStr(i+1)).AsString);
             end;
           end;
         end;
    end;
  finally
    SlDBLookupForm.Release;                    // Don't forget to free the form
  end;
end;

function TSlDBFind.FindNow: Boolean;
 var Sql : String;
   i : Integer;
begin
   FDataSetProvider.DataSet := DataSet;
   Sql := 'Select ' + FSearchField + ' as key ';
   for i:= 0  to FFieldNames.Count -1 do
   begin
   //  if UpperCase(FFieldNames[i]) <>  UpperCase(FSearchField) then
   //  begin
       Sql := Sql + ',';
       Sql := Sql + FFieldNames[i] + ' as Campo'+ IntToStr(i+1);
   //  end;
   end;

   Sql := Sql + ' from ' + FTableName;

  //acresenta as tabelas relacionadas
  //sendo a tabela a primeira que aparece no join de relacionamento
  for i:= 0 to ForeingKeyRelation.Count -1 do
    Sql := Sql +' ' + ForeingKeyRelation.Strings[i];

   Sql := Sql + ' where ' + FSearchField  + ' = ';
   if FDataTypeValue = ftString then
     Sql := Sql + '''' + VarToStr(value) + ''''
   else
     Sql := Sql +  VarToStr(value);

   //acresenta os join do relacionamento
//   for i:= 0 to FForeingKeyRelation.Count -1 do
//     Sql := Sql +' and ' + FForeingKeyRelation.Strings[i];


  if (Length(Trim(FWhere)) > 0) then
     Sql := Sql + ' and ' + FWhere;

   Sql := Sql + ' limit 1 ';

   FClientDataSet.Close;
   FClientDataSet.CommandText := Sql;
   FClientDataSet.Open;

   FSearchFieldValue := '';
   FFieldsValues.Clear;
   if FClientDataSet.RecordCount > 0 then
   begin
     FClientDataSet.First;
     FSearchFieldValue := FClientDataSet.FieldValues['key'];
     //Carrega Resultados no  fieldsvalues
     FFieldsValues.Add(FClientDataSet.FieldByName('Key').AsString);
     For i := 0 to FFieldNames.Count -1 do
     begin
      // if UpperCase(FFieldNames[i]) <> UpperCase(FSearchField) then
          FFieldsValues.Add(FClientDataSet.FieldByName('Campo'+ IntToStr(i+1)).AsString);
     end;
     Result := true
   end
   else
     Result := false;
end;

procedure TSlDBFind.SetFieldNames(const Value: TStrings);
begin
  FFieldNames.Assign(Value);
end;

procedure TSlDBFind.SetFindAutomatic(const Value: Boolean);
begin
  FFindAutomatic := Value;
end;

procedure TSlDBFind.SetFindLocate(const Value: TFindLocate);
begin
  FFindLocate := Value;
end;

procedure TSlDBFind.SetFieldDefault(const Value: string);
begin
  FFieldDefault := Value;
end;

procedure TSlDBFind.SetColumnTitle(const Value: TStrings);
begin
  FColumnTitle.Assign(Value);
end;


procedure TSlDBFind.SetDataSet(const Value: TDataSet);
begin
  FDataSet := Value;
end;


procedure TSlDBFind.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification( AComponent, Operation );
  if ( Operation = opRemove ) and ( AComponent = FDataset ) then
    FDataset := nil;
end;

procedure TSlDBFind.SetFieldsValues(const Value: TStrings);
begin
  FFieldsValues.Assign(Value);
end;

procedure TSlDBFind.SetValue(const Value: Variant);
begin
  FValue := Value;
end;

procedure TSlDBFind.SetDataTypeValue(const Value: TFieldType);
begin
  FDataTypeValue := Value;
end;

procedure TSlDBFind.SetWhere(const Value: String);
begin
  FWhere := Value;
end;

procedure TSlDBFind.SetFieldType(const Value: TStrings);
begin
  FFieldType.Assign(Value);
end;

procedure TSlDBFind.SetForeingKeyRelation(const Value: TStrings);
begin
  FForeingKeyRelation.Assign(Value);
end;

procedure TSlDBFind.SetBpl(const Value: String);
begin
  FBpl := Value;
end;

end.

