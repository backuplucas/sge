object dmSGE: TdmSGE
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 186
  Width = 268
  object SQLConnectionBkp: TFDConnection
    Params.Strings = (
      'Database=autolevesge'
      'User_Name=postgres'
      'Password=postgres'
      'Server=192.168.0.117'
      'Port=5433'
      'DriverID=PG')
    FetchOptions.AssignedValues = [evItems, evUnidirectional, evCursorKind]
    FetchOptions.CursorKind = ckDefault
    FetchOptions.Unidirectional = True
    FetchOptions.Items = [fiBlobs, fiDetails]
    UpdateOptions.AssignedValues = [uvCheckReadOnly, uvAutoCommitUpdates]
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 32
    Top = 72
  end
  object SQLConnection: TFDConnection
    Params.Strings = (
      'Database=autoleveage'
      'User_Name=postgres'
      'Password=postgres'
      'Server=192.168.0.117'
      'Port=5433'
      'DriverID=PG')
    FetchOptions.AssignedValues = [evItems, evUnidirectional, evCursorKind]
    FetchOptions.CursorKind = ckDefault
    FetchOptions.Unidirectional = True
    FetchOptions.Items = [fiBlobs, fiDetails]
    UpdateOptions.AssignedValues = [uvCheckReadOnly, uvAutoCommitUpdates]
    UpdateOptions.CheckReadOnly = False
    UpdateOptions.AutoCommitUpdates = True
    LoginPrompt = False
    Left = 136
    Top = 72
  end
  object FDPhysPgDriverLink1: TFDPhysPgDriverLink
    DriverID = 'PG'
    VendorLib = 'libpq.dll'
    Left = 80
    Top = 24
  end
end
