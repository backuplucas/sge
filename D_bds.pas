// Desenvolvido por : Wendel  26/12/2017  */
// Alterado por.....:                                         */
unit D_bds;                                      
interface


  uses FireDAC.Stan.Intf, FireDAC.Stan.Option,  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,  FireDAC.Comp.DataSet, FireDAC.Comp.Client,  Data.DB,
  SysUtils, Classes, ADODB, DBClient, Provider,  FMTBcd, Vcl.ExtCtrls, SqlExpr,Vcl.Dialogs,
  FireDAC.Phys.PG, FireDAC.Phys.PGDef, FireDAC.Moni.Base,
  FireDAC.Moni.RemoteClient;

type
  TBds = class(TDataModule)
    SQLConnectionWV: TFDConnection;
    SQLConnection: TFDConnection;
    SQLConnectionError: TFDConnection;
    FDPhysPgDriverLink1: TFDPhysPgDriverLink;
    RMonitor: TFDMoniRemoteClientLink;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    
  public
    { Public declarations }
    vparametro : TStrings;

  end;

var
  Bds: TBds;

implementation

{$R *.dfm}

procedure TBds.DataModuleDestroy(Sender: TObject);
begin

  vparametro.Free;
  SQLConnectionWV.Connected := False;
  SQLConnection.Connected   := False;
end;


procedure TBds.DataModuleCreate(Sender: TObject);
begin
  vparametro := TStringList.Create;
  If SQLConnectionWV.Connected Then ShowMessage('Desconectar SlqConnectionWV!');
  If SQLConnection.Connected Then ShowMessage('Desconectar SqlConnection!');
end;

end.








