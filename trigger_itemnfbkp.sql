﻿-- Function: ft_itemnfbkp()

-- DROP FUNCTION ft_itemnfbkp();

CREATE OR REPLACE FUNCTION ft_itemnfbkp()
  RETURNS trigger AS
$BODY$Declare
  vI_CdNfBkp NfBkp.I_CdNfBkp%Type;
  vF_Fechado NfBkp.F_Fechado%Type;
Begin

   /*       Usuario que nao executa a trigger    */
   If User = 'free' Then
      If TG_OP = 'DELETE' Then
         Return Old;
      Else
         Return New;
      End If;
   End If;

   /*    BLOQUEIA ALTERAÇÃO NA PRIMARY KEY   */
   If TG_OP = 'UPDATE' Then
      If (NEW.I_CDITEMNFBKP <> OLD.I_CDITEMNFBKP) Then
         RAISE EXCEPTION 'O(s) campo(s) I_CDITEMNFBKP nao pode(m) ser alterado(s).';
      End If;
   End If;

   If TG_OP = 'INSERT' OR TG_OP = 'UPDATE' Then
      /*       EXECUTANDO UPPERCASE NOS CAMPOS         */
      New.C_CDFORNEC := Trim(Upper(New.C_CDFORNEC));
      /*               ATRIBUINDO VALORES DEFAULT           */
   End If; 

   /* Inicio das Regras Manuais                                  */
	-- 01
	If TG_OP = 'INSERT' Then
		If New.I_CdItemNfBkp Is Null Then
			Select Max(I_CdItemNfBkp) + 1
			Into New.I_CdItemNfBkp
			From ItemNfBkp;     
		End If;
		If New.I_CdItemNfBkp Is Null Then
			New.I_CdItemNfBkp := 1;
		End If;   
	End If;

	-- 02
	If TG_OP = 'INSERT' Then
		If New.i_ctitem Is Null Then
			Select Max(i_ctitem) + 1
			into  New.i_ctitem
			from itemnfbkp
			where i_cdnfbkp = New.i_cdnfbkp;
		End If;
		If New.i_ctitem is Null Then
			New.i_ctitem := 1;
		End If;
	End If;

	-- 04 Barrar ação se NF tiver fechada
	If TG_OP = 'INSERT' Or TG_OP = 'UPDATE' Then
		vI_CdNfBkp := New.I_CdNfBkp;
	Else
		vI_CdNfBkp := Old.I_CdNfBkp;
	End If;
	
	Select F_Fechado
	Into vF_Fechado
	From NfBkp
	Where I_CdNfBkp = vI_CdNfBkp;
	If vF_Fechado = 'S' Then
		Raise Exception 'Não é permitido alterção em itens de um complemento já fechado!';
	End If;

   /* Fim das Regras Manuais                                     */

   /*         CHECAGEM DOS CAMPOS DE FLAG      */
   If TG_OP = 'INSERT' OR TG_OP = 'UPDATE' Then
      /*   VERIFICANDO SE INFORMADO APENAS VALORES PERMITIDOS    */
      If  Not(New.I_CDPRODUTO > 0) Then
         RAISE EXCEPTION 'O valor informado para o campo I_CDPRODUTO esta invalido. (I_CDPRODUTO > 0)';
      End If;

     /* CHECANDO SE NAO TEM VALOR NULO PARA AS CHAVES... */
        If New.I_CdNFBKP Is Null Then
          Raise Exception 'Faltando Informar o I_NFBKP!';
       End If;
   End If;

   If TG_OP = 'DELETE' Then
      Return Old;
   Else
      Return New;
   End If;

End;$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION ft_itemnfbkp() OWNER TO postgres;
