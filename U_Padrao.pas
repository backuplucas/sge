unit U_Padrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, DB, DbClient, SLClientDataSet,
  Printers, DBGrids, ShellAPI, ShlObj, ExtCtrls, ToolWin, ImgList, Vcl.AppEvnts;

const
  WM_DestroyForm = WM_USER;

type
  TF_Padrao = class(TForm)
    stb_Padrao: TStatusBar;
    ApplicationEvents2: TApplicationEvents;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure ApplicationEvents2Message(var Msg: tagMSG; var Handled: Boolean);
  private
    FFocusInit: TWinControl;
    FFormPai: TForm;
    Procedure AlternaForm;
  public
    vF_AbreDataSets: Boolean;
    vC_NomeDepto: string;
    vC_Permissao: string;
    vSqlConnection: TComponent;

    procedure setFormPai(pai:TForm);
    function getFormPai():Tform;
    constructor Create(AOwner: TComponent; SQLConnection: TComponent);
    function StrToFloatDef(s: string; Def: Extended): Extended;
    function RetirarCaractere(s: String; Caractere: Char): string;
    function RetirarEspacos(s: string): string;
    function BrowseDialog(const Title: string): string;
  published
    property FocusInit: TWinControl read FFocusInit write FFocusInit;
  end;

var
  F_Padrao: TF_Padrao;
  v_Form: Integer;

  function PreencheString(Valor, tipo: string; Tam: Integer; Caracter: string): string;
  function CopyLength(var Ext: string; Tam: Integer): string;
  function CorrentPrinter: string;

implementation

uses StrUtils, U_AlternaForm, U_Funcoes, U_Monitor;

{$R *.dfm}

{ TF_Padrao }

function TF_Padrao.StrToFloatDef(s: string; Def: Extended): Extended;
begin
  try
    Result := StrToFloat(s);
  except
    Result := Def;
  end;
end;

procedure TF_Padrao.FormShow(Sender: TObject);
begin
  if Assigned(ActiveControl) then
    FFocusInit := ActiveControl;
end;

procedure TF_Padrao.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vPath: string;
  DataArq: string;
  vPermissao: string;
begin
  if (Shift = [ssCtrl]) and (Key = VK_F12) then begin
    vPath := ExtractFilePath(Application.ExeName);

    if FileExists(vPath + 'pkg' + Copy(Name, 2, Length(Name)) + '.bpl')  then begin
      if  getParametro(Application, 'UsuarioLogin') <> getParametro(Application, 'SUPERUSUARIO') then
        vPermissao := 'Permiss�es: ' + getParametro(Application, 'Permissao' + Copy(Name, 3, Length(Name)), False)
      else
        vPermissao := 'Permiss�es: SuperUsu�rio!';

      DataArq := DateTimeToStr(FileDateToDateTime(FileAge(vPath + 'pkg' + Copy(Name, 2, Length(Name)) + '.bpl')));
      ShowMessage('Arquivo: ' + Name + ' Vers�o: ' + DataArq + #13 +  vPermissao);
    end
    else
      ShowMessage('Arquivo: ' + Name);
  end;

  if (Shift = [ssCtrl, ssAlt]) and (Key = VK_F11) then begin
    Application.CreateForm(TF_Monitor, F_Monitor);
    F_Monitor.Show;
  end;

  if (Shift = [ssCtrl, ssShift]) then
    AlternaForm;

  if ActiveControl is TCustomMemo then
    if TSlDBMemo(ActiveControl).Lines[TSlDBMemo(ActiveControl).Lines.Count -1] <> '' then
      Exit;

  if ActiveControl is TDBGrid then begin
    if Key = VK_SPACE then begin
      if Assigned(TDBGrid(ActiveControl).OnDblClick) then
        TDBGrid(ActiveControl).OnDblClick(ActiveControl);
    end;
  end;

  if Key = VK_RETURN then begin
    if Shift = [ssShift]  then
      SelectNext(ActiveControl, False, True)
    else
      SelectNext(ActiveControl, True, True);
  end;

  if
    (Key = VK_ESCAPE)
    and
    (Assigned(FFocusInit))
    and
    (FFocusInit.CanFocus)
  then
    FFocusInit.SetFocus;

  stb_Padrao.Panels[0].Text := 'Alterando';
end;

procedure TF_Padrao.FormCreate(Sender: TObject);
begin
  inherited;
  vF_AbreDataSets := True;
end;

procedure TF_Padrao.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i: Integer;
begin
  if Action = caNone then
    Exit;

  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TDataSource then
      if TDataSource(Components[i]).DataSet <> nil then
        TDataSet(TDataSource(Components[i]).DataSet).Active := False;
  end;

  Application.ProcessMessages;

  if Tag <> 0 then
    PostMessage(Application.Handle, WM_DestroyForm, Tag, Integer(Sender));

  Release;
end;

function TF_Padrao.RetirarEspacos(s: String): String;
begin
  Result := S;
  while Pos(' ', Result) > 0 do
  Result := Copy(Result, 1, Pos(' ', Result) - 1) + Copy(Result, Pos(' ', Result) + 1, Length(Result));
end;

function TF_Padrao.RetirarCaractere(s: String; Caractere : Char): String;
begin
  Result := S;
  while Pos(Caractere, Result) > 0 do
    Result := Copy(Result, 1, Pos(Caractere, Result) - 1) + Copy(Result, Pos(Caractere, Result) + 1, Length(Result));
end;

function PreencheString(Valor, Tipo: string; Tam: Integer; Caracter: string): string;
begin
  Valor := Trim(Valor);

  while Length(Valor) < Tam do begin
    if tipo = 'D' then
      Valor := Valor + Caracter
    else
      Valor := Caracter + Valor;
  end;

  if Length(Valor) > Tam then
    Valor := Copy(Valor, 0, Tam);

  Result :=  Valor;
end;

Function CopyLength(var Ext:String; Tam:Integer):String;
var
  ExtRet: string;
  count: Integer;
begin
  Ext := Trim(Ext);

  if Length(Ext) <= Tam then begin
    Result := Ext;
    Ext := '';
    Exit;
  end;

  ExtRet := '';
  count := Tam + 1;

  while (Ext[count] <> ' ') and (count > 0) do
    Dec(count);

  if (count > 0) then begin
    ExtRet := Copy(Ext, 1, count);
    Ext := Copy(Ext, count + 1, Length(Ext));
  end
  else begin
    ExtRet := Copy(Ext, 1, Tam);
    Ext := Copy(Ext, Tam + 1, Length(Ext));
  end;

  Result := ExtRet;
end;

function CorrentPrinter: string;
var
  Device: array[0..255] of char;
  Driver: array[0..255] of char;
  Port: array[0..255] of char;
  hDMode: THandle;
  Imp: string;
begin
  Printer.GetPrinter(Device, Driver, Port, hDMode);
  Imp := Device;

  if Copy(Device, 1, 2) = '\\' then
    Result := Device
  else
    Result := Port;
end;

function BrowseDialogCallBack(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
var
  wa: TRect;
  rect: TRect;
  dialogPT: TPoint;
begin
  if uMsg = BFFM_INITIALIZED then begin
    wa := Screen.WorkAreaRect;
    GetWindowRect(Wnd, Rect);

    dialogPT.X := ((wa.Right-wa.Left) div 2) - ((rect.Right-rect.Left) div 2);
    dialogPT.Y := ((wa.Bottom-wa.Top) div 2) - ((rect.Bottom-rect.Top) div 2);

    MoveWindow(
      Wnd,
      dialogPT.X,
      dialogPT.Y,
      Rect.Right - Rect.Left,
      Rect.Bottom - Rect.Top,
      True
    );
  end;

  Result := 0;
end;

procedure TF_Padrao.ApplicationEvents2Message(var Msg: tagMSG; var Handled: Boolean);
var
  i: SmallInt;
begin
  if (ActiveControl is TCustomControl) or (ActiveControl is TCustomEdit) then begin
    if Msg.message = WM_MOUSEWHEEL then begin
      Msg.message := WM_KEYDOWN;
      Msg.lParam := 0;
      i := HiWord(Msg.wParam);

      if i > 0 then
        Msg.wParam := VK_UP
      else
        Msg.wParam := VK_DOWN;

      Handled := False;
    end;
  end;
end;

function TF_Padrao.BrowseDialog(const Title: string): string;
var
  lpItemID: PItemIDList;
  BrowseInfo: TBrowseInfo;
  DisplayName: array[0..MAX_PATH] of char;
  TempPath: array[0..MAX_PATH] of char;
  Flag: Integer;
begin
  Flag := BIF_RETURNONLYFSDIRS;
  Result := '';
  FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);

  with BrowseInfo do begin
    hwndOwner := Application.Handle;
    pszDisplayName := @DisplayName;
    lpszTitle := PChar(Title);
    ulFlags := Flag;
    lpfn := BrowseDialogCallBack;
  end;

  lpItemID := SHBrowseForFolder(BrowseInfo);

  if lpItemId <> nil then begin
    SHGetPathFromIDList(lpItemID, TempPath);
    Result := TempPath;
    GlobalFreePtr(lpItemID);
  end;
end;

procedure TF_Padrao.FormActivate(Sender: TObject);
var
  i: Integer;
  NomeForm: string;
begin
  inherited;
  if vF_AbreDataSets then begin
    for i := 0 to ComponentCount - 1 do begin
      if TComponent(Components[i]).Tag <> 1 then begin
        if
          Components[i] is TClientDataSet
          and
          (TClientDataSet(Components[i]).CommandText <> '')
          and
          not(TClientDataSet(Components[i]).Active)
        then
          TClientDataSet(Components[i]).Active := True;

        if
          (Components[i] is TSLClientDataSet)
          and
          (TSLClientDataSet(Components[i]).CommandText <> '')
          and
          not(TSLClientDataSet(Components[i]).Active)
        then
          TSLClientDataSet(Components[i]).Active := True;
      end;

      if
        (Components[i] is TSLClientDataSet)
        and
        (TSLClientDataSet(Components[i]).CommandText <> '')
      then begin
        if getParametro(Application,'UsuarioLogin') <> getParametro(Application,'SUPERUSUARIO') then begin
          NomeForm := 'Permissao' + Copy(Name, 3, Length(Name));
          if getParametro(Application, NomeForm, False) <> '' then
            TSLClientDataSet(Components[i]).Permissao := getParametro(Application, NomeForm);
        end;
      end;

      if FileExists(getParametro(Application, 'DirServer', False) + '\vaa.dll') then begin
        if (Components[i] is TForm) then begin
          if TForm(Components[i]).Color = clBtnFace then
            TForm(Components[i]).Color := cl3DLight;

          if TForm(Components[i]).Font.Name = 'MS Sans Serif' then begin
            TForm(Components[i]).Font.Name := 'Times New Roman';
            TForm(Components[i]).Font.Size := TForm(Components[i]).Font.Size + 2;
          end;
        end;

        if (Components[i] is TLabel) then begin
          if TLabel(Components[i]).Font.Color = clWindowText then
            TLabel(Components[i]).Font.Color := clNavy;
        end;

        if (Components[i] is TPanel) then begin
          if TPanel(Components[i]).Color = clBtnFace then
            TPanel(Components[i]).Color := cl3DLight;
        end;

        if (Components[i] is TGroupBox) then begin
          if TGroupBox(Components[i]).Font.Name = 'MS Sans Serif' then begin
            TGroupBox(Components[i]).Font.Name := 'Times New Roman';
            TGroupBox(Components[i]).Font.Size := TGroupBox(Components[i]).Font.Size + 2;
          end;
        end;

        if (Components[i] is TDBGrid) then begin
          if TDBGrid(Components[i]).TitleFont.Name = 'MS Sans Serif' then begin
            TDBGrid(Components[i]).TitleFont.Name := 'Times New Roman';
            TDBGrid(Components[i]).TitleFont.Size := TDBGrid(Components[i]).TitleFont.Size + 2;
          end;

          if TDBGrid(Components[i]).Color = clWindow then
            TDBGrid(Components[i]).Color := clBtnFace;

          if TDBGrid(Components[i]).FixedColor = clBtnFace then
            TDBGrid(Components[i]).FixedColor := clSkyBlue;
        end;
      end;
    end;
  end;

  if FileExists(getParametro(Application, 'IconeSistema')) then
    Icon.LoadFromFile(getParametro(Application, 'IconeSistema'));
end;

procedure TF_Padrao.AlternaForm;
begin
  Application.CreateForm(TFAlternaForm, FAlternaForm);
  FAlternaForm.ShowModal;
  v_Form := FAlternaForm.v_Form;
  FAlternaForm.Free;

  if (v_Form >= 0) and (Application.Components[v_Form] is TForm) then begin
    TForm(Application.Components[v_Form]).SetFocus;

    if TForm(Application.Components[v_Form]).WindowState = wsMinimized then
      TForm(Application.Components[v_Form]).WindowState := wsNormal;
  end;
end;

constructor TF_Padrao.Create(AOwner: TComponent; SQLConnection: TComponent);
begin
  vSqlConnection := SQLConnection;
  TCustomForm(Self).Create(AOwner);
end;

procedure TF_Padrao.setFormPai(pai: TForm);
begin
  FFormPai := pai;
end;

function TF_Padrao.getFormPai: Tform;
begin
  Result := FFormPai;
end;

end.
