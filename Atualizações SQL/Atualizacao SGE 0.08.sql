﻿/*	
DESCRICAO----: SGE 0.08
DATA---------: 31/05/2012
*/

/****************************** CRIA COLUNA "I_CDSEQUENCIAL_PC" ******************************/

ALTER TABLE NFBKP
 ADD COLUMN I_CDSEQUENCIAL_PC INTEGER;
/****************************** ALTERA TRIGGER "NFBKP" ******************************/

/* CRIACAO DA FUNCTION TRIGGER                                   */
Create Or Replace Function FT_NFBKP() Returns Trigger As
'Declare
   VTotalItens NfBkp.N_VlrProdutos%type;
Begin
   /* Function Trigger : T_NFBKP                            */
   
   /*       Usuario que nao executa a trigger    */
   If User = ''free'' Then
      If TG_OP = ''DELETE'' Then
         Return Old;
      Else
         Return New;
      End If;
   End If;

   /*    BLOQUEIA ALTERAÇÃO NA PRIMARY KEY   */
   If TG_OP = ''UPDATE'' Then
      If (NEW.I_CDNFBKP <> OLD.I_CDNFBKP) Then
         RAISE EXCEPTION ''O(s) campo(s) I_CDNFBKP nao pode(m) ser alterado(s).'';
      End If;
   End If;

   If TG_OP = ''INSERT'' OR TG_OP = ''UPDATE'' Then

      /*       EXECUTANDO UPPERCASE NOS CAMPOS         */
      New.F_FECHADO := Trim(Upper(New.F_FECHADO));
      New.C_DOCUMENTO := Trim(Upper(New.C_DOCUMENTO));
      New.C_SERIE := Trim(Upper(New.C_SERIE));
      
      /*               ATRIBUINDO VALORES DEFAULT           */
      If New.N_VLRPRODUTOS Is Null Then
         New.N_VLRPRODUTOS := ''0'';
      End If;
      If New.F_FECHADO Is Null Then
         New.F_FECHADO := ''N'';
      End If;
      If New.D_CADASTRO Is Null Then
         New.D_CADASTRO := Now();
      End If;
      
   End If; 
   /* Inicio das Regras Manuais                                  */

	-- 01
	If TG_OP = ''INSERT'' Then
		If New.I_CdNfBkp Is Null Then
			Select Max(I_CdNfBkp) + 1
			Into New.I_CdNfBkp
			From NfBkp;     
		End If;
		If New.I_CdNfBkp Is Null Then
			New.I_CdNfBkp := 1;
		End If;   
	End If;

	-- 02
	If TG_OP = ''UPDATE'' Then
		If New.F_Fechado = ''S'' Then
			Select Sum(N_Quantidade * N_PrcUnitario) into VTotalItens
			from ItemNfBkp Where ItemNfBkp.I_CdNfBkp = New.I_CdNfBkp;
			If trunc(VTotalItens + New.n_despesa - New.n_desconto,0) <> trunc(New.n_VlrProdutos,0) Then
				RAISE EXCEPTION ''O valor da NF está diferente do total dos itens'';
			End If;
		End If;
	End If;

	-- 03
	If TG_OP = ''UPDATE'' Then
		If coalesce(Old.i_cdsequencial,0) > 0 and coalesce(New.i_cdsequencial,0) = 0 Then
			New.c_documento := null;
			New.c_serie := null;
			New.i_cdparceiro := null;
		End If;
	End If;

	--04
	If TG_OP = ''UPDATE'' Then
		If (new.f_fechado = ''S'') and (coalesce(new.i_cdsequencial,0) > 0) and (coalesce(new.i_cdsequencial_pc,0) > 0) then
			raise exception ''Não é possível fechar complemento vinculado com pedido de compra e nota fiscal!'';
		End If;
	End If;

   /* Fim das Regras Manuais                                     */
   /*         CHECAGEM DOS CAMPOS DE FLAG      */
   If TG_OP = ''INSERT'' OR TG_OP = ''UPDATE'' Then
      /*   VERIFICANDO SE INFORMADO APENAS VALORES PERMITIDOS    */
      If (New.F_FECHADO Is Not Null) And Not(New.F_FECHADO IN (''S'', ''N'')) Then
         RAISE EXCEPTION ''O valor informado para o campo F_FECHADO esta invalido. (F_FECHADO IN ("S", "N"))'';
      End If;
      
     /* CHECANDO SE NAO TEM VALOR NULO PARA AS CHAVES... */
        
   End If;

   If TG_OP = ''DELETE'' Then
      Return Old;
   Else
      Return New;
   End If;
End;'
Language 'plpgsql' VOLATILE;

/*                   CRIACAO DA TRIGGER                          
Create Trigger T_NFBKP
   Before Insert OR Update OR Delete
   On NFBKP
   
   For Each Row
   EXECUTE PROCEDURE FT_NFBKP();
*/


/****************************** ALTERA TRIGGER "ITEMNFBKP" ******************************/
-- Function: ft_itemnfbkp()

-- DROP FUNCTION ft_itemnfbkp();

CREATE OR REPLACE FUNCTION ft_itemnfbkp()
  RETURNS trigger AS
$BODY$Declare
  vI_CdNfBkp NfBkp.I_CdNfBkp%Type;
  vF_Fechado NfBkp.F_Fechado%Type;
Begin

   /*       Usuario que nao executa a trigger    */
   If User = 'free' Then
      If TG_OP = 'DELETE' Then
         Return Old;
      Else
         Return New;
      End If;
   End If;

   /*    BLOQUEIA ALTERAÇÃO NA PRIMARY KEY   */
   If TG_OP = 'UPDATE' Then
      If (NEW.I_CDITEMNFBKP <> OLD.I_CDITEMNFBKP) Then
         RAISE EXCEPTION 'O(s) campo(s) I_CDITEMNFBKP nao pode(m) ser alterado(s).';
      End If;
   End If;

   If TG_OP = 'INSERT' OR TG_OP = 'UPDATE' Then
      /*       EXECUTANDO UPPERCASE NOS CAMPOS         */
      New.C_CDFORNEC := Trim(Upper(New.C_CDFORNEC));
      /*               ATRIBUINDO VALORES DEFAULT           */
   End If; 

   /* Inicio das Regras Manuais                                  */
	-- 01
	If TG_OP = 'INSERT' Then
		If New.I_CdItemNfBkp Is Null Then
			Select Max(I_CdItemNfBkp) + 1
			Into New.I_CdItemNfBkp
			From ItemNfBkp;     
		End If;
		If New.I_CdItemNfBkp Is Null Then
			New.I_CdItemNfBkp := 1;
		End If;   
	End If;

	-- 02
	If TG_OP = 'INSERT' Then
		If New.i_ctitem Is Null Then
			Select Max(i_ctitem) + 1
			into  New.i_ctitem
			from itemnfbkp
			where i_cdnfbkp = New.i_cdnfbkp;
		End If;
		If New.i_ctitem is Null Then
			New.i_ctitem := 1;
		End If;
	End If;

	-- 04 Barrar ação se NF tiver fechada
	If TG_OP = 'INSERT' Or TG_OP = 'UPDATE' Then
		vI_CdNfBkp := New.I_CdNfBkp;
	Else
		vI_CdNfBkp := Old.I_CdNfBkp;
	End If;
	
	Select F_Fechado
	Into vF_Fechado
	From NfBkp
	Where I_CdNfBkp = vI_CdNfBkp;
	If vF_Fechado = 'S' Then
		Raise Exception 'Não é permitido alterção em itens de um complemento já fechado!';
	End If;

   /* Fim das Regras Manuais                                     */

   /*         CHECAGEM DOS CAMPOS DE FLAG      */
   If TG_OP = 'INSERT' OR TG_OP = 'UPDATE' Then
      /*   VERIFICANDO SE INFORMADO APENAS VALORES PERMITIDOS    */
      If  Not(New.I_CDPRODUTO > 0) Then
         RAISE EXCEPTION 'O valor informado para o campo I_CDPRODUTO esta invalido. (I_CDPRODUTO > 0)';
      End If;

     /* CHECANDO SE NAO TEM VALOR NULO PARA AS CHAVES... */
        If New.I_CdNFBKP Is Null Then
          Raise Exception 'Faltando Informar o I_NFBKP!';
       End If;
   End If;

   If TG_OP = 'DELETE' Then
      Return Old;
   Else
      Return New;
   End If;

End;$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION ft_itemnfbkp() OWNER TO postgres;

/**************************************************************************************************************************/ 

COMMIT WORK;