/*	
DESCRICAO----: SGE 0.06
DATA---------: 09/05/2012
*/

/****************************** ALTERA TRIGGER "nfbkp" ******************************/

/* CRIACAO DA FUNCTION TRIGGER                                   */
Create Or Replace Function FT_NFBKP() Returns Trigger As
'Declare
   VTotalItens NfBkp.N_VlrProdutos%type;
Begin
   /* Function Trigger : T_NFBKP                            */
   /* Desenvolvido por : Vinicius Garcia Oliveira em 26/05/2004  */
   /* Alterado por.....:                          em __/__/____  */
   
   /*       Usuario que nao executa a trigger    */
   If User = ''free'' Then
      If TG_OP = ''DELETE'' Then
         Return Old;
      Else
         Return New;
      End If;
   End If;

   /*    BLOQUEIA ALTERA��O NA PRIMARY KEY   */
   If TG_OP = ''UPDATE'' Then
      If (NEW.I_CDNFBKP <> OLD.I_CDNFBKP) Then
         RAISE EXCEPTION ''O(s) campo(s) I_CDNFBKP nao pode(m) ser alterado(s).'';
      End If;
   End If;


   

   If TG_OP = ''INSERT'' OR TG_OP = ''UPDATE'' Then

      /*       EXECUTANDO UPPERCASE NOS CAMPOS         */
      New.F_FECHADO := Trim(Upper(New.F_FECHADO));
      New.C_DOCUMENTO := Trim(Upper(New.C_DOCUMENTO));
      New.C_SERIE := Trim(Upper(New.C_SERIE));
      
      /*               ATRIBUINDO VALORES DEFAULT           */
      If New.N_VLRPRODUTOS Is Null Then
         New.N_VLRPRODUTOS := ''0'';
      End If;
      If New.F_FECHADO Is Null Then
         New.F_FECHADO := ''N'';
      End If;
      If New.D_CADASTRO Is Null Then
         New.D_CADASTRO := Now();
      End If;
      
   End If; 
   /* Inicio das Regras Manuais                                  */

   -- 01
      If TG_OP = ''INSERT'' Then
         If New.I_CdNfBkp Is Null Then
            Select Max(I_CdNfBkp) + 1
              Into New.I_CdNfBkp
              From NfBkp;     
         End If;
         If New.I_CdNfBkp Is Null Then
            New.I_CdNfBkp := 1;
         End If;   
      End If;

   -- 02
      If TG_OP = ''UPDATE'' Then
        If New.F_Fechado = ''S'' Then
          Select Sum(N_Quantidade * N_PrcUnitario) into VTotalItens
            from ItemNfBkp Where ItemNfBkp.I_CdNfBkp = New.I_CdNfBkp;
	  
          If trunc(VTotalItens + New.n_despesa - New.n_desconto,0) <> trunc(New.n_VlrProdutos,0) Then
            RAISE EXCEPTION ''O valor da NF est� diferente do total dos itens'';
	  End If;
        End If;
      End If;

   /* Fim das Regras Manuais                                     */
   /*         CHECAGEM DOS CAMPOS DE FLAG      */
   If TG_OP = ''INSERT'' OR TG_OP = ''UPDATE'' Then
      /*   VERIFICANDO SE INFORMADO APENAS VALORES PERMITIDOS    */
      If (New.F_FECHADO Is Not Null) And Not(New.F_FECHADO IN (''S'', ''N'')) Then
         RAISE EXCEPTION ''O valor informado para o campo F_FECHADO esta invalido. (F_FECHADO IN ("S", "N"))'';
      End If;
      
     /* CHECANDO SE NAO TEM VALOR NULO PARA AS CHAVES... */
        
   End If;

   If TG_OP = ''DELETE'' Then
      Return Old;
   Else
      Return New;
   End If;
End;'
Language 'plpgsql' VOLATILE;

/*                   CRIACAO DA TRIGGER                          
Create Trigger T_NFBKP
   Before Insert OR Update OR Delete
   On NFBKP
   
   For Each Row
   EXECUTE PROCEDURE FT_NFBKP();
*/

/**************************************************************************************************************************/ 

COMMIT WORK;