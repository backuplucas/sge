{=========================================================================
  UFindForm

  Formulario do Componente de busca automatica
  Developing Wendel Alves Machado
  Copyright � 2005
=========================================================================}

   unit UDBFindForm;

interface

uses
  Windows, Messages, SysUtils, DB, DBClient, Provider, Menus, Grids,
  DBGrids, StdCtrls, ExtCtrls, Buttons, Controls, Classes, Forms,
  SqlExpr, Dialogs, SLClientDataSet, Graphics,FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TSlDBLookupForm = class(TForm)
    pParametros: TPanel;
    gbResultados: TGroupBox;
    lCampo: TLabel;
    eDescricao: TEdit;
    btOK: TBitBtn;
    lDescricao: TLabel;
    dsBusca: TDataSource;
    dbgResultados: TDBGrid;
    PopupMenu1: TPopupMenu;
    Confirma1: TMenuItem;
    DataSetProvider: TDataSetProvider;
    ClientDataSet: TClientDataSet;
    sbpesquisa: TSpeedButton;
    bbExecutaBpl: TBitBtn;
    Limpa1: TMenuItem;
    gbBusca: TGroupBox;
    bbInicio: TBitBtn;
    bbMeio: TBitBtn;
    bbFim: TBitBtn;
    BuscaInicio1: TMenuItem;
    BuscaMeio1: TMenuItem;
    BuscaFim1: TMenuItem;
    pmsm: TPanel;
    Pesquisa1: TMenuItem;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    bCresc: TSpeedButton;
    bDecres: TSpeedButton;
    lOrdenacao: TLabel;
    Ordenao1: TMenuItem;
    cbCampos: TListBox;
    Campos1: TMenuItem;
    edata: TSLDate;
    BitBtn1: TBitBtn;
    Sair1: TMenuItem;
    procedure cbCampos1Change(Sender: TObject);
    procedure eDescricaoChange(Sender: TObject);
    procedure Confirma1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgBuscaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure bbExecutaBplClick(Sender: TObject);
    procedure dbgResultadosTitleClick(Column: TColumn);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Limpa1Click(Sender: TObject);
    procedure bbInicioClick(Sender: TObject);
    procedure bbMeioClick(Sender: TObject);
    procedure bbFimClick(Sender: TObject);
    procedure sbpesquisaClick(Sender: TObject);
    procedure bCrescClick(Sender: TObject);
    procedure bDecresClick(Sender: TObject);
    procedure Ordenao1Click(Sender: TObject);
    procedure Campos1Click(Sender: TObject);
    procedure cbCamposClick(Sender: TObject);
    procedure cbCamposExit(Sender: TObject);
    procedure edataChange(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure dbgResultadosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure ClientDataSetAfterOpen(DataSet: TDataSet);
  private
     vSql : TStrings;
     vbusca : Integer;
     vOrdem : Integer;
     valtData : boolean;
  public
     vTableName : string;
     vColumns: TStrings;
     vFieldType: TStrings;
     vSearchField : String;
     vWhere : String;
     ForeingKeyRelation : TStrings;
     vActiveButton : Boolean;
     vCaption :String;
     procedure AtualizaGrid(ORD : boolean);
     procedure AlteraPesquisa;
  end;

var
  SlDBLookupForm: TSlDBLookupForm;

implementation

uses Math;

{$R *.dfm}


procedure TSlDBLookupForm.AtualizaGrid(ORD : boolean);
var i, x, j: Integer;
    campo, colunaPri : String;
    Tipo :String;
begin

   {if  Not ORD Then exit;

   if vcultpes + 0.000005 > now() then exit;
    vcultpes := now();
   }

   if   vbusca = 0 then exit;

   if vSearchField = '' then
   begin
     ShowMessage('n�o foi fornecido o SearchField');
     exit;
   end;

   btOK.Enabled := false;
   if cbCampos.Items.Count > 0 then
    with vSql do
      begin
        dbgResultados.Columns.Clear;
        x := cbCampos.ItemIndex;
        if x >= 0 then
        begin
          dbgResultados.Columns.Add;
          dbgResultados.Columns.Items[0].FieldName := 'Campo'+ IntToStr(x+1);
          dbgResultados.Columns.Items[0].Title.Caption  := cbCampos.Items[x];
          dbgResultados.Columns.Items[0].Font.Color :=  clHotLight;
        end;
        j:= 1;
        For i:= 0 to cbCampos.Items.Count -1 do
        begin
          if i <> x then
          begin
            dbgResultados.Columns.Add;
            dbgResultados.Columns.Items[j].FieldName := 'Campo'+ IntToStr(i+1);
            dbgResultados.Columns.Items[j].Title.Caption  := cbCampos.Items[i];
            inc(j);
          end;
        end;

        Clear;
        Add('Select ' + vSearchField + ' as key '  );


        for i:= 0  to cbCampos.Items.Count -1 do
        begin
          if (vFieldType.Count > 0) and (UpperCase(copy(vFieldType.Strings[i],1,4)) = 'DATE') then
            Add(', date(' + vColumns.Strings[i]+') as Campo'+  IntToStr(i+1) + ' ' )
          else
            Add(',' + vColumns.Strings[i] + ' as Campo'+  IntToStr(i+1) + ' ');
        end;
        Add(' from ' + vTableName);

        //acresenta as tabelas relacionadas
        //sendo a tabela a primeira que aparece no join de relacionamento
        for i:= 0 to ForeingKeyRelation.Count -1 do
           Add(' ' + ForeingKeyRelation.Strings[i]);


     //   for i:= 0 to ForeingKeyRelation.Count -1 do
     //   begin
     //     vTable := copy(ForeingKeyRelation.Strings[i],1,pos('.',ForeingKeyRelation.Strings[i])-1);
     //     Add(', ' + vTable);
     //   end;

        Add(' where ');

        //busca na lista do comboBox (descricao dos campos) a posicao do  campo selecionado
        // se nao existir na listagem de campos (vcolumns) busca o primeiro campo.
        x := cbCampos.ItemIndex;
        if (x > vColumns.Count) or (x = -1) then x := 0;

        campo := vColumns.Strings[x];
        colunaPri := 'Campo' + IntToStr(x+1);

        Tipo := '';
        if vFieldType.Count > 0 then
          Tipo :=  UpperCase(vFieldType.Strings[x]);

        if copy(Tipo,1,4) = 'DATE' then
        begin
         Add( ' date('+ campo + ') =  date(:data) ');
        end
        else
        begin
          if gbBusca.Visible then
            Case vbusca of
              1: Add( 'upper(cast(' + campo + ' as varchar)) like upper('''  + eDescricao.Text+ '%'')' );
              2: Add( 'upper(cast(' + campo + ' as varchar)) like upper(''%' + eDescricao.Text+ '%'')' );
              3: Add( 'upper(cast(' + campo + ' as varchar)) like upper(''%' + eDescricao.Text+ ''')' );
            end
          else  Add( 'upper(cast('+ campo + ' as varchar)) like upper(''' + eDescricao.Text+ '%'')' );
        end;


        if (Length(Trim(vWhere)) > 0) then
          Add(' and ' + vWhere);


        if (Length(eDescricao.Text) > 0) and (pos('::NOORDER',tipo) < 0) then
        begin
          if vOrdem = 1 then
              Add(' order by '+ campo )
          else
              Add(' order by '+ campo + ' Desc ');
        end;

        if vOrdem = 1 then
          Add(' limit 100 ');

      end;
       ClientDataSet.close;
      try
        pmsm.Visible := true;
        Application.ProcessMessages;

        TFDQuery(DataSetProvider.DataSet).SQL.Text := vSql.Text;
        if copy(Tipo,1,4) = 'DATE' Then
        begin
          if not (TFDQuery(DataSetProvider.DataSet).Params.FindParam('data')  = nil) then
            TFDQuery(DataSetProvider.DataSet).Params.ParamByName('data').AsDate := edata.Date;
        end;

        ClientDataSet.Open;

        gbResultados.Caption := '&Resultado - '+ IntToStr(ClientDataSet.RecordCount);
        if Length(eDescricao.Text) = 0 then
            ClientDataSet.IndexFieldNames :=  colunaPri;

        btOK.Enabled := (ClientDataSet.RecordCount > 0);

      finally
        pmsm.Visible := false;
        Application.ProcessMessages;

      end;

   //      MessageDlg('total'+ IntToStr(TFDQuery(DataSetProvider.DataSet).RecordCount), mtError,[mbOk], 0);
end;

procedure TSlDBLookupForm.cbCampos1Change(Sender: TObject);
begin
   AtualizaGrid(True);
end;

procedure TSlDBLookupForm.eDescricaoChange(Sender: TObject);
begin
   AtualizaGrid(true);

end;

procedure TSlDBLookupForm.Confirma1Click(Sender: TObject);
begin
   ModalResult := mrOK;
end;

procedure TSlDBLookupForm.FormShow(Sender: TObject);
begin
  alterapesquisa;
  AtualizaGrid(True);
end;

procedure TSlDBLookupForm.rgBuscaClick(Sender: TObject);
begin
//  if cbBuscaAutmatica.Checked then
    AtualizaGrid(True);
end;

procedure TSlDBLookupForm.FormCreate(Sender: TObject);
begin
  vbusca := 0;
  vOrdem := 1;
  vColumns := TStringList.Create;
  vFieldType := TStringList.Create;
  vSql :=  TStringList.Create;
  ForeingKeyRelation:= TStringList.Create;
end;

procedure TSlDBLookupForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vSql.Free;
end;

procedure TSlDBLookupForm.FormActivate(Sender: TObject);
begin
  valtData := False;
  vbusca := 1;
  vOrdem := 1;
  bbExecutaBpl.Caption := vCaption;
  bbExecutaBpl.Visible := vActiveButton;
  Application.ProcessMessages;
  AlteraPesquisa;
  AtualizaGrid(True);
end;

procedure TSlDBLookupForm.bbExecutaBplClick(Sender: TObject);
begin
   ModalResult := mrYes;
end;

procedure TSlDBLookupForm.dbgResultadosTitleClick(Column: TColumn);
begin
  try
    ClientDataSet.IndexDefs.Add(Column.FieldName+'Desc',Column.FieldName,[ixDescending]);
    ClientDataSet.IndexName := Column.FieldName+'Desc';
  except
    try
      ClientDataSet.IndexFieldNames := Column.FieldName;
      ClientDataSet.IndexDefs.Clear;
    except
    end;
  end;
end;

procedure TSlDBLookupForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If not((ActiveControl = eDescricao) or (ActiveControl = edata)) Then Exit;
  If key = VK_DOWN Then
    If Not ClientDataSet.Eof Then
       ClientDataSet.Next;
  If key = VK_UP Then
    if Not ClientDataSet.Bof Then
       ClientDataSet.Prior;
  If key = VK_NEXT Then
    If Not ClientDataSet.Eof Then
       ClientDataSet.MoveBy(5);

  If key = VK_PRIOR Then
    if Not ClientDataSet.Bof Then
       ClientDataSet.MoveBy(-5);

   if key = VK_ESCAPE then
     Close;

   if Key = VK_RETURN then
     if valtData then
     begin
       valtData := False;
       AtualizaGrid(True);
     end
     else
        ModalResult := mrOK;

end;

procedure TSlDBLookupForm.Limpa1Click(Sender: TObject);
begin
  AlteraPesquisa;
  eDescricao.Clear;
end;

procedure TSlDBLookupForm.bbInicioClick(Sender: TObject);
begin
   vbusca := 1;
   bbInicio.Kind :=  bkAll;
   bbInicio.Kind :=  bkCustom;
   bbInicio.Caption := 'F&5 - Inicio';
   bbInicio.ModalResult := mrNone;

   bbMeio.Kind :=  bkNo;
   bbMeio.Kind :=  bkCustom;
   bbMeio.Caption := 'F&6 - Meio';
   bbMeio.ModalResult := mrNone;

   bbFim.Kind :=  bkNo;
   bbFim.Kind :=  bkCustom;
   bbFim.Caption := 'F&7 - Fim';
   bbFim.ModalResult := mrNone;
   AtualizaGrid(True);
end;

procedure TSlDBLookupForm.bbMeioClick(Sender: TObject);
begin
   vbusca := 2;
   bbInicio.Kind :=  bkNo;
   bbInicio.Kind :=  bkCustom;
   bbInicio.Caption := 'F&5 - Inicio';
   bbInicio.ModalResult := mrNone;

   bbMeio.Kind :=  bkAll;
   bbMeio.Kind :=  bkCustom;
   bbMeio.Caption := 'F&6 - Meio';
   bbMeio.ModalResult := mrNone;

   bbFim.Kind :=  bkNo;
   bbFim.Kind :=  bkCustom;
   bbFim.Caption := 'F&7 - Fim';
   bbFim.ModalResult := mrNone;
   AtualizaGrid(True);
end;

procedure TSlDBLookupForm.bbFimClick(Sender: TObject);
begin
   vbusca := 3;
   bbInicio.Kind :=  bkNo;
   bbInicio.Kind :=  bkCustom;
   bbInicio.Caption := 'F&5 - Inicio';
   bbInicio.ModalResult := mrNone;

   bbMeio.Kind :=  bkNo;
   bbMeio.Kind :=  bkCustom;
   bbMeio.Caption := 'F&6 - Meio';
   bbMeio.ModalResult := mrNone;

   bbFim.Kind :=  bkAll;
   bbFim.Kind :=  bkCustom;
   bbFim.Caption := 'F&7 - Fim';
   bbFim.ModalResult := mrNone;
   AtualizaGrid(True);
end;

procedure TSlDBLookupForm.sbpesquisaClick(Sender: TObject);
begin
  AtualizaGrid(True);
end;

procedure TSlDBLookupForm.bCrescClick(Sender: TObject);
begin
   lOrdenacao.Caption := '- Crescente';
   vOrdem := 1;
   AtualizaGrid(True);
end;

procedure TSlDBLookupForm.bDecresClick(Sender: TObject);
begin
   lOrdenacao.Caption := '- Decrescente';
   vOrdem := 2;
   AtualizaGrid(True);
end;

procedure TSlDBLookupForm.Ordenao1Click(Sender: TObject);
begin
  if vOrdem = 1 then
  begin
    lOrdenacao.Caption := '- Decrescente';
    vOrdem := 2;
  end
  else
  begin
    lOrdenacao.Caption := '- Crescente';
    vOrdem := 1;
  end;
  AtualizaGrid(True);
end;

procedure TSlDBLookupForm.Campos1Click(Sender: TObject);
begin
    if (cbCampos.ItemIndex = cbCampos.Items.Count-1) then
      cbCampos.ItemIndex := 0
    else
      cbCampos.ItemIndex := cbCampos.ItemIndex + 1;
    AlteraPesquisa;
end;

procedure TSlDBLookupForm.AlteraPesquisa;
begin
  if vFieldType.count > 0 then
  begin
    if UpperCase(copy(vFieldType.Strings[cbCampos.ItemIndex],1,4)) = 'DATE' then
    begin
      valtData := False;
      edata.Visible := True;
      edata.SetFocus;
      eDescricao.Visible := false;
   end
    else
    begin
      eDescricao.Visible := True;
      eDescricao.SetFocus;
      edata.Visible := false;
    end;
  end
  else
  begin
    eDescricao.Visible := True;
    eDescricao.SetFocus;
    edata.Visible := false;
  end;
  AtualizaGrid(true);
end;

procedure TSlDBLookupForm.cbCamposClick(Sender: TObject);
begin
  AlteraPesquisa;
end;

procedure TSlDBLookupForm.cbCamposExit(Sender: TObject);
begin
  AlteraPesquisa;
end;

procedure TSlDBLookupForm.edataChange(Sender: TObject);
begin
  valtData := True;
end;

procedure TSlDBLookupForm.btOKClick(Sender: TObject);
begin
   ModalResult := mrOK;
end;

procedure TSlDBLookupForm.dbgResultadosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    ModalResult := mrOK;
end;

procedure TSlDBLookupForm.BitBtn1Click(Sender: TObject);
begin
  close;
end;

procedure TSlDBLookupForm.Sair1Click(Sender: TObject);
begin
 close;
end;

procedure TSlDBLookupForm.ClientDataSetAfterOpen(DataSet: TDataSet);
var i, tam : integer;
begin
  inherited;
  tam := 35;
  for i := 0 to dbgResultados.Columns.Count - 1 do
  begin
    if dbgResultados.Columns.Items[i].Width > 340 then
      dbgResultados.Columns.Items[i].Width := 340;
    tam := tam + 1 + dbgResultados.Columns.Items[i].Width;
  end;

  if tam < SlDBLookupForm.Width then
      tam :=  SlDBLookupForm.Width;

  if tam > Monitor.Width then
      tam :=  Monitor.Width;

  if Monitor.Width  > tam then
    left := trunc((Monitor.Width  - tam)/2)
  else
    left := 0;
   Width := tam;
  {
  if tam + Left > Monitor.Width then
  begin
    Left := 0;
    Width := Monitor.Width
  end
  else
  begin
    Left := Monitor.Width - Width;
    Width := tam;
  end;
  }
end;

end.






